package sv.school;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import sv.school.controller.SchoolController;
import sv.school.view.RootLayoutController;

public class App extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	private RootLayoutController rlController;

	@Override
	public void start(Stage primaryStage) {

		this.primaryStage = primaryStage; // saves the stage in the apps class
		this.primaryStage
				.setTitle("SPTrans - Sistema de Bilhete Único Escolar ");

		initRootLayout();
		// loads the date in the left-inferior corner
		loadDate(primaryStage);
		// loads sptrans logo
		// loadLogo(primaryStage);
		// calls the apps first main content pane (just displays some logos)
		setInitialContent();

		SchoolController schoolController = new SchoolController();
		// descomentar este if para avisar o usuário se a escola ja importou
		// ou não os arquivos de configuração
		// if (schoolController.getAll().size() == 0) {
		// Alert alert = new Alert(AlertType.WARNING,
		// "Para começar a usar o programa integre o arquivo de configuração");
		// alert.show();
		// }

	}

	public BorderPane getRootLayout() {
		return rootLayout;
	}

	public void setInitialContent() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("view/intialContent.fxml"));

			AnchorPane initContent = (AnchorPane) loader.load();

			rootLayout.setCenter(initContent);

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	private void loadDate(Stage primaryStage) {
		// pega a data pelo id
		Label dateLabel = (Label) primaryStage.getScene().lookup("#date");
		// pega a data atual
		LocalDate currentDate = LocalDate.now();
		// cria formatador para mostrar a data é brasileira
		DateTimeFormatter dtfBR = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		// seta o texto na tela principal do app, já formantando da forma
		// usual
		dateLabel.setText(currentDate.format(dtfBR));
	}

	private void initRootLayout() {
		try {

			// loads rootlayout.faml from the directorory to the rootLayout
			// object
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();
			// mostrando no primarystage recebido pelo start
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

				@Override
				public void handle(WindowEvent event) {

					System.exit(0);
				}
			});
			primaryStage.show();

			rlController = loader.getController();
			rlController.setRootLayout(rootLayout);
			rlController.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/*
	 * Apps entry point calls the launch method that sets up somestuff and calls
	 * the start method
	 */
	public static void main(String[] args) {
		Locale.setDefault(new Locale("pt", "BR"));
		System.setProperty("file.encoding", "UTF-8");

		launch(args);

	}

	/**
	 * Reloads the left main menu with apps original menu
	 */
	public void reloadMainMenu() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(App.class.getResource("view/RootLayout.fxml"));
		BorderPane anotherRootLayout;
		try {
			anotherRootLayout = loader.load();
			Pane rootlayoutLeftPane = (Pane) anotherRootLayout.getLeft();
			rootLayout.setLeft(rootlayoutLeftPane);

		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}