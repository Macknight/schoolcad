package sv.school.util;

public class SysDomainConventer {

	public static String periodType(int periodTypeId) {
		String vigencia = "";
		switch (periodTypeId) {
		case 1:
			vigencia = "Semestral";
			break;
		case 2:
			vigencia = "Anual";
			break;
		case 3:
			vigencia = "Anual com aula em julho";
			break;
		}

		return vigencia;
	}

	public static String cota(int schemaId) {
		String schema = "";
		switch (schemaId) {
		case 0:
			schema = "Cota 0 (2o semestre)";
			break;
		case 10:
			schema = "Cota 10 (anual)";
			break;
		case 11:
			schema = "Cota 10 (1o semestre)";
			break;
		case 12:
			schema = "Cota 10 (2o semestre)";
			break;
		case 20:
			schema = "Cota 20 (anual)	";
			break;
		case 21:
			schema = "Cota 20 (1o semestre)";
			break;
		case 22:
			schema = "Cota 20 (2o semestre)";
			break;
		case 23:
			schema = "Cota 10 (anual) (inclusive Julho)";
			break;
		case 30:
			schema = "Cota 30 (anual)";
			break;
		case 31:
			schema = "Cota 30 (1o semestre)";
			break;
		case 32:
			schema = "Cota 30 (2o semestre)";
			break;
		case 33:
			schema = "Cota 20 (anual) (inclusive Julho)";
			break;
		case 40:
			schema = "Cota 40 (anual)";
			break;
		case 41:
			schema = "Cota 40 (1o semestre)";
			break;
		case 42:
			schema = "Cota 40 (2o semestre)";
			break;
		case 43:
			schema = "Cota 30 (anual) (inclusive Julho)";
			break;
		case 50:
			schema = "Cota 50 (anual)";
			break;
		case 51:
			schema = "Cota 50 (1o semestre)";
			break;
		case 52:
			schema = "Cota 50 (2o semestre)";
			break;
		case 53:
			schema = "Cota 40 (anual) (inclusive Julho)";
			break;
		case 80:
			schema = "Cota 80 (anual)";
			break;
		case 81:
			schema = "Cota 80 (1o semestre)";
			break;
		case 82:
			schema = "Cota 80 (2o semestre)	";
			break;
		case 83:
			schema = "Cota 70 (anual) (inclusive Julho)";
			break;
		case 99:
			schema = "Cota 0 (2o semestre)";
			break;
		}
		return schema;
	}

}
