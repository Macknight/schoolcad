package sv.school.controller;

import org.hibernate.Query;

import sv.school.model.School;

public class SchoolController extends Controller<School, Integer> {

	public SchoolController() {
		super(School.class);
		// TODO Auto-generated constructor stub
	}

	public void wipeData() {

		Query query = getCurrentSession().createQuery("delete SchoolContacts");
		query.executeUpdate();

	}

	// private static Controller<School, Integer> controller;
	//
	// public SchoolController() {
	// controller = new Controller<School, Integer>(School.class);
	// }
	//
	// @Override
	// public void persist(School t) {
	// controller.persist(t);
	//
	// }
	//
	// @Override
	// public School getById(Integer id) {
	// School school = controller.getById(id);
	// return school;
	// }
	//
	// @Override
	// public List<School> getAll() {
	// List<School> schools = controller.getAll();
	// return schools;
	// }
	//
	// @Override
	// public void update(School t) {
	// controller.update(t);
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// public void delete(School t) {
	// controller.delete(t);
	//
	// }
	//
	// @Override
	// public void deleteAll() {
	// controller.deleteAll();
	// }
	//
	// public void printToJson(List<School> schools) {
	// controller.printToJson(schools);
	// }
}
