package sv.school.controller;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import sv.school.model.Model;

public class Controller<T, Id extends Serializable> implements DAO<T, Id> {

	private static final SessionFactory sessionFactory = buildSessionFactory();
	private final Class<T> clazz;

	public Controller(Class<T> clazz) {
		this.clazz = clazz;
	}

	private static SessionFactory buildSessionFactory() {
		Configuration configuration = new Configuration().configure();

		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		// builder.applySetting("org.hibernate", "warn");
		SessionFactory sessionFactory = configuration
				.buildSessionFactory(builder.build());

		return sessionFactory;
	}

	// public void closeCurrentSessionwithTransactionc() {
	// if (!currentTransaction.wasCommitted()) {
	// currentTransaction.commit();
	// }
	// if (currentSession.isOpen())
	// currentSession.close();
	// }

	public void commit() {
		sessionFactory.getCurrentSession().getTransaction().commit();
		sessionFactory.getCurrentSession().close();
	}

	public void rollback() {
		sessionFactory.getCurrentSession().getTransaction().rollback();
		sessionFactory.getCurrentSession().close();
	}

	public Session getCurrentSession() {
		if (sessionFactory.getCurrentSession().getTransaction() == null
				|| !sessionFactory.getCurrentSession().getTransaction()
						.isActive()) {
			sessionFactory.getCurrentSession().beginTransaction();
		}
		return sessionFactory.getCurrentSession();
	}

	public void persist(T t) {
		// openCurrentSessionwithTransaction();
		getCurrentSession().save(t);
		// closeCurrentSessionwithTransaction();
	}

	public T getById(Id id) {
		// openCurrentSession();
		@SuppressWarnings("unchecked")
		T t = (T) getCurrentSession().get(clazz, id);
		// closeCurrentSession();
		return t;
	}

	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		// openCurrentSession();
		List<T> list = (List<T>) getCurrentSession().createQuery(
				"from " + clazz.getName()).list();
		// closeCurrentSession();
		return list;
	}

	public void update(T t) {
		// openCurrentSessionwithTransaction();
		getCurrentSession().update(t);
		// closeCurrentSessionwithTransaction();
	}

	public void delete(T t) {
		// openCurrentSessionwithTransaction();
		getCurrentSession().delete(t);
		// closeCurrentSessionwithTransaction();
	}

	public void deleteAll() {
		// openCurrentSessionwithTransaction();
		List<T> people = getAll();
		for (T t : people) {
			delete(t);
		}
		// closeCurrentSessionwithTransaction();
	}

	public void printToJson(List<T> l) {

		int size = l.size();
		System.out.println("[");
		for (T i : l) {
			System.out.println(((Model) i).toJson());
			if (--size != 0) {
				System.out.print(",");
			}
		}
		System.out.print("]");
	}
}
