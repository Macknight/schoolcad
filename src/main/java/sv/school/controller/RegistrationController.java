package sv.school.controller;

import java.util.List;

import org.hibernate.Query;

import sv.school.model.Registration;

public class RegistrationController extends Controller<Registration, Integer> {

	public RegistrationController() {
		super(Registration.class);
		// TODO Auto-generated constructor stub
	}

	public int getLastRegistrationId() {
		Query query = getCurrentSession()
				.createQuery(
						" from Registration where REGISTRATION_ID = (select max(id) from Registration)");
		Registration rRegistration = (Registration) query.list().get(0);
		return rRegistration.getRegistrationId();
	}

	public List<Registration> getRegistrationByUserId(int userId) {
		Query query = getCurrentSession().createQuery(
				" from Registration where USER_ID = :code");
		query.setParameter("code", userId);
		List list = query.list();
		return list;
	}

	// private static Controller<Registration, Integer> controller;
	//
	// public RegistrationController() {
	// controller = new Controller<Registration, Integer>(Registration.class);
	// }
	//
	// @Override
	// public void persist(Registration t) {
	// controller.persist(t);
	//
	// }
	//
	// @Override
	// public Registration getById(Integer id) {
	// Registration retReg = controller.getById(id);
	// return retReg;
	// }
	//
	// @Override
	// public List<Registration> getAll() {
	// List<Registration> allReg = controller.getAll();
	// return allReg;
	// }
	//
	// @Override
	// public void update(Registration t) {
	// controller.update(t);
	//
	// }
	//
	// @Override
	// public void delete(Registration t) {
	// controller.delete(t);
	//
	// }
	//
	// @Override
	// public void deleteAll() {
	// controller.deleteAll();
	//
	// }

}
