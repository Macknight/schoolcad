package sv.school.controller;

import java.util.Collections;
import java.util.List;

import org.hibernate.Query;

import sv.school.model.User;

public class UserController extends Controller<User, Integer> {

	public UserController() {
		super(User.class);
	}

	public int getLastUserId() {
		int lastId ;
        Query query = getCurrentSession().createQuery(
	" from User where USER_ID = (select max(id) from User)");
	List<User> lastUser = query.list();
	if(lastUser!=null && !lastUser.isEmpty()) {
		User uUser = lastUser.get(0);
		lastId =  lastUser.get(0).getUserId();; // � o primeiro
	}else {
		lastId   = 1;

	}
	return lastId;
}

	public List<User> getLimitUsers(int init, int maxUsers) {
		Query q = getCurrentSession().createQuery("from User");
		q.setFirstResult(init);
		q.setMaxResults(maxUsers);
		List<User> uList = q.list();
		return uList;

	}


	public List<User> getUsersByName(String sName) {
		sName =  sName.toUpperCase();
		String singleQuotedName = "\'%".concat(sName.concat("%\'"));

		String sQuery  = "from User where NAME like ".concat(singleQuotedName);
		System.out.println(sQuery);
		Query q = getCurrentSession().createQuery(sQuery);

		List<User> uList = q.list();
		return uList;

	}

	public List<User> getUsersByRG(String sRG) {
		sRG =  sRG.toUpperCase();
		String singleQuotedName = "\'%".concat(sRG.concat("%\'"));

		String sQuery  = "from User where RG_NUMBER like ".concat(singleQuotedName);
		System.out.println(sQuery);
		Query q = getCurrentSession().createQuery(sQuery);

		List<User> uList = q.list();
		return uList;

	}


}
