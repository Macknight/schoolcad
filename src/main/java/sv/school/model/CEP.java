package sv.school.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SPT_CEP")
public class CEP implements Model {
	@Id
	@Column(name = "CEP")
	private long lCep;

	@Column(name = "LOGRADOURO")
	private String logradouro;

	@Column(name = "CIDADE")
	private String cidade;

	@Column(name = "BAIRRO")
	private String bairro;

	@Column(name = "ESTADO")
	private String estado;

	@Column(name = "TIPOLOGRADOURO")
	private String tipoLogradouro;

	@Column(name = "INICIO")
	private String inicio;

	@Column(name = "FIM")
	private String fim;

	@Column(name = "PARIDADE")
	private String paridade;

	@Override
	public String toJson() {
		// TODO Auto-generated method stub
		return (lCep + ";" + logradouro + ";" + cidade + ";" + bairro + ";"
				+ estado + ";" + tipoLogradouro + ";" + inicio + ";" + fim
				+ ";" + paridade);
	}

	public long getlCep() {
		return lCep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public String getCidade() {
		return cidade;
	}

	public String getBairro() {
		return bairro;
	}

	public String getEstado() {
		return estado;
	}

	public String getTipoLogradouro() {
		return tipoLogradouro;
	}

	public String getInicio() {
		return inicio;
	}

	public String getFim() {
		return fim;
	}

	public String getParidade() {
		return paridade;
	}

	public void setlCep(long lCep) {
		this.lCep = lCep;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public void setTipoLogradouro(String tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}

	public void setInicio(String inicio) {
		this.inicio = inicio;
	}

	public void setFim(String fim) {
		this.fim = fim;
	}

	public void setParidade(String paridade) {
		this.paridade = paridade;
	}

	public CEP() {
		this.lCep = 0;
		this.logradouro = "default";
	}

	public CEP(long lCep, String logradouro, String cidade, String bairro,
			String estado, String tipoLogradouro, String inicio, String fim,
			String paridade) {
		super();
		this.lCep = lCep;
		this.logradouro = logradouro;
		this.cidade = cidade;
		this.bairro = bairro;
		this.estado = estado;
		this.tipoLogradouro = tipoLogradouro;
		this.inicio = inicio;
		this.fim = fim;
		this.paridade = paridade;
	}

	public CEP(long lCep, String logradouro, String cidade, String bairro,
			String estado, String tipoLogradouro) {
		super();
		this.lCep = lCep;
		this.logradouro = logradouro;
		this.cidade = cidade;
		this.bairro = bairro;
		this.estado = estado;
		this.tipoLogradouro = tipoLogradouro;
	}

}
