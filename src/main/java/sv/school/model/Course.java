package sv.school.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//CREATE TABLE SPT_COURSES (
//	       COURSE_CODE          CHAR(10) NOT NULL,
//	       DESCRIPTION          CHAR(40) NOT NULL,
//	       SCHOOL_LEVEL_CODE_ID INTEGER NULL,
//	       XID                  INTEGER NULL,
//	       COMMENTS             TEXT NULL
//	);

@Entity
@Table(name = "SPT_COURSES")
public class Course implements Model {

	@Id
	@Column(name = "COURSE_CODE")
	private String courseCode;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "SCHOOL_LEVEL_CODE_ID")
	private int schoolLevelCodeId;

	@Column(name = "XID")
	private int xId;

	@Column(name = "COMMENTS")
	private String comments;

	public String getCourseCode() {
		return courseCode;
	}

	public String getDescription() {
		return description;
	}

	public int getSchoolLevelCodeId() {
		return schoolLevelCodeId;
	}

	public int getxId() {
		return xId;
	}

	public String getComments() {
		return comments;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setSchoolLevelCodeId(int schoolLevelCodeId) {
		this.schoolLevelCodeId = schoolLevelCodeId;
	}

	public void setxId(int xId) {
		this.xId = xId;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Course(String courseCode, String description, int schoolLevelCodeId,
			int xId, String comments) {
		super();
		this.courseCode = courseCode;
		this.description = description;
		this.schoolLevelCodeId = schoolLevelCodeId;
		this.xId = xId;
		this.comments = comments;
	}

	public Course(String courseCode) {
		this.courseCode = courseCode;
		this.description = "nooo description";
		this.schoolLevelCodeId = 10;
		this.xId = 10;
		this.comments = " there are no comments yet";

	}

	public Course(String courseCode, String description) {
		this.courseCode = courseCode;
		this.description = description;
		this.schoolLevelCodeId = 10;
		this.xId = 10;
		this.comments = " there are no comments yet";

	}

	public String toJson() {
		return "{\"courseCode\": \"" + getCourseCode()
				+ "\", \"description\":\"" + getDescription() + "\"}";
	}

	public Course() {

	}

}
