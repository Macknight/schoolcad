package sv.school.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 CREATE TABLE SPT_SCHOOL (
 SCHOOL_ID            INTEGER NOT NULL,
 NAME                 CHAR(35) NOT NULL,
 FANTASY_NAME         CHAR(25) NULL,
 ADDRESS_STREET       CHAR(25) NULL,
 ADDRESS_NUMBER       CHAR(10) NULL,
 ADDRESS_COMPLEMENT   CHAR(20) NULL,
 ADDRESS_QUARTER      CHAR(20) NULL,
 ADDRESS_ZIP_CODE     CHAR(8) NULL,
 TELEPHONE_NUMBER     CHAR(15) NULL,
 TELEPHONE_BRANCH     CHAR(5) NULL,
 EMAIL                CHAR(40) NULL,
 MEC_NUMBER           CHAR(10) NULL,
 INSERT_DT            DATE NULL,
 ESTABLISHMENT        CHAR(1) NULL,
 SCHOOL_LEVEL_TYPE_ID INTEGER NULL,
 SCHOOL_TYPE_ID       INTEGER NULL,
 SCHOOL_STATUS_ID     INTEGER NULL,
 XID                  INTEGER NULL,
 COMMENTS             TEXT NULL
 );
 */

/**
 * Modelo para o objeto escola
 * 
 * @author rodrigo.poloni version 1.0
 *
 */
@Entity
@Table(name = "SPT_SCHOOL")
public class School implements Model {

	@Id
	@Column(name = "SCHOOL_ID")
	private int schoolId;

	@Column(name = "NAME")
	private String name;

	@Column(name = "FANTASY_NAME")
	private String fantasyName;

	@Column(name = "ADDRESS_STREET")
	private String addressStreet;

	@Column(name = "ADDRESS_NUMBER")
	private String addressNumber;

	@Column(name = "ADDRESS_COMPLEMENT")
	private String addressComplement;

	@Column(name = "ADDRESS_QUARTER")
	private String addressQuarter;

	@Column(name = "ADDRESS_ZIP_CODE")
	private String addressZipCode;

	@Column(name = "TELEPHONE_NUMBER")
	private String telephoneNumber;

	@Column(name = "TELEPHONE_BRANCH")
	private String teleephoneBranch;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "MEC_NUMBER")
	private String mecNumber;

	@Column(name = "INSERT_DT")
	private Date insertDT;

	@Column(name = "ESTABLISHMENT")
	private String establishment;

	@Column(name = "SCHOOL_LEVEL_TYPE_ID")
	private int schoolLevelTypeId;

	@Column(name = "SCHOOL_TYPE_ID")
	private int schoolTypeId;

	@Column(name = "XID")
	private int xId;

	@Column(name = "COMMENTS")
	private String comments;

	/**
	 * Construtores
	 */
	public School() {
	}

	public School(String name) {
		this.name = name;
		this.schoolId = 1;
	}

	public School(String name, int id) {
		this.name = name;
		this.schoolId = id;
	}

	// SCHOOL_ID
	// NAME
	// FANTASY_NAME
	// ADDRESS_STREET
	// ADDRESS_NUMBER
	// ADDRESS_COMPLEMENT
	// ADDRESS_QUARTER
	// ADDRESS_ZIP_CODE
	// TELEPHONE_NUMBER
	// TELEPHONE_BRANCH
	// EMAIL
	// MEC_NUMBER
	// INSERT_DT
	// ESTABLISHMENT_TYPE_ID
	// SCHOOL_LEVEL_FLAGS
	// SCHOOL_LEVEL_TYPE_ID
	// SCHOOL_STATUS_ID
	// XID
	// COMMENTS

	public School(int schoolId, String name, String fantasyName,
			String addressStreet, String addressNumber,
			String addressComplement, String addressQuarter,
			String addressZipCode, String telephoneNumber,
			String teleephoneBranch, String email, String mecNumber,
			Date insertDT, String establishment, int schoolLevelTypeId,
			int schoolTypeId, int xId, String comments) {

		this.schoolId = schoolId;
		this.name = name;
		this.fantasyName = fantasyName;
		this.addressStreet = addressStreet;
		this.addressNumber = addressNumber;
		this.addressComplement = addressComplement;
		this.addressQuarter = addressQuarter;
		this.addressZipCode = addressZipCode;
		this.telephoneNumber = telephoneNumber;
		this.teleephoneBranch = teleephoneBranch;
		this.email = email;
		this.mecNumber = mecNumber;
		this.insertDT = insertDT;
		this.establishment = establishment;
		this.schoolLevelTypeId = schoolLevelTypeId;
		this.schoolTypeId = schoolTypeId;
		this.xId = xId;
		this.comments = comments;
	}

	/**
	 * 
	 * @return The Json String of this class
	 */
	public String toJson() {
		return "{\"schoolId\": \"" + getSchoolId() + "\", \"name\":\""
				+ getName() + "\"," + "\"fantasyName\":\"" + getFantasyName()
				+ "\""

				+ "}";
	}

	/*
	 * Getters and Setters
	 */
	public int getSchoolId() {
		return schoolId;
	}

	public String getName() {
		return name;
	}

	public String getFantasyName() {
		return fantasyName;
	}

	public String getAddressStreet() {
		return addressStreet;
	}

	public String getAddressNumber() {
		return addressNumber;
	}

	public String getAddressComplement() {
		return addressComplement;
	}

	public String getAddressQuarter() {
		return addressQuarter;
	}

	public String getAddressZipCode() {
		return addressZipCode;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public String getTeleephoneBranch() {
		return teleephoneBranch;
	}

	public String getEmail() {
		return email;
	}

	public String getMecNumber() {
		return mecNumber;
	}

	public Date getInsertDT() {
		return insertDT;
	}

	public String getEstablishment() {
		return establishment;
	}

	public int getSchoolLevelTypeId() {
		return schoolLevelTypeId;
	}

	public int getSchoolTypeId() {
		return schoolTypeId;
	}

	public int getxId() {
		return xId;
	}

	public String getComments() {
		return comments;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setFantasyName(String fantasyName) {
		this.fantasyName = fantasyName;
	}

	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}

	public void setAddressComplement(String addressComplement) {
		this.addressComplement = addressComplement;
	}

	public void setAddressQuarter(String addressQuarter) {
		this.addressQuarter = addressQuarter;
	}

	public void setAddressZipCode(String addressZipCode) {
		this.addressZipCode = addressZipCode;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public void setTeleephoneBranch(String teleephoneBranch) {
		this.teleephoneBranch = teleephoneBranch;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setMecNumber(String mecNumber) {
		this.mecNumber = mecNumber;
	}

	public void setInsertDT(Date insertDT) {
		this.insertDT = insertDT;
	}

	public void setEstablishment(String establishment) {
		this.establishment = establishment;
	}

	public void setSchoolLevelTypeId(int schoolLevelTypeId) {
		this.schoolLevelTypeId = schoolLevelTypeId;
	}

	public void setSchoolTypeId(int schoolTypeId) {
		this.schoolTypeId = schoolTypeId;
	}

	public void setxId(int xId) {
		this.xId = xId;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	// sv.school.model.School.School(int schoolId, String name, String
	// fantasyName, String addressStreet,
	// String addressNumber, String addressComplement, String addressQuarter,
	// String addressZipCode,
	// String telephoneNumber, String teleephoneBranch, String email, String
	// mecNumber, Date insertDT,
	// String establishment,
	// int schoolLevelTypeId, int schoolTypeId, int xId, String comments)
	public String toCSV() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
		System.out.println(formatter.format(this.insertDT));
		return String.valueOf(this.schoolId) + (";") + this.name + (";")
				+ this.fantasyName + (";") + this.addressStreet + (";")
				+ this.addressComplement + (";") + this.addressQuarter + (";")
				+ this.addressZipCode + (";") + this.telephoneNumber + (";")
				+ this.teleephoneBranch + (";") + this.email + (";")
				+ this.mecNumber + (";") + formatter.format(this.insertDT)
				+ (";") + this.establishment + (";") + this.schoolLevelTypeId
				+ (";") + this.schoolTypeId + (";") + this.xId + (";")
				+ this.comments + (";");

	}
}
