package sv.school.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

//CREATE TABLE SPT_SCHOOL_COURSES (
//	       SCHOOL_COURSE_ID     INTEGER NOT NULL,
//	       SCHOOL_ID            INTEGER NOT NULL,
//	       COURSE_CODE          CHAR(10) NOT NULL,
//	       DESCRIPTION          CHAR(50) NOT NULL,
//	       XID                  INTEGER NULL,
//	       SCHEMA_ID            INTEGER NULL,
//	       PERIOD_TYPE_ID       INTEGER NULL,
//	       YEAR                 CHAR(02) NULL,
//	       VALID_INITIAL_DT     DATE NULL,
//	       VALID_FINAL_DT       DATE NULL,
//	       STUDENT_QUANTITY     INTEGER NULL,
//	       SCHOOL_COURSE_STATUS_ID INTEGER NULL
//	);

/**
 * 
 * @author rodrigo.poloni Model for the school courses (turmas)
 */
@Entity
@Table(name = "SPT_SCHOOL_COURSES")
public class SchoolCourse implements Model, Serializable {

	@Embeddable
	public static class SchoolCoursePK implements Serializable {

		@Column(name = "SCHOOL_ID")
		private int schoolId;
		@Column(name = "COURSE_CODE")
		private String courseCode;
		@Column(name = "SCHOOL_COURSE_ID")
		private int schoolCourseId;

		public static final int SCHOOLCOURSE_STATUS_ATIVO = 1;
		public static final int SCHOOLCOURSE_STATUS_INATIVO = 0;

		public int getSchoolId() {
			return schoolId;
		}

		public String getCourseCode() {
			return courseCode;
		}

		public int getSchoolCourseId() {
			return schoolCourseId;
		}

		public void setSchoolId(int schoolId) {
			this.schoolId = schoolId;
		}

		public void setCourseCode(String courseCode) {
			this.courseCode = courseCode;
		}

		public void setSchoolCourseId(int schoolCourseId) {
			this.schoolCourseId = schoolCourseId;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((courseCode == null) ? 0 : courseCode.hashCode());
			result = prime * result + schoolCourseId;
			result = prime * result + schoolId;
			return result;
		}

		@Override
		public boolean equals(Object obj) {

			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof SchoolCoursePK))
				return false;
			SchoolCoursePK other = (SchoolCoursePK) obj;
			if (courseCode == null) {
				if (other.courseCode != null)
					return false;
			} else if (!courseCode.equals(other.courseCode))
				return false;
			if (schoolCourseId != other.schoolCourseId)
				return false;
			if (schoolId != other.schoolId)
				return false;
			return true;
		}

		public SchoolCoursePK(int schoolId, String courseCode,
				int schoolCourseId) {
			super();
			this.schoolId = schoolId;
			this.courseCode = courseCode;
			this.schoolCourseId = schoolCourseId;
		}

		public SchoolCoursePK() {

		}

	}

	@EmbeddedId
	SchoolCoursePK schoolCoursePK;

	public SchoolCoursePK getSchoolCoursePK() {
		return schoolCoursePK;
	}

	public String getCourseCode() {
		return this.schoolCoursePK.courseCode;
	}

	public void SchoolCoursePK() {
	}

	public void setPK(int schoolId, String courseCode, int schoolCourseId) {
		this.schoolCoursePK.setSchoolId(schoolId);
		this.schoolCoursePK.setCourseCode(courseCode);
		this.schoolCoursePK.setSchoolCourseId(schoolCourseId);

	}

	public int getVigenciaTypeId() {
		return vigenciaTypeId;
	}

	public int getPeriodId() {
		return periodId;
	}

	public void setSchoolCoursePK(SchoolCoursePK schoolCoursePK) {
		this.schoolCoursePK = schoolCoursePK;
	}

	public void setVigenciaTypeId(int vigenciaTypeId) {
		this.vigenciaTypeId = vigenciaTypeId;
	}

	public void setPeriodId(int periodId) {
		this.periodId = periodId;
	}

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "SCHEMA_ID")
	private int schemaId;

	@Column(name = "PERIOD_TYPE_ID")
	private int periodTypeId;

	@Column(name = "YEAR")
	private String year;

	@Column(name = "VALID_INITIAL_DT")
	private Date validInitialDate;

	@Column(name = "VALID_FINAL_DT")
	private Date validFinalDate;

	@Column(name = "STUDENT_QUANTITY")
	private int studentQuantity;

	@Column(name = "SCHOOL_COURSE_STATUS_ID")
	private int schoolCourseStatusId;

	private int vigenciaTypeId;

	private int periodId;

	@Column(name = "XID")
	private int xId;

	// private String grade;

	public SchoolCourse() {
	}

	public SchoolCourse(int schoolId, String courseCode, int schoolCourseId,
			String description, String grade, int schemaId, int periodTypeId,
			String sysYear, Date initDate, Date finalDate, int studentQtty,
			int schoolCourseStatusId, int vigenciaTypeId, int periodId, int xId) {
		schoolCoursePK = new SchoolCoursePK(schoolId, courseCode,
				schoolCourseId);
		this.schoolCoursePK.setSchoolId(schoolId);
		this.schoolCoursePK.setCourseCode(courseCode);
		this.schoolCoursePK.setSchoolCourseId(schoolCourseId);
		this.description = description;
		// this.grade =grade
		this.schemaId = schemaId;
		this.periodTypeId = periodTypeId;
		if (sysYear.length() > 2) {
			this.year = sysYear.substring(sysYear.length() - 2,
					sysYear.length());
		} else {
			this.year = sysYear;
		}
		this.validInitialDate = initDate;
		this.validFinalDate = finalDate;
		this.studentQuantity = studentQtty;
		this.schoolCourseStatusId = schoolCourseStatusId;
		this.vigenciaTypeId = vigenciaTypeId;
		this.periodId = periodId;
		this.xId = xId;
	}

	@Override
	public String toJson() {
		return "schoolCourseId  " + getSchoolCoursePK().getSchoolCourseId()
				+ "{\"schoolId\": \"" + getSchoolCoursePK().getSchoolId()
				+ "\", \"name\":\"" + getDescription() + "\"}";
	}

	public String getDescription() {
		return description;
	}

	public int getxId() {
		return xId;
	}

	public int getSchemaId() {
		return schemaId;
	}

	public int getPeriodTypeId() {
		return periodTypeId;
	}

	public String getYear() {
		return year;
	}

	public Date getValidInitialDate() {
		return validInitialDate;
	}

	public Date getValidFinalDate() {
		return validFinalDate;
	}

	public int getStudentQuantity() {
		return studentQuantity;
	}

	public int getSchoolCourseStatusId() {
		return schoolCourseStatusId;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setxId(int xId) {
		this.xId = xId;
	}

	public void setSchemaId(int schemaId) {
		this.schemaId = schemaId;
	}

	public void setPeriodTypeId(int periodTypeId) {
		this.periodTypeId = periodTypeId;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public void setValidInitialDate(Date validInitialDate) {
		this.validInitialDate = validInitialDate;
	}

	public void setValidInitialDate(String sDate) {
		DateFormat format = new SimpleDateFormat("dd/mm/yyyy");
		try {
			Date date = format.parse(sDate);
			this.validInitialDate = date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setValidFinalDate(String sDate) {
		DateFormat format = new SimpleDateFormat("dd/mm/yyyy");
		try {
			Date date = format.parse(sDate);
			this.validFinalDate = date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setValidFinalDate(Date validFinalDate) {
		this.validFinalDate = validFinalDate;
	}

	public void setStudentQuantity(int studentQuantity) {
		this.studentQuantity = studentQuantity;
	}

	public void setSchoolCourseStatusId(int schoolCourseStatusId) {
		this.schoolCourseStatusId = schoolCourseStatusId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((schoolCoursePK == null) ? 0 : schoolCoursePK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SchoolCourse other = (SchoolCourse) obj;
		if (schoolCoursePK == null) {
			if (other.schoolCoursePK != null)
				return false;
		} else if (!schoolCoursePK.equals(other.schoolCoursePK))
			return false;
		return true;
	}

	public String toCSV() {
		// int schoolId, String courseCode, int schoolCourseId,
		// String description, String grade, int schemaId, int periodTypeId,
		// String sysYear, Date initDate, Date finalDate, int studentQtty,
		// int schoolCourseStatusId, int vigenciaTypeId, int periodId, int xId
		//
		SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");

		return this.schoolCoursePK.getSchoolId() + ";"
				+ this.getCourseCode()
				+ ";"
				+ this.schoolCoursePK.getSchoolCourseId()
				+ ";"
				+ this.description
				+ ";;"
				+ // two semicolons because grade is a deprecated info
				this.schemaId + ";" + this.periodTypeId + ";" + this.year + ";"
				+ formatter.format(this.validInitialDate) + ";"
				+ formatter.format(this.validFinalDate) + ";"
				+ this.studentQuantity + ";" + this.schoolCourseStatusId + ";"
				+ this.vigenciaTypeId + ";" + this.periodId + ";" + this.xId
				+ ";";

	}

}
