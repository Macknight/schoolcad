package sv.school.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//CREATE TABLE SPT_COUNTRY (
//	       COUNTRY_CODE         CHAR(2) NOT NULL,
//	       NAME                 CHAR(30) NULL,
//	       NACIONALITY          CHAR(30) NULL
//	);

@Entity
@Table(name = "SPT_COUNTRY")
public class Country {
	@Id
	@Column(name = "COUNTRY_CODE")
	private String countryCode;
	@Column(name = "NAME")
	private String name;
	@Column(name = "NACIONALITY")
	private String nationality;

	public String getCountryCode() {
		return countryCode;
	}

	public String getName() {
		return name;
	}

	public String getNationality() {
		return nationality;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
}
