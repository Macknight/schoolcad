package sv.school.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//CREATE TABLE SPT_SCHOOL_CONTACTS (
//	       SCHOOL_CONTACT_ID    INTEGER NOT NULL,
//	       SCHOOL_ID            INTEGER NULL,
//	       NAME                 CHAR(50) NULL,
//	       RG_NUMBER            CHAR(14) NULL,
//	       RG_DIGIT             CHAR(2) NULL,
//	       RG_EMITTED_STATE     CHAR(2) NULL,
//	       RG_EMITTED_DT        DATE NULL,
//	       EMAIL                CHAR(50) NULL,
//	       TELEPHONE_NUMBER     CHAR(15) NULL,
//	       TELEPHONE_BRANCH     CHAR(5) NULL,
//	       SCHOOL_CONTACT_TYPE_ID INTEGER NULL,
//	       SCHOOL_CONTACT_STATUS_ID INTEGER NULL,
//	       XID                  INTEGER NULL,
//	       COMMENTS             TEXT NULL
//	);
@Entity
@Table(name = "SPT_SCHOOL_CONTACTS")
public class SchoolContacts implements Model {

	@Id
	@Column(name = "SCHOOL_CONTACT_ID")
	private int schoolContactId;

	@Column(name = "SCHOOL_ID")
	private int schoolId;

	@Column(name = "NAME")
	private String name;

	@Column(name = "RG_NUMBER")
	private String rgNumber;

	@Column(name = "RG_EMITTED_STATE")
	private String rgEmittedState;

	@Column(name = "RG_DIGIT")
	private String rgDigit;

	@Column(name = "RG_EMITTED_DT")
	private Date rgEmittedDate;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "TELEPHONE_NUMBER")
	private String telephoneNumber;
	@Column(name = "TELEPHONE_BRANCH")
	private String telephoneBranch;
	@Column(name = "SCHOOL_CONTACT_TYPE_ID")
	private int schoolContantTypeId;
	@Column(name = "SCHOOL_CONTACT_STATUS_ID")
	private int schoolContactStatusId;

	@Column(name = "XID")
	private String xId;
	@Column(name = "COMMENTS")
	private String comments;

	public SchoolContacts() {
		name = "random_contact";
		schoolId = 1000;
	}

	public SchoolContacts(String name) {
		this.name = name;
	}

	public SchoolContacts(String name, int schoolContactId) {
		this.schoolContactId = schoolContactId;
		this.schoolId = 1000;
		this.name = name;
		this.rgNumber = "129783";
		this.rgEmittedState = "98102739";
		this.rgEmittedDate = new Date();
		this.rgDigit = "5";
		this.email = "aksjldhf@gmail.com";
		this.telephoneNumber = "7981263";
		this.telephoneBranch = "19";
		this.schoolContantTypeId = 5;
		this.schoolContactStatusId = 3;
		this.xId = "123";
		this.comments = "Noo comments";
	}

	public SchoolContacts(int schoolContactId, int schoolId, String name,
			String rgNumber, String rgEmittedState, Date rgEmittedDate,
			String email, String telephoneNumber, String telephoneBranch,
			int schoolContantTypeId, int schoolContactStatusId, String xId,
			String comments) {

		this.schoolContactId = schoolContactId;
		this.schoolId = schoolId;
		this.name = name;
		this.rgNumber = rgNumber;
		this.rgEmittedState = rgEmittedState;
		this.rgEmittedDate = rgEmittedDate;
		this.email = email;
		this.telephoneNumber = telephoneNumber;
		this.telephoneBranch = telephoneBranch;
		this.schoolContantTypeId = schoolContantTypeId;
		this.schoolContactStatusId = schoolContactStatusId;
		this.xId = xId;
		this.comments = comments;
	}

	public SchoolContacts(int schoolContactId, int schoolId, String name,
			String rgNumber, String rgDigit, String rgEmittedState,
			Date rgEmittedDate, String email, String telephoneNumber,
			String telephoneBranch, int schoolContantTypeId,
			int schoolContactStatusId, String xId, String comments) {
		this.rgDigit = rgDigit;
		this.schoolContactId = schoolContactId;
		this.schoolId = schoolId;
		this.name = name;
		this.rgNumber = rgNumber;
		this.rgEmittedState = rgEmittedState;
		this.rgEmittedDate = rgEmittedDate;
		this.email = email;
		this.telephoneNumber = telephoneNumber;
		this.telephoneBranch = telephoneBranch;
		this.schoolContantTypeId = schoolContantTypeId;
		this.schoolContactStatusId = schoolContactStatusId;
		this.xId = xId;
		this.comments = comments;
	}

	public SchoolContacts(int schoolContactId, int schoolId, String name,
			String rgNumber, String rgDigit, String rgEmittedState,
			Date rgEmittedDate, String email, String telephoneNumber,
			String telephoneBranch, int schoolContantTypeId,
			int schoolContactStatusId, String xId) {
		this.rgDigit = rgDigit;
		this.schoolContactId = schoolContactId;
		this.schoolId = schoolId;
		this.name = name;
		this.rgNumber = rgNumber;
		this.rgEmittedState = rgEmittedState;
		this.rgEmittedDate = rgEmittedDate;
		this.email = email;
		this.telephoneNumber = telephoneNumber;
		this.telephoneBranch = telephoneBranch;
		this.schoolContantTypeId = schoolContantTypeId;
		this.schoolContactStatusId = schoolContactStatusId;
		this.xId = xId;
		this.comments = "";
	}

	@Override
	public String toJson() {
		return "'schoolContactId' : '" + getSchoolContactId() + "'"
				+ "'schoolId' :" + getSchoolId() + "'" + "'rgNumber' :"
				+ getRgNumber() + "'name' :" + getName();
	}

	public int getSchoolContactId() {
		return schoolContactId;
	}

	public int getSchoolId() {
		return schoolId;
	}

	public String getName() {
		return name;
	}

	public String getRgNumber() {
		return rgNumber;
	}

	public String getRgEmittedState() {
		return rgEmittedState;
	}

	public Date getRgEmittedDate() {
		return rgEmittedDate;
	}

	public String getEmail() {
		return email;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public String getTelephoneBranch() {
		return telephoneBranch;
	}

	public int getSchoolContantTypeId() {
		return schoolContantTypeId;
	}

	public int getSchoolContactStatusId() {
		return schoolContactStatusId;
	}

	public String getxId() {
		return xId;
	}

	public String getComments() {
		return comments;
	}

	public void setSchoolContactId(int schoolContactId) {
		this.schoolContactId = schoolContactId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRgNumber(String rgNumber) {
		this.rgNumber = rgNumber;
	}

	public void setRgEmittedState(String rgEmittedState) {
		this.rgEmittedState = rgEmittedState;
	}

	public void setRgEmittedDate(Date rgEmittedDate) {
		this.rgEmittedDate = rgEmittedDate;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public void setTelephoneBranch(String telephoneBranch) {
		this.telephoneBranch = telephoneBranch;
	}

	public void setSchoolContantTypeId(int schoolContantTypeId) {
		this.schoolContantTypeId = schoolContantTypeId;
	}

	public void setSchoolContactStatusId(int schoolContactStatusId) {
		this.schoolContactStatusId = schoolContactStatusId;
	}

	public void setxId(String xId) {
		this.xId = xId;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getRgDigit() {
		return rgDigit;
	}

	public void setRgDigit(String rgDigit) {
		this.rgDigit = rgDigit;
	}

}
