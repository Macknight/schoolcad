package sv.school.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SPT_COTA")
public class Cota {
	@Id
	@Column(name = "COTA_CODE")
	private int cotaCode;

	@Column(name = "COTA_DESCRIPTION")
	private String cotaDescription;

	public int getCotaCode() {
		return cotaCode;
	}

	public String getCotaDescription() {
		return cotaDescription;
	}

	public void setCotaCode(int cotaCode) {
		this.cotaCode = cotaCode;
	}

	public void setCotaDescription(String cotaDescription) {
		this.cotaDescription = cotaDescription;
	}

}
