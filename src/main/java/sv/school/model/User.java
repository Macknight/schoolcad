package sv.school.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//CREATE TABLE SPT_USERS (
//	       USER_ID              INTEGER NOT NULL,
//	       NAME                 CHAR(70) NOT NULL,
//	       REGISTER_NUMBER      CHAR(15) NULL,
//	       RG_NUMBER            CHAR(14) NULL,
//	       RG_DIGIT             CHAR(2) NULL,
//	       RG_EMITTED_STATE     CHAR(2) NULL,
//	       RG_EMITTED_DT        DATE NULL,
//	       GENDER_TYPE          CHAR(1) NULL,
//	       BIRTH_DT             DATE NULL,
//	       COUNTRY_CODE         CHAR(2)  NULL,
//
//	       ADDRESS_STREET       CHAR(40) NULL,
//	       ADDRESS_NUMBER       CHAR(10) NULL,
//	       ADDRESS_AP_NUMBER    CHAR(10) NULL,
//	       ADDRESS_BLOCK_NUMBER CHAR(10) NULL,
//	       ADDRESS_COMPLEMENT   CHAR(10) NULL,
//	       ADDRESS_QUARTER      CHAR(20) NULL,
//	       ADDRESS_ZIP_CODE     CHAR(8) NULL,
//	       TELEPHONE            CHAR(15) NULL,
//	       TELEPHONE_MOBILE     CHAR(15) NULL,
//
//	       EMAIL                CHAR(50) NULL,
//	       EMAIL_OPTIONAL       CHAR(50) NULL,
//	       RESPONSIBLE          CHAR(40) NULL,
//	       REGISTERED_DT        DATE NULL,
//	       USER_STATUS_ID       INTEGER NULL,
//	       USER_TYPE_ID         INTEGER NULL,
//	       XID                  INTEGER NULL,
//	       PHOTO                BLOB NULL,
//	       COMMENTS             VARCHAR2(2000) NULL
//	);

@Entity
@Table(name = "SPT_USERS")
public class User implements Model, Comparable<User> {

	@Id
	@Column(name = "USER_ID")
	private int userId;

	@Column(name = "NAME")
	private String name;

	@Column(name = "REGISTER_NUMBER")
	private String registerName;

	@Column(name = "RG_NUMBER")
	private String rgNumber;

	@Column(name = "RG_DIGIT")
	private String rgDigit;

	@Column(name = "RG_EMITTED_STATE")
	private String rgEmittedState;

	@Column(name = "RG_EMITTED_DT")
	private Date rgEmittedDate;

	@Column(name = "GENDER_TYPE")
	private String gender;

	@Column(name = "BIRTH_DT")
	private Date birthDate;

	@Column(name = "COUNTRY_CODE")
	private String countryCode;

	@Column(name = "ADDRESS_STREET")
	private String adressStreet;

	@Column(name = "ADDRESS_NUMBER")
	private String adressNumber;

	@Column(name = "ADDRESS_AP_NUMBER")
	private String adressApNumber;

	@Column(name = "ADDRESS_BLOCK_NUMBER")
	private String adressBlockNumber;

	@Column(name = "ADDRESS_COMPLEMENT")
	private String adressComplement;

	@Column(name = "ADDRESS_QUARTER")
	private String adressQuarter;
	// ADRESS ZIP_CODE = CEP
	@Column(name = "ADDRESS_ZIP_CODE")
	private String adressZipCode;

	@Column(name = "TELEPHONE")
	private String telephone;

	@Column(name = "TELEPHONE_MOBILE")
	private String telephoneMobile;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "EMAIL_OPTIONAL")
	private String emailOptional;

	@Column(name = "RESPONSIBLE")
	private String responsible;

	@Column(name = "REGISTERED_DT")
	private Date registtrationDate;

	@Column(name = "USER_STATUS_ID")
	private int userStatusId;

	@Column(name = "USER_TYPE_ID")
	private int userTypeId;

	@Column(name = "XID")
	private int xId;

	@Column(name = "COMMENTS")
	private String comments;

	@Column(name = "CPF")
	private String cpf;

	@Column(name = "CPT_TYPE")
	private String cpfType;

	public String toJson() {
		return "userId:" + getUserId() + "userName:" + getName();
	}

	public int getUserId() {
		return userId;
	}

	public String getName() {
		return name;
	}

	public String getRegisterName() {
		return registerName;
	}

	public String getRgNumber() {
		return rgNumber;
	}

	public String getRgDigit() {
		return rgDigit;
	}

	public String getRgEmittedState() {
		return rgEmittedState;
	}

	public Date getRgEmittedDate() {
		return rgEmittedDate;
	}

	public String getGender() {
		return gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public String getAdressStreet() {
		return adressStreet;
	}

	public String getAdressNumber() {
		return adressNumber;
	}

	public String getAdressApNumber() {
		return adressApNumber;
	}

	public String getAdressBlockNumber() {
		return adressBlockNumber;
	}

	public String getAdressComplement() {
		return adressComplement;
	}

	public String getAdressQuarter() {
		return adressQuarter;
	}

	public String getAdressZipCode() {
		return adressZipCode;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getTelephoneMobile() {
		return telephoneMobile;
	}

	public String getEmail() {
		return email;
	}

	public String getEmailOptional() {
		return emailOptional;
	}

	public String getResponsible() {
		return responsible;
	}

	public Date getRegisttrationDate() {
		return registtrationDate;
	}

	public int getUserStatusId() {
		return userStatusId;
	}

	public int getUserTypeId() {
		return userTypeId;
	}

	public int getxId() {
		return xId;
	}

	public String getComments() {
		return comments;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRegisterName(String registerName) {
		this.registerName = registerName;
	}

	public void setRgNumber(String rgNumber) {
		this.rgNumber = rgNumber;
	}

	public void setRgDigit(String rgDigit) {
		this.rgDigit = rgDigit;
	}

	public void setRgEmittedState(String rgEmittedState) {
		this.rgEmittedState = rgEmittedState;
	}

	public void setRgEmittedDate(Date rgEmittedDate) {
		this.rgEmittedDate = rgEmittedDate;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public void setAdressStreet(String adressStreet) {
		this.adressStreet = adressStreet;
	}

	public void setAdressNumber(String adressNumber) {
		this.adressNumber = adressNumber;
	}

	public void setAdressApNumber(String adressApNumber) {
		this.adressApNumber = adressApNumber;
	}

	public void setAdressBlockNumber(String adressBlockNumber) {
		this.adressBlockNumber = adressBlockNumber;
	}

	public void setAdressComplement(String adressComplement) {
		this.adressComplement = adressComplement;
	}

	public void setAdressQuarter(String adressQuarter) {
		this.adressQuarter = adressQuarter;
	}

	public void setAdressZipCode(String adressZipCode) {
		this.adressZipCode = adressZipCode;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public void setTelephoneMobile(String telephoneMobile) {
		this.telephoneMobile = telephoneMobile;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmailOptional(String emailOptional) {
		this.emailOptional = emailOptional;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public void setRegisttrationDate(Date registtrationDate) {
		this.registtrationDate = registtrationDate;
	}

	public void setUserStatusId(int userStatusId) {
		this.userStatusId = userStatusId;
	}

	public void setUserTypeId(int userTypeId) {
		this.userTypeId = userTypeId;
	}

	public void setxId(int xId) {
		this.xId = xId;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public User(int userId, String name) {
		this.userId = userId;
		this.name = name;
	}

	public User() {
		this.name = "default User";
	}

	public User(int userId, String name, String registerName, String rgNumber,
			String rgDigit, String rgEmittedState, Date rgEmittedDate,
			String gender, Date birthDate, String countryCode,
			String adressStreet, String adressNumber, String adressApNumber,
			String adressBlockNumber, String adressComplement,
			String adressQuarter, String adressZipCode, String telephone,
			String telephoneMobile, String email, String emailOptional,
			String responsible, Date registtrationDate, int userStatusId,
			int userTypeId, int xId, String comments) {
		super();
		this.userId = userId;
		this.name = name;
		this.registerName = registerName;
		this.rgNumber = rgNumber;
		this.rgDigit = rgDigit;
		this.rgEmittedState = rgEmittedState;
		this.rgEmittedDate = rgEmittedDate;
		this.gender = gender;
		this.birthDate = birthDate;
		this.countryCode = countryCode;
		this.adressStreet = adressStreet;
		this.adressNumber = adressNumber;
		this.adressApNumber = adressApNumber;
		this.adressBlockNumber = adressBlockNumber;
		this.adressComplement = adressComplement;
		this.adressQuarter = adressQuarter;
		this.adressZipCode = adressZipCode;
		this.telephone = telephone;
		this.telephoneMobile = telephoneMobile;
		this.email = email;
		this.emailOptional = emailOptional;
		this.responsible = responsible;
		this.registtrationDate = registtrationDate;
		this.userStatusId = userStatusId;
		this.userTypeId = userTypeId;
		this.xId = xId;
		this.comments = comments;
	}

	public User(int userId, String name, String registerName, String rgNumber,
			String rgDigit, String rgEmittedState, Date rgEmittedDate,
			String gender, Date birthDate, String countryCode,
			String adressStreet, String adressNumber, String adressApNumber,
			String adressBlockNumber, String adressComplement,
			String adressQuarter, String adressZipCode, String telephone,
			String telephoneMobile, String email, String emailOptional,
			String responsible, Date registtrationDate, int userStatusId,
			int userTypeId, int xId, String comments, String cpf) {
		super();
		this.userId = userId;
		this.name = name;
		this.registerName = registerName;
		this.rgNumber = rgNumber;
		this.rgDigit = rgDigit;
		this.rgEmittedState = rgEmittedState;
		this.rgEmittedDate = rgEmittedDate;
		this.gender = gender;
		this.birthDate = birthDate;
		this.countryCode = countryCode;
		this.adressStreet = adressStreet;
		this.adressNumber = adressNumber;
		this.adressApNumber = adressApNumber;
		this.adressBlockNumber = adressBlockNumber;
		this.adressComplement = adressComplement;
		this.adressQuarter = adressQuarter;
		this.adressZipCode = adressZipCode;
		this.telephone = telephone;
		this.telephoneMobile = telephoneMobile;
		this.email = email;
		this.emailOptional = emailOptional;
		this.responsible = responsible;
		this.registtrationDate = registtrationDate;
		this.userStatusId = userStatusId;
		this.userTypeId = userTypeId;
		this.xId = xId;
		this.comments = comments;
		this.cpf = cpf;
	}

	public User(int userId, String name, String registerName, String rgNumber,
			String rgDigit, String rgEmittedState, Date rgEmittedDate,
			String gender, Date birthDate, String countryCode,
			String adressStreet, String adressNumber, String adressApNumber,
			String adressBlockNumber, String adressComplement,
			String adressQuarter, String adressZipCode, String telephone,
			String telephoneMobile, String email, String emailOptional,
			String responsible, Date registtrationDate, int userStatusId,
			int userTypeId, int xId, String comments, String cpf, String cpfType) {
		super();
		this.userId = userId;
		this.name = name;
		this.registerName = registerName;
		this.rgNumber = rgNumber;
		this.rgDigit = rgDigit;
		this.rgEmittedState = rgEmittedState;
		this.rgEmittedDate = rgEmittedDate;
		this.gender = gender;
		this.birthDate = birthDate;
		this.countryCode = countryCode;
		this.adressStreet = adressStreet;
		this.adressNumber = adressNumber;
		this.adressApNumber = adressApNumber;
		this.adressBlockNumber = adressBlockNumber;
		this.adressComplement = adressComplement;
		this.adressQuarter = adressQuarter;
		this.adressZipCode = adressZipCode;
		this.telephone = telephone;
		this.telephoneMobile = telephoneMobile;
		this.email = email;
		this.emailOptional = emailOptional;
		this.responsible = responsible;
		this.registtrationDate = registtrationDate;
		this.userStatusId = userStatusId;
		this.userTypeId = userTypeId;
		this.xId = xId;
		this.comments = comments;
		this.cpf = cpf;
		this.cpfType = cpfType;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void setCpfType(String cpfType) {
		this.cpfType = cpfType;
	}

	public String getCpf() {
		return this.cpf;
	}

	public String getCpfType() {
		return this.cpfType;
	}

	@Override
	public int compareTo(User o) {
		int userId = o.getUserId();
		return this.userId - o.getUserId();

	}

	public String toCVS() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");

		return this.userId
				+ ";"
				+ this.name
				+ ";"
				+ this.registerName
				+ ";"
				+ this.rgNumber
				+ ";"
				+ this.rgDigit
				+ ";"
				+ this.rgEmittedState
				+ ";"
				+ formatter.format(this.rgEmittedDate == null ? new Date()
						: this.rgEmittedDate)
				+ ";"
				+ this.gender
				+ ";"
				+ formatter.format(this.birthDate == null ? new Date()
						: this.birthDate)
				+ ";"
				+ this.countryCode
				+ ";"
				+ this.adressStreet
				+ ";"
				+ this.adressNumber
				+ ";"
				+ this.adressApNumber
				+ ";"
				+ this.adressBlockNumber
				+ ";"
				+ this.adressComplement
				+ ";"
				+ this.adressQuarter
				+ ";"
				+ this.adressZipCode
				+ ";"
				+ this.telephone
				+ ";"
				+ this.telephoneMobile
				+ ";"
				+ this.email
				+ ";"
				+ this.emailOptional
				+ ";"
				+ this.responsible
				+ ";"
				+ formatter.format(this.registtrationDate == null ? new Date()
						: this.registtrationDate) + ";" + this.userStatusId
				+ ";" + this.userTypeId + ";" + this.xId + ";" + this.comments
				+ ";" + this.cpf + ";" + this.cpfType + ";";

	}

}
