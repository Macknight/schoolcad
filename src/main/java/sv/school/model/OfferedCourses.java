package sv.school.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SPT_OFFERED_COURSES")
public class OfferedCourses implements Model {
	@Id
	@Column(name = "OFFERED_COURSE_ID")
	private long offeredCourseId;

	@Column(name = "SCHOOL_ID")
	private long schoolId;

	@Column(name = "COURSE_CODE")
	private String courseCode;

	@Column(name = "SCHEMA_ID")
	private int schemaId;

	@Column(name = "SYS_YEAR")
	private String sysYear;

	@Column(name = "PERIOD_TYPE_ID")
	private int periodTypeId;

	public static final int PERIOD_TYPE_ID_SEMESTRAL = 1;
	public static final int PERIOD_TYPE_ID_ANUAL = 2;
	public static final int PERIOD_TYPE_ID_ANUAL_AULA_EM_JULHO = 3;

	public long getOfferedCourseId() {
		return offeredCourseId;
	}

	public long getSchoolId() {
		return schoolId;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public int getSchemaId() {
		return schemaId;
	}

	public String getSysYear() {
		return sysYear;
	}

	public int getPeriodTypeId() {
		return periodTypeId;
	}

	public void setOfferedCourseId(long offeredCourseId) {
		this.offeredCourseId = offeredCourseId;
	}

	public void setSchoolId(long schoolId) {
		this.schoolId = schoolId;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public void setSchemaId(int schemaId) {
		this.schemaId = schemaId;
	}

	public OfferedCourses(long offeredCourseId, long schoolId,
			String courseCode, int schemaId, String sysYear, int periodTypeId) {
		super();
		this.offeredCourseId = offeredCourseId;
		this.schoolId = schoolId;
		this.courseCode = courseCode;
		this.schemaId = schemaId;
		this.sysYear = sysYear;
		this.periodTypeId = periodTypeId;
	}

	public OfferedCourses() {

	}

	public void setSysYear(String sysYear) {
		this.sysYear = sysYear;
	}

	public void setPeriodTypeId(int periodTypeId) {
		this.periodTypeId = periodTypeId;
	}

	@Override
	public String toJson() {

		return ("offeredCourseId = " + getOfferedCourseId() + "schoolId = "
				+ getSchoolId() + "courseCode = " + getCourseCode());
	}
}
