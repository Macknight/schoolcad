package sv.school.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//REGISTRATION_ID INTEGER NOT NULL,
//REGISTER_NUMBER CHAR(15),
//USER_ID INTEGER NULL,
//SCHOOL_ID INTEGER NULL,
//COURSE_CODE CHAR(10) NULL,
//SCHOOL_COURSE_ID INTEGER NULL,
//REGISTRATION_STATUS_ID INTEGER NOT NULL,
//MOVEMENT_TYPE_ID INTEGER NULL,
//SCHEMA_ID INTEGER NULL,
//REGISTRATION_DT DATE NULL,
//INITIAL_VALID_DT DATE NULL,
//FINAL_VALID_DT DATE NULL
@Entity
@Table(name = "SPT_REGISTRATION")
public class Registration implements Model {

	@Id
	@Column(name = "REGISTRATION_ID")
	private int registrationId;

	@Column(name = "REGISTER_NUMBER")
	private String registerNumber;

	@Column(name = "USER_ID")
	private int userId;

	@Column(name = "SCHOOL_ID")
	private int schoolId;

	@Column(name = "COURSE_CODE")
	private String courseCode;

	@Column(name = "SCHOOL_COURSE_ID")
	private int schoolCourseId;

	@Column(name = "REGISTRATION_STATUS_ID")
	private int registrationStatusId;

	@Column(name = "MOVEMENT_TYPE_ID")
	private int movementTypeId;

	@Column(name = "SCHEMA_ID")
	private int schemaId;

	@Column(name = "REGISTRATION_DT")
	private Date registrationDate;

	@Column(name = "INITIAL_VALID_DT")
	private Date initialValidDate;

	@Column(name = "FINAL_VALID_DT")
	private Date finalValidDate;

	@Column(name = "GRATUITY_TYPE")
	private int gratuityType;

	@Column(name = "GRATUITY_ACTIVE")
	private int gratuityActive;

	public Registration(int registrationId, String registerNumber, int userId,
			int schoolId, String courseCode, int schoolCourseId,
			int registrationStatusId, int movementTypeId, int schemaId,
			Date registrationDate, Date initialValidDate, Date finalValidDate) {
		super();
		this.registrationId = registrationId;
		this.registerNumber = registerNumber;
		this.userId = userId;
		this.schoolId = schoolId;
		this.courseCode = courseCode;
		this.schoolCourseId = schoolCourseId;
		this.registrationStatusId = registrationStatusId;
		this.movementTypeId = movementTypeId;
		this.schemaId = schemaId;
		this.registrationDate = registrationDate;
		this.initialValidDate = initialValidDate;
		this.finalValidDate = finalValidDate;
	}

	public Registration(int registrationId, String registerNumber, int userId,
			int schoolId, String courseCode, int schoolCourseId,
			int registrationStatusId, int movementTypeId, int schemaId,
			Date registrationDate, Date initialValidDate, Date finalValidDate,
			int gratuidade, int gratuidadeAtiva) {
		super();
		this.registrationId = registrationId;
		this.registerNumber = registerNumber;
		this.userId = userId;
		this.schoolId = schoolId;
		this.courseCode = courseCode;
		this.schoolCourseId = schoolCourseId;
		this.registrationStatusId = registrationStatusId;
		this.movementTypeId = movementTypeId;
		this.schemaId = schemaId;
		this.registrationDate = registrationDate;
		this.initialValidDate = initialValidDate;
		this.finalValidDate = finalValidDate;
		this.gratuityType = gratuidade;
		this.gratuityActive = gratuidadeAtiva;

	}

	public Registration() {

	}

	public int getGratuityType() {
		return this.gratuityType;
	}

	public int getGratuityActive() {
		return this.gratuityActive;
	}

	public Registration(int registrationId, String registerNumber) {
		super();
		this.registrationId = registrationId;
		this.registerNumber = registerNumber;
		this.userId = 0;
		this.schoolId = 0;
		this.courseCode = "5555";
		this.schoolCourseId = 0;
		this.registrationStatusId = 0;
		this.movementTypeId = 0;
		this.schemaId = 0;
		this.registrationDate = new Date();
		this.initialValidDate = new Date();
		this.finalValidDate = new Date();
	}

	public String toJson() {
		return "RegistrationId =" + getRegistrationId() + " registerNumber = "
				+ getRegisterNumber();
	}

	public int getRegistrationId() {
		return registrationId;
	}

	public String getRegisterNumber() {
		return registerNumber;
	}

	public int getUserId() {
		return userId;
	}

	public int getSchoolId() {
		return schoolId;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public int getSchoolCourseId() {
		return schoolCourseId;
	}

	public int getRegistrationStatusId() {
		return registrationStatusId;
	}

	public int getMovementTypeId() {
		return movementTypeId;
	}

	public int getSchemaId() {
		return schemaId;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public Date getInitialValidDate() {
		return initialValidDate;
	}

	public Date getFinalValidDate() {
		return finalValidDate;
	}

	public void setRegistrationId(int registrationId) {
		this.registrationId = registrationId;
	}

	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public void setSchoolCourseId(int schoolCourseId) {
		this.schoolCourseId = schoolCourseId;
	}

	public void setRegistrationStatusId(int registrationStatusId) {
		this.registrationStatusId = registrationStatusId;
	}

	public void setMovementTypeId(int movementTypeId) {
		this.movementTypeId = movementTypeId;
	}

	public void setSchemaId(int schemaId) {
		this.schemaId = schemaId;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public void setInitialValidDate(Date initialValidDate) {
		this.initialValidDate = initialValidDate;
	}

	public void setFinalValidDate(Date finalValidDate) {
		this.finalValidDate = finalValidDate;
	}

	// int registrationId, String registerNumber, int userId,
	// int schoolId, String courseCode, int schoolCourseId,
	// int registrationStatusId, int movementTypeId, int schemaId,
	// Date registrationDate, Date initialValidDate, Date finalValidDate
	public String  toCSV() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");

		return this.registrationId + ";"
				+ this.registerNumber + ";"
				+ this.userId + ";"
				+ this.schoolId + ";"
				+ this.courseCode + ";"
				+ this.schoolCourseId + ";"
				+ this.registrationStatusId + ";"
				+ this.movementTypeId + ";"
				+ this.schemaId + ";"
				+ formatter.format(registrationDate) + ";"
				+ formatter.format(initialValidDate) + ";"
				+ formatter.format(finalValidDate) + ";;" // xid???
				+ this.gratuityType + ";"
				+this.gratuityActive + ";"
				;
	}
}
