package sv.school.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import sv.school.App;
import sv.school.model.SchoolCourse;

public class SpecificSchoolCourseViewController {

	private static BorderPane rootLayout;
	@FXML
	private TableView<SchoolCourseComp> scTable;

	@FXML
	private TableColumn<SchoolCourse, String> scCode;
	@FXML
	private TableColumn<SchoolCourse, String> scdDescription;
	@FXML
	private TableColumn<SchoolCourse, String> scSeries;
	@FXML
	private TableColumn<SchoolCourse, String> scVigencia;
	@FXML
	private TextField courseCode;
	@FXML
	private TextField description;
	@FXML
	private TextField courseLevel;
	@FXML
	private TextField schoolPeriod;
	@FXML
	private Button bVoltar;

	@FXML
	private ComboBox<Integer> year;
	@FXML
	private ComboBox<String> semester;
	@FXML
	private Button bSchoolCourseDetails;

	private ComposedCourse selectCourse;

	private SchoolCourseComp selectSCCourse;

	private static List<SchoolCourse> allSCourses;

	static private RootLayoutController rootController;

	public static final String ANUAL = "0 - Anual";
	public static final String PRIMEIRO_SEMESTRE = "1 - 1\u00ba Semestre";
	public static final String SEGUNDO_SEMESTRE = "2 - 2\u00ba Semestre";

	public void setRootLayout(BorderPane rl) {
		this.rootLayout = rl;
	}

	public void showSelectedCourse(ComposedCourse selectedCourse) {
		courseCode.setText(selectedCourse.getCourseCode());
		description.setText(selectedCourse.getDescription());
		courseLevel.setText(selectedCourse.getSchoolarLevel());
		schoolPeriod.setText(selectedCourse.getVigencia());
		this.selectCourse = selectedCourse;
	}

	public void setSelectedCourse(ComposedCourse selectedCourse) {
		this.selectCourse = selectedCourse;

	}

	@FXML
	private void onClickVoltar() {
		RootLayoutController controller = new RootLayoutController();
		controller.voltarTurmas();
	}

	public void setCombosChoice() {
		// returns the current year - 2000 which is the sys_year format
		// for example: 15 stands for 2015
		Integer years = Calendar.getInstance().get(Calendar.YEAR) - 2000;

		// Applying content to the combo box
		// the user will only have access to school courses
		// from the current/past/next year (ie 2014,2015,2016 in 2015)
		year.getItems().add(years - 1);
		year.getItems().add(years);
		year.getItems().add(years + 1);
		year.setValue(years);

		// The user select the periodType of the schoolCourse
		// sets the default for the yearly one
		semester.getItems().add(ANUAL);
		semester.getItems().add(PRIMEIRO_SEMESTRE);
		semester.getItems().add(SEGUNDO_SEMESTRE);
		semester.setValue(ANUAL);
	}

	public void setRootLayoutController(RootLayoutController rlc) {
		this.rootController = rlc;
	}

	public void showSchoolCourses() {
		int period = 0;
		List<SchoolCourse> selectCourses = new ArrayList<SchoolCourse>();
		if (semester.getSelectionModel().getSelectedItem().equals(ANUAL)) {
			period = 0;
		} else if (semester.getSelectionModel().getSelectedItem()
				.equals(PRIMEIRO_SEMESTRE)) {
			period = 1;
		} else if (semester.getSelectionModel().getSelectedItem()
				.equals(SEGUNDO_SEMESTRE)) {
			period = 2;
		}

		for (SchoolCourse scCourse : allSCourses) {

			if (scCourse.getCourseCode().equals(selectCourse.getCourseCode())) {

				if (scCourse.getYear().equals(
						year.getSelectionModel().getSelectedItem().toString())
						&& scCourse.getPeriodId() == period) {

					selectCourses.add(scCourse);
				}
			}
		}

		List<SchoolCourseComp> sccCourses = new ArrayList<SchoolCourseComp>();

		for (SchoolCourse scCourse : selectCourses) {
			SchoolCourseComp sccCourse = new SchoolCourseComp(scCourse);
			sccCourses.add(sccCourse);
		}

		ObservableList<SchoolCourseComp> olCourse = FXCollections
				.observableArrayList(sccCourses);

		scCode.setCellValueFactory(new PropertyValueFactory<SchoolCourse, String>(
				"scCode"));
		scdDescription
				.setCellValueFactory(new PropertyValueFactory<SchoolCourse, String>(
						"scDescription"));
		scSeries.setCellValueFactory(new PropertyValueFactory<SchoolCourse, String>(
				"scSerie"));
		scVigencia
				.setCellValueFactory(new PropertyValueFactory<SchoolCourse, String>(
						"scVigencia"));

		scTable.setItems(olCourse);
		if (olCourse.size() == 0 || olCourse == null) {
			scTable.setPlaceholder(new Label("N\u00e3o h\u00e1 conte\u00fado"));
		}
		scTable.getSelectionModel()
				.selectedItemProperty()
				.addListener(
						(observable, oldValue, newValue) -> enableDetails(newValue));
	}

	public void balanceTable() {
		scCode.prefWidthProperty().bind(scTable.widthProperty().divide(4));
		scdDescription.prefWidthProperty().bind(
				scTable.widthProperty().divide(4));
		scSeries.prefWidthProperty().bind(scTable.widthProperty().divide(4));
		scVigencia.prefWidthProperty().bind(scTable.widthProperty().divide(4));
	}

	@FXML
	public void setComboBoxChangeSensitivity() {
		showSchoolCourses();

	}

	public void setSchoolCourses(List<SchoolCourse> courses) {
		this.allSCourses = courses;
	}

	@FXML
	public void onClickCourseDetails() {
		FXMLLoader loader = new FXMLLoader(
				App.class.getResource("view/SchoolCOurseDetails.fxml"));
		try {
			RootLayoutController.bNewCourse = false;
			AnchorPane apMiddle = loader.load();
			rootLayout.setCenter(apMiddle);
			SchoolCOurseDetailsController viewController = loader
					.getController();
			viewController.setRootController(rootController);

			viewController.displaySchoolCOurse(selectSCCourse);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void onClickNewCourse() {
		FXMLLoader loader = new FXMLLoader(
				App.class.getResource("view/SchoolCOurseDetails.fxml"));
		try {
			RootLayoutController.bNewCourse = true;
			AnchorPane apMiddle = loader.load();
			rootLayout.setCenter(apMiddle);
			SchoolCOurseDetailsController viewController = loader
					.getController();
			viewController.setRootController(rootController);

			viewController.prepareForNewCourse(selectCourse);
			rootController.prepareForNewCourse();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void disableDetais() {
		bSchoolCourseDetails.setDisable(true);
	}

	public void enableDetails(SchoolCourseComp scCourse) {
		if (scCourse != null) {
			this.selectSCCourse = scCourse;
			bSchoolCourseDetails.setDisable(false);
		}
	}

}
