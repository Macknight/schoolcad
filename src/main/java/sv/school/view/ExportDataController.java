package sv.school.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import sv.school.util.FileParser;

public class ExportDataController {
	@FXML
	private TextField tfDirectory;

	@FXML
	private Button bSelectDirectory;

	@FXML
	private Button bExportar;

	@FXML
	private Button bLimpar;
	@FXML
	private CheckBox cbExportAllData;
	@FXML
	private TextArea prompt;
	@FXML
	private ProgressBar progress;

	public void onClickSelectDirectory() {
		// chose the file when the button is clicked on the interface
		try {
			DirectoryChooser chooser = new DirectoryChooser();
			chooser.setTitle("Escola o caminho para exportar");
			String path = chooser.showDialog(null).getAbsolutePath();
			if (path != null) {
				tfDirectory.setText(path);
			}
		}catch (Exception e ){

			e.printStackTrace();
		}

	}

	public void onClickExportar() {
		FileParser parser = new FileParser();

		parser.exportDataToSptrans(tfDirectory.getText(), prompt, progress,
				cbExportAllData.isSelected());

		progress.progressProperty().bind(parser.exportationPorgress);
	}
}
