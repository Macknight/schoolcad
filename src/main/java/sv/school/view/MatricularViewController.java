package sv.school.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import sv.school.controller.SchoolCourseController;
import sv.school.model.SchoolCourse;

public class MatricularViewController {

	private static final String SEMESTRE_ANUAL = "0 - Anual";
	private static final String SEMESTRE_PRIMEIRO = "1 - 1\u00ba Semestre";
	private static final String SEMESTRE_SEGUNDO = "2 - 2\u00ba Semestre";

	@FXML
	private ComboBox<Integer> cbAnoLetivo;
	@FXML
	private ComboBox<String> cbSemestre;

	@FXML
	private TableView<SchoolCourseComp> tvCourses;

	@FXML
	private TableColumn<SchoolCourseComp, String> ID;
	@FXML
	private TableColumn<SchoolCourseComp, String> courseDesc;
	@FXML
	private TableColumn<SchoolCourseComp, String> turmaDesc;
	@FXML
	private TableColumn<SchoolCourseComp, String> semestre;
	@FXML
	private TableColumn<SchoolCourseComp, String> serie;

	@FXML
	private Button bSelecionar;
	private static RootLayoutController rootController;
	private static SchoolCourseComp selected;

	public void setRootController(RootLayoutController rootController) {
		MatricularViewController.rootController = rootController;
	}

	public void balanceTable() {
		ID.prefWidthProperty().bind(tvCourses.widthProperty().divide(5));
		courseDesc.prefWidthProperty()
				.bind(tvCourses.widthProperty().divide(5));
		turmaDesc.prefWidthProperty().bind(tvCourses.widthProperty().divide(5));
		semestre.prefWidthProperty().bind(tvCourses.widthProperty().divide(5));
		serie.prefWidthProperty().bind(tvCourses.widthProperty().divide(5));
	}

	private List<SchoolCourse> scCourses;
	private List<SchoolCourseComp> scCompCourses;

	public void prepareView() {
		int year = Calendar.getInstance().get(Calendar.YEAR) - 2000;
		if (cbAnoLetivo == null) {
			System.out.println("anoletivo null");
		} else {
			cbAnoLetivo.getItems().add(year - 1);
			cbAnoLetivo.getItems().add(year);
			cbAnoLetivo.getItems().add(year + 1);
			cbAnoLetivo.setValue(year);
		}
		if (cbSemestre == null) {
			System.out.println("sbsemestre null");
		} else {
			cbSemestre.getItems().add(SEMESTRE_ANUAL);
			cbSemestre.getItems().add(SEMESTRE_PRIMEIRO);
			cbSemestre.getItems().add(SEMESTRE_SEGUNDO);
			cbSemestre.setValue(SEMESTRE_PRIMEIRO);
		}

	}

	public void prepareSchoolCourses() {
		SchoolCourseController scController = new SchoolCourseController();
		scCourses = new ArrayList<SchoolCourse>();
		scCompCourses = new ArrayList<SchoolCourseComp>();

		scCourses = scController.getAll();

		for (SchoolCourse schoolCourse : scCourses) {
			SchoolCourseComp sccCourse = new SchoolCourseComp(schoolCourse);
			scCompCourses.add(sccCourse);
		}
	}

	@FXML
	public void setComboBoxChangeSensitivity() {
		showSchoolCourses();

	}

	public void showSchoolCourses() {
		int sysYear = cbAnoLetivo.getSelectionModel().getSelectedItem();

		int periodId = 0;
		List<SchoolCourseComp> coursesToShow = new ArrayList<SchoolCourseComp>();

		if (cbSemestre.getSelectionModel().getSelectedItem()
				.equals(SEMESTRE_ANUAL)) {
			periodId = 0;
		} else if (cbSemestre.getSelectionModel().getSelectedItem()
				.equals(SEMESTRE_PRIMEIRO)) {
			periodId = 1;
		} else if (cbSemestre.getSelectionModel().getSelectedItem()
				.equals((SEMESTRE_SEGUNDO))) {
			periodId = 2;
		}

		for (SchoolCourseComp schoolCourseComp : scCompCourses) {
			SchoolCourse scOriginal = schoolCourseComp.getScOriginal();
			if (Integer.parseInt(scOriginal.getYear()) == sysYear
					&& scOriginal.getPeriodId() == periodId) {
				coursesToShow.add(schoolCourseComp);

			}

		}// this for gets the courses that matches the criteria selected by the
			// user

		ObservableList<SchoolCourseComp> olCourse = FXCollections
				.observableArrayList(coursesToShow);

		ID.setCellValueFactory(new PropertyValueFactory<SchoolCourseComp, String>(
				"schoolCourseCode"));
		courseDesc
				.setCellValueFactory(new PropertyValueFactory<SchoolCourseComp, String>(
						"courseDescrption"));
		turmaDesc
				.setCellValueFactory(new PropertyValueFactory<SchoolCourseComp, String>(
						"scDescription"));
		semestre.setCellValueFactory(new PropertyValueFactory<SchoolCourseComp, String>(
				"semestre"));
		serie.setCellValueFactory(new PropertyValueFactory<SchoolCourseComp, String>(
				"scSerie"));
		tvCourses.setItems(olCourse);

		tvCourses
				.getSelectionModel()
				.selectedItemProperty()
				.addListener(
						(observable, oldValue, newValue) -> enableSelecionar(newValue));
	}

	public void enableSelecionar(SchoolCourseComp selectedSchoolCourse) {
		// TODO Auto-generated method stub
		if (selectedSchoolCourse != null) {
			bSelecionar.setDisable(false);
			selected = selectedSchoolCourse;
		}
	}

	@FXML
	public void onClickSelecionar() {
		rootController.showAssociarMatricula(selected);
	}

}
