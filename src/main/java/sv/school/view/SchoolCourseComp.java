package sv.school.view;

import sv.school.controller.CourseController;
import sv.school.model.Course;
import sv.school.model.SchoolCourse;

/**
 * Auxiliar class to display the schoolCourses to the user with user friedly
 * strings
 * 
 * @author rodrigo.poloni
 *
 */
public class SchoolCourseComp {

	String scCode;
	String scDescription;
	String scSerie;
	String scVigencia;
	String schoolCourseCode;

	String semestre;
	String courseDescrption;
	private SchoolCourse scOriginal;
	private Course cRelated;

	public String getSchoolCourseCode() {
		return schoolCourseCode;
	}

	public String getSemestre() {
		return semestre;
	}

	public String getCourseDescrption() {
		return courseDescrption;
	}

	public void setSchoolCourseCode(String schoolCourseCode) {
		this.schoolCourseCode = schoolCourseCode;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	public void setCourseDescrption(String courseDescrption) {
		this.courseDescrption = courseDescrption;
	}

	public String getScCode() {
		return scCode;
	}

	public String getScDescription() {
		return scDescription;
	}

	public String getScSerie() {
		return scSerie;
	}

	public String getScVigencia() {
		return scVigencia;
	}

	public void setScCode(String scCode) {
		this.scCode = scCode;
	}

	public void setScDescription(String scDescription) {
		this.scDescription = scDescription;
	}

	public void setScSerie(String scSerie) {
		this.scSerie = scSerie;
	}

	public void setScVigencia(String scVigencia) {
		this.scVigencia = scVigencia;
	}

	public SchoolCourseComp(SchoolCourse scCourse) {
		scOriginal = scCourse;
		scCode = "" + scCourse.getSchoolCoursePK().getSchoolCourseId();
		scDescription = scCourse.getDescription();
		scSerie = "" + scCourse.getPeriodId();
		schoolCourseCode = ""
				+ scCourse.getSchoolCoursePK().getSchoolCourseId();
		switch (scCourse.getVigenciaTypeId()) {
		case 1:
			this.scVigencia = "Semestral";
			break;
		case 2:
			this.scVigencia = "Anual";
			break;
		case 3:
			this.scVigencia = "Anual com aula em julho";
			break;
		default:
			this.scVigencia = "Desconhecido";
			break;
		}

		semestre = "1� e 2�";
		if (scCourse.getPeriodId() == 0) {
			semestre = "1� e 2�";
		} else if (scCourse.getPeriodId() == 1) {
			semestre = "1�";
		} else if (scCourse.getPeriodId() == 2) {
			semestre = "2�";
		}
		getRelatedCourse();
		courseDescrption = cRelated.getDescription();
	}

	public SchoolCourse getScOriginal() {
		return scOriginal;
	}

	public void setScOriginal(SchoolCourse scOriginal) {
		this.scOriginal = scOriginal;
	}

	public void getRelatedCourse() {
		CourseController controller = new CourseController();
		cRelated = controller.getById(scOriginal.getSchoolCoursePK()
				.getCourseCode());
	}

	public Course getCourseOriginal() {
		return cRelated;
	}
}
