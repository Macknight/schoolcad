package sv.school.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import sv.school.App;
import sv.school.controller.CourseController;
import sv.school.controller.SchoolCourseController;
import sv.school.model.Course;
import sv.school.model.OfferedCourses;
import sv.school.model.SchoolCourse;

public class TurmasController {
	@FXML
	private Button bSsearchCourses;
	@FXML
	private Button bShowSchoolCourses;
	@FXML
	private TableView<ComposedCourse> tvCoursesTable;
	@FXML
	private TableColumn<ComposedCourse, String> tcCourseCode;
	@FXML
	private TableColumn<ComposedCourse, String> tcDescription;
	@FXML
	private TableColumn<ComposedCourse, String> tcSchoolLevel;
	@FXML
	private TableColumn<ComposedCourse, String> tcPeriodType;
	@FXML
	private TableColumn<ComposedCourse, String> tcSchema;
	@FXML
	private TextField tfSearchField;

	private static RootLayoutController rootController;
	private ComposedCourse selectedCourse;
	private static BorderPane rootLayout;

	/*
	 * allCourses = all courses from the MEC insitritution, its a semi-static
	 * table that is delivered from the SPTRANS
	 * 
	 * allCompCourses = the composition from the courses + offeredCourses +
	 * schema + sys_domain_values this class brings together all the data
	 * required to be displayed in the 'turmas' screen
	 * 
	 * allOfferedCourses = all the current classses available for the students
	 * to register
	 */
	private static List<Course> allCourses;
	private static List<ComposedCourse> allCompCourses;
	private static List<OfferedCourses> allOfferedCourses;

	public void setOfferedCourses(List<OfferedCourses> ofCourses) {
		this.allOfferedCourses = ofCourses;
	}

	public void generateCompositeCourses() {
		allCompCourses = new ArrayList<ComposedCourse>();
		if (allOfferedCourses != null) {
			for (OfferedCourses ofCourse : allOfferedCourses) {
				CourseController cController = new CourseController();
				Course assosciatedCourse = cController.getById(ofCourse
						.getCourseCode());

				if (assosciatedCourse != null) {
					ComposedCourse compCourse = new ComposedCourse(
							ofCourse.getCourseCode(),
							assosciatedCourse.getDescription(),
							assosciatedCourse.getSchoolLevelCodeId(),
							ofCourse.getPeriodTypeId(), ofCourse.getSchemaId(),
							assosciatedCourse, ofCourse);
					allCompCourses.add(compCourse);
				}
			}
		}
	}

	@FXML
	public void searchCourses() {
		generateCompositeCourses();

		String searchValue = tfSearchField.getText();
		List<ComposedCourse> matchedCourses = new ArrayList<ComposedCourse>();
		/**
		 * checks if the string coming from the search field is empty, case
		 * true: show all courses case false: uses the string from the search
		 * field to filter the courses that are being shownd to the user
		 * 
		 */
		this.bShowSchoolCourses.setDisable(true);
		if (searchValue.toUpperCase().equals("")) {
			ObservableList<ComposedCourse> olCourse = FXCollections
					.observableArrayList(allCompCourses);

			tcCourseCode
					.setCellValueFactory(new PropertyValueFactory<ComposedCourse, String>(
							"courseCode"));
			// tcCourseCode
			// .setCellValueFactory(new
			// Callback<TableColumn.CellDataFeatures<Film, String>,
			// ObservableValue<String>>() {
			// @Override
			// public ObservableValue<String> call(
			// TableColumn.CellDataFeatures<Film, String> film) {
			// SimpleStringProperty property = new SimpleStringProperty();
			// DateFormat dateFormat = new SimpleDateFormat(
			// "dd/MM/yyyy");
			// property.setValue(dateFormat.format(film.getValue()
			// .getCreatedDate()));
			// return property;
			// }
			// });

			tcDescription
					.setCellValueFactory(new PropertyValueFactory<ComposedCourse, String>(
							"description"));

			tcSchoolLevel
					.setCellValueFactory(new PropertyValueFactory<ComposedCourse, String>(
							"schoolarLevel"));
			tcPeriodType
					.setCellValueFactory(new PropertyValueFactory<ComposedCourse, String>(
							"vigencia"));

			tcSchema.setCellValueFactory(new PropertyValueFactory<ComposedCourse, String>(
					"schema"));

			tvCoursesTable.setItems(olCourse);

		} else {
			for (ComposedCourse course : allCompCourses) {
				if (course.getDescription().contains(searchValue.toUpperCase())) {
					matchedCourses.add(course);
				}
			}
			ObservableList<ComposedCourse> olCourse = FXCollections
					.observableArrayList(matchedCourses);

			tcCourseCode
					.setCellValueFactory(new PropertyValueFactory<ComposedCourse, String>(
							"courseCode"));

			tcDescription
					.setCellValueFactory(new PropertyValueFactory<ComposedCourse, String>(
							"description"));

			tcSchoolLevel
					.setCellValueFactory(new PropertyValueFactory<ComposedCourse, String>(
							"schoolarLevel"));
			tcPeriodType
					.setCellValueFactory(new PropertyValueFactory<ComposedCourse, String>(
							"vigencia"));

			tcSchema.setCellValueFactory(new PropertyValueFactory<ComposedCourse, String>(
					"schema"));
			tvCoursesTable.setItems(olCourse);
		}
		tvCoursesTable
				.getSelectionModel()
				.selectedItemProperty()
				.addListener(
						(observable, oldValue, newValue) -> enableMostrarTurmas(newValue));
		// tcSchema.setCellValueFactory(new PropertyValueFactory<Course,
		// String>(
		// "courseCode"));

		/*
		 * TODO: find out how schema rules (where does it come from? how does it
		 * is configured to display)
		 */

	}

	/**
	 * sets the textField sensitivity to the Enter key
	 */
	public void setEnterKeySensibility() {
		tfSearchField.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					searchCourses();
				}
			}
		});

	}

	private void enableMostrarTurmas(ComposedCourse newCourse) {
		if (newCourse != null) {
			this.bShowSchoolCourses.setDisable(false);
			this.selectedCourse = newCourse;
		}
	}

	public void seaarchCourses() {
		for (Course course : allCourses) {
			System.out.println(course.toJson());
		}
	}

	public void setCourseList(List<Course> courseList) {
		allCourses = courseList;

	}

	public void disableTurmasButton() {
		this.bShowSchoolCourses.setDisable(true);

	}

	public void setAutomaticSearch(String desc) {
		tfSearchField.setText(desc);
	}

	public void onClickMostrarTurmas() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(App.class
				.getResource("view/SpecificSchoolCourseView.fxml"));

		try {
			SchoolCourseController scController = new SchoolCourseController();
			List<SchoolCourse> allSCourses = scController.getAll();
			AnchorPane center = (AnchorPane) loader.load();
			rootLayout.setCenter(center);
			SpecificSchoolCourseViewController controller = loader
					.getController();
			rootController.setSpecificCourseController(controller);
			controller.setRootLayoutController(rootController);
			controller.setSelectedCourse(selectedCourse);
			controller.showSelectedCourse(selectedCourse);
			controller.setCombosChoice();
			controller.balanceTable();
			controller.setSchoolCourses(allSCourses);
			controller.showSchoolCourses();
			controller.setRootLayout(rootLayout);
			controller.disableDetais();
			rootController.enableSearchButton();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void setRootLayout(BorderPane root) {
		rootLayout = root;
	}

	public void setRootLayoutController(RootLayoutController roootController) {
		rootController = roootController;
	}

	public void balanceTableColumns() {
		tcCourseCode.prefWidthProperty().bind(
				tvCoursesTable.widthProperty().divide(6));
		tcDescription.prefWidthProperty().bind(
				tvCoursesTable.widthProperty().divide(6).multiply(2));
		tcSchoolLevel.prefWidthProperty().bind(
				tvCoursesTable.widthProperty().divide(6));
		tcPeriodType.prefWidthProperty().bind(
				tvCoursesTable.widthProperty().divide(6));
		tcSchema.prefWidthProperty().bind(
				tvCoursesTable.widthProperty().divide(6));

	}

}
