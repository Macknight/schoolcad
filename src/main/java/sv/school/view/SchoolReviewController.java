package sv.school.view;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import sv.school.model.School;

public class SchoolReviewController {
	@FXML
	private TextField tfSchoolId;
	@FXML
	private TextField tfSchoolName;
	@FXML
	private TextField tfSchoolMecNumber;
	@FXML
	private TextField tfSchoolFantasyName;
	@FXML
	private TextField tfSchoolAdress;
	@FXML
	private TextField tfSchoolNumber;
	@FXML
	private TextField tfSchoolComplement;
	@FXML
	private TextField tfSchoolAdressQuarter;
	@FXML
	private TextField tfSchoolPhone;
	@FXML
	private TextField tfSchoolEmail;

	private School appSchool;

	private ImportController importController;

	public void setImportController(ImportController importController) {
		this.importController = importController;
	}

	public void setSchoolFields(School sSchool) {
		this.tfSchoolId.setText(Integer.toString(sSchool.getSchoolId()));
		this.tfSchoolName.setText(sSchool.getName());
		this.tfSchoolMecNumber.setText(sSchool.getMecNumber());
		this.tfSchoolFantasyName.setText(sSchool.getFantasyName());
		this.tfSchoolAdress.setText(sSchool.getAddressStreet());
		this.tfSchoolNumber.setText(sSchool.getAddressNumber());
		this.tfSchoolComplement.setText(sSchool.getAddressComplement());
		this.tfSchoolPhone.setText(sSchool.getTelephoneNumber());
		this.tfSchoolEmail.setText(sSchool.getEmail());
		this.tfSchoolAdressQuarter.setText(sSchool.getAddressQuarter());

	}

	public void setAppSChool(School school) {
		this.appSchool = school;
	}

	public School updateSchoolFromInterface() {
		this.appSchool.setSchoolId(Integer.parseInt(tfSchoolId.getText()));
		this.appSchool.setName(tfSchoolName.getText());
		this.appSchool.setMecNumber(tfSchoolMecNumber.getText());
		this.appSchool.setFantasyName(tfSchoolFantasyName.getText());
		this.appSchool.setAddressStreet(tfSchoolAdress.getText());
		this.appSchool.setAddressNumber(tfSchoolNumber.getText());
		this.appSchool.setAddressComplement(tfSchoolComplement.getText());
		this.appSchool.setAddressQuarter(tfSchoolAdressQuarter.getText());
		this.appSchool.setTelephoneNumber(tfSchoolPhone.getText());
		this.appSchool.setEmail(tfSchoolEmail.getText());
		return this.appSchool;
	}
}
