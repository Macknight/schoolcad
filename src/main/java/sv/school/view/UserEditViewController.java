package sv.school.view;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import sv.school.controller.CEPController;
import sv.school.controller.RegistrationController;
import sv.school.controller.SchoolController;
import sv.school.controller.SchoolCourseController;
import sv.school.controller.UserController;
import sv.school.model.CEP;
import sv.school.model.Registration;
import sv.school.model.School;
import sv.school.model.SchoolCourse;
import sv.school.model.SchoolCourse.SchoolCoursePK;
import sv.school.model.User;

/*
 * This class controls the userEditScreens
 */
public class UserEditViewController {
	@FXML
	private TextField userId;
	@FXML
	private TextField matricula;
	@FXML
	private TextField nome;
	@FXML
	private DatePicker birthDate;
	@FXML
	private TextField RG;
	@FXML
	private TextField rgDigit;
	@FXML
	private TextField estadoEmissor;
	@FXML
	private DatePicker emissionDate;
	@FXML
	private ComboBox<String> sexo;
	@FXML
	private TextField cpf;
	@FXML
	private ComboBox<String> cpfType;
	@FXML
	private TextField nacionalidade;
	@FXML
	private TextField responsavel;
	@FXML
	private TextField cep;
	@FXML
	private TextField endereco;
	@FXML
	private TextField numeroEndereco;
	@FXML
	private TextField numeroApartamento;
	@FXML
	private TextField bloco;
	@FXML
	private TextField complemento;
	@FXML
	private TextField bairro;
	@FXML
	private TextField cidade;
	@FXML
	private TextField telefone;
	@FXML
	private TextField celular;
	@FXML
	private TextField email;
	@FXML
	private TextField instituicaoEnsino;
	@FXML
	private TextField dadosCurso;
	@FXML
	private TextField inicioAnoLetivo;
	@FXML
	private TextField fimAnoLetivo;

	@FXML
	private Button bProcurar;
	@FXML
	private Button bEditar;
	@FXML
	private Button bSalvar;
	@FXML
	private Button bCancelar;
	@FXML
	private Button bApagar;
	@FXML
	private Button bNovo;
	@FXML
	private Label registrationWarning;
	@FXML
	private Button cepSearch;

	/*
	 * Following are the objects that references the confirm registration screen
	 */

	private static UserEditViewController userInfoController;

	@FXML
	private Button bCRConfirmar;

	@FXML
	private Button bCRCancelar;

	@FXML
	private Button bCREditar;

	@FXML
	private Button bCRSalvar;

	@FXML
	private Button bCRDesfazer;

	public void onClickCepSearch() {
		System.out.println("onclickcepsearch");
		String cepSearch = cep.getText();
		System.out.println(cepSearch);
		if (cepSearch != null && !cepSearch.equals("")) {
			System.out.println("entrou");
			CEPController controller = new CEPController();
			CEP cCep = controller.getById(Long.parseLong(cepSearch));
			if (cCep != null) {
				System.out.println("possui em cep");
				endereco.setText(cCep.getLogradouro());
				bairro.setText(cCep.getBairro());
				cidade.setText(cCep.getCidade());

			} else {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setContentText("Este CEP n�o existe)!");
				alert.showAndWait();
			}

		}
	}

	public void enableCepSearchButton() {
		this.cepSearch.setDisable(false);
	}

	public void disableConfirmationButtons() {

		bCRSalvar.setDisable(true);
		bCRDesfazer.setDisable(true);

	}

	public void onCLickbCRConfirmar() {

		// amController.setUserRegistration( this.getEditedUser());
		amController.registerUser(this.getEditedUser());
	}

	private static AssociarMatriculaController amController;

	private static boolean isNew = false;

	private static User original;

	private static RootLayoutController rootController;

	private static UserEditViewController center;

	public void setOriginalUser(User original) {
		this.original = original;
	}

	public void setRootController(RootLayoutController rootController) {
		this.rootController = rootController;
	}

	public void setCenterReference(UserEditViewController center) {
		this.center = center;
	}

	public void disableButtons() {
		// bProcurar.setDisable(true);
		// bEditar.setDisable(true);
		bSalvar.setDisable(true);
		bCancelar.setDisable(true);
		bApagar.setDisable(true);
	}

	public void onClickVoltar() {
		rootController.onClickVoltar();
	}

	public void onCLickAjuda() {
		rootController.onClickAjuda();
	}

	public void onClickSair() {
		rootController.onClickSaida();
	}

	public void enableUserEdit() {
		userId.setEditable(true);

		matricula.setEditable(true);

		nome.setEditable(true);

		birthDate.setEditable(true);
		birthDate.setDisable(false);

		RG.setEditable(true);

		rgDigit.setEditable(true);

		estadoEmissor.setEditable(true);

		emissionDate.setEditable(true);
		emissionDate.setDisable(false);

		sexo.setEditable(true);
		sexo.setDisable(false);

		cpf.setEditable(true);

		cpfType.setEditable(true);

		// nacionalidade.setEditable(true);

		responsavel.setEditable(true);

		cep.setEditable(true);

		endereco.setEditable(true);

		numeroEndereco.setEditable(true);

		numeroApartamento.setEditable(true);

		bloco.setEditable(true);

		complemento.setEditable(true);

		bairro.setEditable(true);

		cidade.setEditable(true);

		telefone.setEditable(true);

		celular.setEditable(true);

		email.setEditable(true);

		// instituicaoEnsino.setEditable(true);

		// dadosCurso.setEditable(true);

		// inicioAnoLetivo.setEditable(true);

		// /fimAnoLetivo.setEditable(true);
	}

	public void prepareForNewUser() {
		isNew = true;
		userId.setDisable(false);

		matricula.setDisable(false);

		nome.setDisable(false);

		birthDate.setDisable(false);

		RG.setDisable(false);

		rgDigit.setDisable(false);

		estadoEmissor.setDisable(false);

		emissionDate.setDisable(false);

		// ComboBox<String> sexo;

		cpf.setDisable(false);

		// ComboBox<String> cpfType;

		nacionalidade.setDisable(false);

		responsavel.setDisable(false);

		cep.setDisable(false);

		endereco.setDisable(false);

		numeroEndereco.setDisable(false);

		numeroApartamento.setDisable(false);

		bloco.setDisable(false);

		complemento.setDisable(false);

		bairro.setDisable(false);

		cidade.setDisable(false);

		telefone.setDisable(false);

		celular.setDisable(false);

		email.setDisable(false);

		instituicaoEnsino.setDisable(false);

		dadosCurso.setDisable(false);

		inicioAnoLetivo.setDisable(false);

		fimAnoLetivo.setDisable(false);

		cepSearch.setDisable(false);

	}

	public void displayUser(User selected) {
		this.userInfoController = this;
		original = selected;
		isNew = false;
		RegistrationController regController = new RegistrationController();

		List<Registration> allRegistration = regController.getAll();

		for (Registration registration : allRegistration) {
			if (registration.getUserId() == selected.getUserId()) {
				registrationWarning.setText("Aluno matr\u00edculado!");
				SchoolCourseController scController = new SchoolCourseController();
				SchoolCourse selectedSchoolCourse = scController
						.getById(new SchoolCoursePK(registration.getSchoolId(),
								registration.getCourseCode(), registration
										.getSchoolCourseId()));
				SchoolController schoolController = new SchoolController();
				School thisOne = schoolController.getAll().get(0);
				instituicaoEnsino.setText(thisOne.getFantasyName());

				dadosCurso.setText(selectedSchoolCourse.getDescription());

				inicioAnoLetivo.setText(selectedSchoolCourse
						.getValidInitialDate().toLocaleString()
						.substring(0, 11));

				fimAnoLetivo.setText(selectedSchoolCourse.getValidFinalDate()
						.toLocaleString().substring(0, 11));
			}
		}

		userId.setText(selected.getUserId() + "");
		;

		matricula.setText(selected.getRegisterName());

		nome.setText(selected.getName());

		LocalDate birthLocalDate = selected.getBirthDate().toInstant()
				.atZone(ZoneId.systemDefault()).toLocalDate();
		birthDate.setValue(birthLocalDate);

		birthDate.setDisable(true);

		RG.setText(selected.getRgNumber());

		rgDigit.setText(selected.getRgDigit());

		estadoEmissor.setText(selected.getRgDigit());

		LocalDate emissionLocalDate = selected.getRgEmittedDate().toInstant()
				.atZone(ZoneId.systemDefault()).toLocalDate();
		emissionDate.setValue(emissionLocalDate);
		emissionDate.setDisable(true);
		sexo.getItems().addAll("Masculino", "Feminino");
		if (selected.getGender().equals("M")) {
			sexo.getSelectionModel().select(0);
		} else {
			System.out.println(selected.getGender());
			sexo.getSelectionModel().select(1);
		}
		sexo.setDisable(true);

		// Cpf is optional as a Sptrans request
		if (selected.getCpf() != null) {
			cpf.setText(selected.getCpf());
		}
		// ComboBox<String> cpfType;

		if (selected.getCpfType() != null) {
			cpfType.getItems().addAll("Titular", "Dependente");
			if (selected.getCpfType() != null
					&& selected.getCpfType().equals("T")) {
				cpfType.getSelectionModel().selectFirst();
			}
		} else {
			cpfType.getItems().addAll("N�o possui");
		}
		cpfType.setDisable(true);

		nacionalidade.setText("Brasileiro");

		responsavel.setText(selected.getResponsible());
		;

		// the cep is used to find the user adress
		cep.setText(selected.getAdressZipCode());

		endereco.setText(selected.getAdressStreet());

		numeroEndereco.setText(selected.getAdressNumber());

		numeroApartamento.setText(selected.getAdressApNumber());

		bloco.setText(selected.getAdressBlockNumber());

		complemento.setText(selected.getAdressComplement());

		bairro.setText(selected.getAdressQuarter());
		// city wtf?
		cidade.setText(selected.getAdressStreet());

		telefone.setText(selected.getTelephone());

		celular.setText(selected.getTelephoneMobile());

		email.setText(selected.getEmail());

		// instituicaoEnsino.setDisable(false);

		// dadosCurso.setDisable(false);

		// inicioAnoLetivo.setDisable(false);

		// fimAnoLetivo.setDisable(false);
		// cepSearch.setDisable(true);
	}

	public void onClickEditarAluno() {
		bProcurar.setDisable(true);
		bNovo.setDisable(true);
		bEditar.setDisable(true);
		bSalvar.setDisable(false);
		bCancelar.setDisable(false);
		bApagar.setDisable(false);
		userInfoController.enableCepSearchButton();
		center.enableUserEdit();

	}

	public User getEditedUser() {
		User oe = original;
		oe.setAdressApNumber(numeroApartamento.getText());
		oe.setAdressBlockNumber(bloco.getText());
		oe.setAdressComplement(complemento.getText());
		oe.setAdressNumber(numeroEndereco.getText());
		oe.setAdressQuarter(bairro.getText());
		oe.setAdressStreet(endereco.getText());
		oe.setAdressZipCode(cep.getText());
		// oe.setBirthDate(birthDate);
		// oe.setComments(comments);
		oe.setEmail(email.getText());
		oe.setName(nome.getText());
		oe.setTelephone(telefone.getText());
		oe.setResponsible(responsavel.getText());
		return oe;
	}

	public void onClickSalvar() {
		if (isNew == false) {
			// updating user
			User editeduser = center.getEditedUser();
			UserController controller = new UserController();
			controller.update(editeduser);
			controller.commit();
			bProcurar.setDisable(false);
			bNovo.setDisable(false);
			bEditar.setDisable(false);
			bSalvar.setDisable(true);
			bCancelar.setDisable(true);
			bApagar.setDisable(true);
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Usu�rio salvo");
			alert.setHeaderText(null);
			alert.setContentText("O usu�rio foi salvo com sucesso!");

			alert.showAndWait();
		} else {
			// saving new user
			int id = 0;
			UserController controller = new UserController();
			List<User> allUsers = controller.getAll();
			if (allUsers.size() == 0) {
				id = 1; // no users yet
			}
			Collections.sort(allUsers);

			User lastOne = allUsers.get(allUsers.size() - 1);
			id = lastOne.getUserId() + 1;

			User newUser = center.getUserFromFields();
			newUser.setUserId(id);
			controller.persist(newUser);
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Novo Usu�rio salvo");
			alert.setHeaderText(null);
			alert.setContentText("O Novo usu�rio foi salvo com sucesso!");

			alert.showAndWait();
			controller.commit();
		}
		bCancelar.setDisable(true);
		bApagar.setDisable(true);
		bSalvar.setDisable(true);
		rootController.onClickMostrarAlunos();
		rootController.reloadMainMenu();

	}

	private User getUserFromFields() {
		User oe = new User();
		oe.setAdressApNumber(numeroApartamento.getText());
		oe.setAdressBlockNumber(bloco.getText());
		oe.setAdressComplement(complemento.getText());
		oe.setAdressNumber(numeroEndereco.getText());
		oe.setAdressQuarter(bairro.getText());
		oe.setAdressStreet(endereco.getText());
		oe.setAdressZipCode(cep.getText());
		// oe.setBirthDate(birthDate);
		// oe.setComments(comments);
		oe.setEmail(email.getText());
		oe.setName(nome.getText());
		oe.setTelephone(telefone.getText());
		oe.setResponsible(responsavel.getText());
		return oe;
	}

	public void onClickCancelar() {
		if (isNew) {
			rootController.onClickMostrarAlunos();
			rootController.reloadMainMenu();

		}
		if (original == null) {
			rootController.onClickMostrarAlunos();
			rootController.reloadMainMenu();
		} else {
			center.displayUser(original);
			bProcurar.setDisable(false);
			bNovo.setDisable(false);
			bEditar.setDisable(false);
			bSalvar.setDisable(true);
			bCancelar.setDisable(true);
			bApagar.setDisable(true);
		}
	}

	public void onClickApagar() {
		UserController controller = new UserController();
		Alert redAlert = new Alert(AlertType.CONFIRMATION);
		redAlert.setTitle("Confirmar exclus�o de usu�rio");
		redAlert.setHeaderText(null);
		redAlert.setContentText("Voc� tem certeza que deseja deletar este usu�rio?? Isto n�o poder� ser desfeito.");

		Optional<ButtonType> result = redAlert.showAndWait();
		if (result.get() == ButtonType.OK) {
			controller.delete(original);
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Usu�rio deletado");
			alert.setHeaderText(null);
			alert.setContentText("O usu�rio foi exclu�do com sucesso!");

			alert.showAndWait();
			rootController.onClickMostrarAlunos();
			rootController.reloadMainMenu();

		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Usu�rio n�o deletado");
			alert.setHeaderText(null);
			alert.setContentText("Voc� cancelou a exclus�o do usu�rio!");

			alert.showAndWait();
		}

	}

	public void clearData() {

		userId.setText("");

		matricula.setText("");

		nome.setText("");

		// birthDate.setText("");

		RG.setText("");

		rgDigit.setText("");

		estadoEmissor.setText("");

		// emissionDate.setText("");

		// ComboBox<String> sexo;

		cpf.setText("");

		// ComboBox<String> cpfType;

		nacionalidade.setText("");

		responsavel.setText("");

		cep.setText("");

		endereco.setText("");

		numeroEndereco.setText("");

		numeroApartamento.setText("");

		bloco.setText("");

		complemento.setText("");

		bairro.setText("");

		cidade.setText("");

		telefone.setText("");

		celular.setText("");

		email.setText("");

		instituicaoEnsino.setText("");

		dadosCurso.setText("");

		inicioAnoLetivo.setText("");

		fimAnoLetivo.setText("");
	}

	public void enableEdit() {

		userId.setEditable(false);

		matricula.setEditable(true);

		nome.setEditable(true);

		// birthDate.setEditable(true);

		RG.setEditable(true);

		rgDigit.setEditable(true);

		estadoEmissor.setEditable(true);

		// emissionDate.setEditable(true);

		// ComboBox<String> sexo;

		cpf.setEditable(true);

		// ComboBox<String> cpfType;

		nacionalidade.setEditable(true);

		responsavel.setEditable(true);

		cep.setEditable(true);

		endereco.setEditable(true);

		numeroEndereco.setEditable(true);

		numeroApartamento.setEditable(true);

		bloco.setEditable(true);

		complemento.setEditable(true);

		bairro.setEditable(true);

		cidade.setEditable(true);

		telefone.setEditable(true);

		celular.setEditable(true);

		email.setEditable(true);

		// instituicaoEnsino.setEditable(true);

		dadosCurso.setEditable(true);

		inicioAnoLetivo.setEditable(true);

		fimAnoLetivo.setEditable(true);
	}

	public void onClickNovo() {
		center.prepareForNewUser();
		center.clearData();
		center.enableEdit();
		bProcurar.setDisable(true);
		bNovo.setDisable(true);
		bSalvar.setDisable(false);
		bCancelar.setDisable(false);
		bApagar.setDisable(true);

		bEditar.setDisable(true);
	}

	public void onClickProcurar() {
		original = null;
		rootController.onClickMostrarAlunos();
		rootController.reloadMainMenu();
	}

	public void setMatriculaController(
			AssociarMatriculaController associarMatriculaController) {
		this.amController = associarMatriculaController;

	}

	public void disableButtonsForDisplayStudent() {
		bProcurar.setDisable(false);
		bNovo.setDisable(false);
		bEditar.setDisable(false);
		bSalvar.setDisable(true);
		bCancelar.setDisable(true);
		bApagar.setDisable(true);
	}

	public void disableButtonsForNewStudent() {
		bProcurar.setDisable(true);
		bNovo.setDisable(true);
		bEditar.setDisable(true);
		bSalvar.setDisable(false);
		bCancelar.setDisable(false);
		bApagar.setDisable(true);
	}

}
