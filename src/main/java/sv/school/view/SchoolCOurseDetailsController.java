package sv.school.view;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import sv.school.controller.SchoolCourseController;
import sv.school.model.Course;
import sv.school.model.OfferedCourses;
import sv.school.model.SchoolCourse;
import sv.school.util.SysDomainConventer;

public class SchoolCOurseDetailsController {
	@FXML
	private TextField tfSchoolNumber;
	@FXML
	private TextField tfCourseCode;
	@FXML
	private TextField tfSchoolCourseCode;
	@FXML
	private TextField tfYear;
	@FXML
	private TextField tfSituation;
	@FXML
	private TextField tfXID;
	@FXML
	private TextField tfDescription;
	@FXML
	private TextField tfSchoolSerie;

	@FXML
	private DatePicker dpStartDate;

	@FXML
	private DatePicker dpFinishDate;
	@FXML
	private TextField tfVigencia;
	@FXML
	private TextField tfStudentList;
	@FXML
	private TextField tfSchema;
	@FXML
	private TextField tfCota;

	@FXML
	private RadioButton rbFirstSemester;
	@FXML
	private RadioButton rbSecondSemester;
	@FXML
	private RadioButton rbBothSemester;

	private RootLayoutController rootController;

	private SchoolCourseComp scOriginal;
	private SchoolCourse scCourse;

	private static String createdCourseDesc;
	private ToggleGroup group;

	public void displaySchoolCOurse(SchoolCourseComp scCompCourse) {
		setRadioGroup();
		scOriginal = scCompCourse;

		SchoolCourse scCourse = scCompCourse.getScOriginal();

		tfSchoolNumber.setText("" + scCourse.getSchoolCoursePK().getSchoolId());
		tfCourseCode.setText("" + scCourse.getSchoolCoursePK().getCourseCode());
		tfSchoolCourseCode.setText(""
				+ scCourse.getSchoolCoursePK().getSchoolCourseId());
		tfYear.setText(scCourse.getYear());
		if (scCourse.getSchoolCourseStatusId() == 1) {
			tfSituation.setText("Ativo");
		} else {
			tfSituation.setText("Inativo");
		}
		tfXID.setText("" + scCourse.getxId());
		tfDescription.setText(scCourse.getDescription());
		tfSchoolSerie.setText("" + scCourse.getPeriodId());
		tfSchema.setText("" + scCourse.getSchemaId());
		tfVigencia.setText(SysDomainConventer.periodType(scCourse
				.getVigenciaTypeId()));
		tfCota.setText(SysDomainConventer.cota(scCourse.getSchemaId()));

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

		LocalDate initDat = getLocalDate(scCourse.getValidInitialDate());
		LocalDate FinishDat = getLocalDate(scCourse.getValidFinalDate());
		dpStartDate.setValue(initDat);
		dpFinishDate.setValue(FinishDat);

		if (scCourse.getPeriodId() == 1) {
			rbFirstSemester.setSelected(true);
		} else if (scCourse.getPeriodId() == 2) {
			rbSecondSemester.setSelected(true);
		} else if (scCourse.getPeriodId() == 0) {
			rbBothSemester.setSelected(true);
		}

	}

	public String getCreatedCourseDesc() {
		return createdCourseDesc;
	}

	public void setRadioGroup() {
		group = new ToggleGroup();
		rbFirstSemester.setToggleGroup(group);
		rbSecondSemester.setToggleGroup(group);
		rbBothSemester.setToggleGroup(group);

		rbFirstSemester.setDisable(true);
		rbSecondSemester.setDisable(true);
		rbBothSemester.setDisable(true);
	}

	public void setRootController(RootLayoutController rootController) {
		this.rootController = rootController;
		rootController.setSchoolCourseDetailsController(this);

	}

	private LocalDate getLocalDate(Date d) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		LocalDate date = LocalDate.of(cal.get(Calendar.YEAR),
				cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
		return date;
	}

	public void enableEdit() {
		tfDescription.setEditable(true);
		rbFirstSemester.setDisable(false);
		rbSecondSemester.setDisable(false);
		rbBothSemester.setDisable(false);
		dpFinishDate.setEditable(true);
		dpStartDate.setEditable(true);
		tfSchoolSerie.setEditable(true);

	}

	private Date getDateFromLocalDate(LocalDate d) {

		Instant instant = Instant.from(d.atStartOfDay(ZoneId.systemDefault()));
		Date date = Date.from(instant);
		return date;
	}

	/**
	 * this method gets the schoolCourse from the screen and saves it
	 * 
	 * @param {boolean} isNew tells if the course should be persisted (true) or
	 *        updated(false)
	 * 
	 */
	public void saveSchoolCourse(boolean isNew) {

		SchoolCourseController controller = new SchoolCourseController();
		if (isNew) {

			int periodTypeId;
			if (tfVigencia.getText().trim().equals("SEMESTRAL")) {
				periodTypeId = 1;
			} else if (tfVigencia.getText().trim().equals("ANUAL")) {
				periodTypeId = 2;
			} else {
				periodTypeId = 3; // anual com aula em julho
			}

			int periodId;
			if (group.getSelectedToggle() == rbBothSemester) {
				periodId = 0;
			} else if (group.getSelectedToggle() == rbFirstSemester) {
				periodId = 1;
			} else {
				periodId = 2;

			}

			DateFormat dFormat = new SimpleDateFormat("dd/mm/yyyy");
			Date initDate;
			Date finishDate;
			initDate = getDateFromLocalDate(dpStartDate.getValue());
			finishDate = getDateFromLocalDate(dpFinishDate.getValue());

			SchoolCourse newSchoolCourse = new SchoolCourse(
					Integer.parseInt(tfSchoolNumber.getText()),
					tfCourseCode.getText(), Integer.parseInt(tfSchoolCourseCode
							.getText()), tfDescription.getText(), "",
					Integer.parseInt(tfSchema.getText()), periodTypeId,
					tfYear.getText(), initDate, finishDate, 0, 1, periodTypeId,
					periodId, Integer.parseInt(tfXID.getText()));
			// if the course should be PERSISTED
			createdCourseDesc = newSchoolCourse.getDescription();
			controller.persist(newSchoolCourse);
			controller.commit();

		} else {

			scCourse = scOriginal.getScOriginal();
			scCourse.setDescription(tfDescription.getText());
			scCourse.setPeriodId(Integer.parseInt(tfSchoolSerie.getText()));

			scCourse.setValidInitialDate(getDateFromLocalDate(dpStartDate
					.getValue()));
			scCourse.setValidInitialDate(getDateFromLocalDate(dpFinishDate
					.getValue()));
			if (group.getSelectedToggle() == rbFirstSemester) {
				System.out.println("firstsemester here");
				scCourse.setPeriodId(1);

			} else if (group.getSelectedToggle() == rbSecondSemester) {
				System.out.println("second semester here");
				scCourse.setPeriodId(2);
			} else if (group.getSelectedToggle() == rbBothSemester) {
				System.out.println("both semestere here");
				scCourse.setPeriodId(0);
			}
			createdCourseDesc = scCourse.getDescription();
			controller.update(scCourse);
			controller.commit();
		}
	}

	/*
	 */
	public void cancel() {
		displaySchoolCOurse(scOriginal);
	}

	public void delete() {
		SchoolCourse toBeDeleted = scOriginal.getScOriginal();
		SchoolCourseController controller = new SchoolCourseController();
		controller.delete(toBeDeleted);
		controller.commit();
	}

	public void onClickVoltar() {
		rootController.showSchoolCourses();
	}

	public void prepareForNewCourse(ComposedCourse cCompCourse) {
		Course cCourse = cCompCourse.getOriginalCourse();
		OfferedCourses offCourse = cCompCourse.getOfferedCourse();
		String vigencia;
		setRadioGroup();
		if (cCourse == null) {
			System.out.println("oCourse eh null");
		}
		if (offCourse == null) {
			System.out.println("offCourse eh null");
		}

		tfSchoolNumber.setText(offCourse.getSchoolId() + "");
		tfCourseCode.setText(cCourse.getCourseCode());

		SchoolCourseController scController = new SchoolCourseController();
		List<SchoolCourse> allSCourses = scController.getAll();
		List<SchoolCourse> associatedSchoolCourses = scController.getAll();

		for (SchoolCourse scCourse : allSCourses) {
			if (scCourse.getCourseCode() == cCompCourse.getCourseCode()) {
				associatedSchoolCourses.add(scCourse);
			}
		}
		String newSchoolCourseCode;

		if (associatedSchoolCourses != null
				&& associatedSchoolCourses.size() != 0) {

			sortBySchoolCourseCoude(associatedSchoolCourses);

			SchoolCourse lastOne = associatedSchoolCourses
					.get(associatedSchoolCourses.size() - 1);
			newSchoolCourseCode = String.valueOf(lastOne.getSchoolCoursePK()
					.getSchoolCourseId() + 1);
			tfSchoolCourseCode.setText(newSchoolCourseCode);
		} else {
			newSchoolCourseCode = "1";
			tfSchoolCourseCode.setText(1 + "");
		}
		int Sysyear = Calendar.getInstance().get(Calendar.YEAR);
		tfYear.setText(Sysyear + "");
		tfSituation.setText("ATIVO");
		tfXID.setText(cCourse.getxId() + "");

		if (offCourse.getPeriodTypeId() == OfferedCourses.PERIOD_TYPE_ID_SEMESTRAL) {

			// when period_type_id is 'semestral' only allow user tp choose
			// between
			// one the semester, not both
			rbSecondSemester.setDisable(false);
			rbFirstSemester.setDisable(false);
			rbBothSemester.setDisable(true);
			vigencia = "SEMESTRAL";
		} else if (offCourse.getPeriodTypeId() == OfferedCourses.PERIOD_TYPE_ID_ANUAL) {
			// when period type id is not semestral (yearly or yearly with
			// classes in june)
			// the user is not allowed to choose
			rbSecondSemester.setDisable(true);
			rbFirstSemester.setDisable(true);
			rbBothSemester.setSelected(true);
			rbBothSemester.setDisable(true);
			vigencia = "ANUAL";

		} else {
			// same as before, but with classes in june
			// (offeredCourses.PERIOD_TYPE_ID_ANUAL)
			rbSecondSemester.setDisable(true);
			rbFirstSemester.setDisable(true);
			rbBothSemester.setSelected(true);
			rbBothSemester.setDisable(true);
			vigencia = "ANUAL COM AULA EM JUNHO";
		}
		// building the default String that describes the schoolCourse
		// the user can change it to whatever it likes, thou.
		String desc = cCourse.getDescription() + ", TURMA "
				+ newSchoolCourseCode + ", " + offCourse.getSysYear();

		// tfStartDate.textProperty().addListener(new ChangeListener<String>() {
		// @Override
		// public void changed(ObservableValue<? extends String> observable,
		// String oldValue, String newValue) {
		// if (newValue
		// .matches("(0?\\d|1\\d|2\\d|3[01])/(0?\\d|1[012])/((19|20)\\d\\d)")) {
		// tfStartDate.setText(newValue);
		// } else {
		// tfStartDate.setText(oldValue);
		// }
		// }
		// });
		// tfFInishDate.textProperty().addListener(new ChangeListener<String>()
		// {
		// @Override
		// public void changed(ObservableValue<? extends String> observable,
		// String oldValue, String newValue) {
		// if (newValue
		// .matches("(0?\\d|1\\d|2\\d|3[01])/(0?\\d|1[012])/((19|20)\\d\\d)")) {
		// tfFInishDate.setText(newValue);
		// } else {
		// tfFInishDate.setText(oldValue);
		// }
		// }
		// });
		tfDescription.setText(desc);
		tfSchoolSerie.setText("");

		// tfStartDate.setText("");
		// tfFInishDate.setText("");
		tfVigencia.setText(vigencia);
		tfStudentList.setText("0");
		tfSchema.setText(offCourse.getSchemaId() + "");
		tfCota.setText(SysDomainConventer.cota(offCourse.getSchemaId()));
		tfDescription.setEditable(true);
		tfSchoolSerie.setEditable(true);
		dpFinishDate.setEditable(true);
		dpStartDate.setEditable(true);
		dpStartDate.setValue(LocalDate.now());
		if (offCourse.getPeriodTypeId() == OfferedCourses.PERIOD_TYPE_ID_SEMESTRAL) {
			// curso semestral
			dpFinishDate.setValue(LocalDate.now().plusMonths(6));
		} else {
			dpFinishDate.setValue(LocalDate.now().plusMonths(12));
		}

	}

	private void sortBySchoolCourseCoude(
			List<SchoolCourse> associatedSchoolCourses) {

		Collections.sort(associatedSchoolCourses,
				new Comparator<SchoolCourse>() {

					@Override
					public int compare(SchoolCourse o1, SchoolCourse o2) {

						return Integer.valueOf(
								o1.getSchoolCoursePK().getSchoolCourseId())
								.compareTo(
										Integer.valueOf(o2.getSchoolCoursePK()
												.getSchoolCourseId()));
					}
				});
	}

}
