package sv.school.view;

import sv.school.model.Registration;
import sv.school.model.User;

public class UserRegistrationComp {

	private String register;
	private String name;
	private String RG;
	private String uf;
	private int gratuityType;
	private int gratuityActive;
	private User originalUser;

	public UserRegistrationComp(User uUser, Registration rRegistration) {
		register = uUser.getRegisterName();
		name = uUser.getName();
		RG = uUser.getRgNumber() + "-" + uUser.getRgDigit();
		uf = uUser.getRgEmittedState();
		gratuityType = rRegistration.getGratuityType();
		gratuityActive = rRegistration.getGratuityActive();
		originalUser = uUser;
	}

	public User getOriginalUser() {
		return this.originalUser;
	}

	public String getRegister() {
		return register;
	}

	public String getName() {
		return name;
	}

	public String getRG() {
		return RG;
	}

	public String getUf() {
		return uf;
	}

	public int getGratuityType() {
		return gratuityType;
	}

	public int getGratuityActive() {
		return gratuityActive;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRG(String rG) {
		RG = rG;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public void setGratuityType(int gratuityType) {
		this.gratuityType = gratuityType;
	}

	public void setGratuityActive(int gratuityActive) {
		this.gratuityActive = gratuityActive;
	}

}
