package sv.school.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sv.school.App;
import sv.school.controller.RegistrationController;
import sv.school.controller.SchoolController;
import sv.school.controller.UserController;
import sv.school.model.Registration;
import sv.school.model.User;

public class AssociarMatriculaController {

	private static SchoolCourseComp matriculaCourse;

	@FXML
	private TextField tfSchoolNumber;

	@FXML
	private TextField tfCurse;

	@FXML
	private TextField tfSchoolCourseCode;

	@FXML
	private TextField tfYear;

	@FXML
	private TextField tfSemester;

	@FXML
	private TextField tfDescription;

	@FXML
	private TextField tfRGNumber;
	@FXML
	private TextField tfRGDigit;
	@FXML
	private TextField tfRGName;
	@FXML
	private CheckBox cbAtiva;

	private static final int REGISTRATION_STATUS_ACTIVE = 2;

	@FXML
	private ComboBox<String> cbGratuidade;

	private static final int SEARCH_BY_RG = 0;
	private static final int SEARCH_BY_NAME = 1;
	private static final int SEARCH_ALL = -1;
	@FXML
	private Button bProcurar;
	@FXML
	private Button bAssociar;
	@FXML
	private Button bNovo;
	@FXML
	private Button bLigarFiltro;
	@FXML
	private Button bDesligarFiltro;
	@FXML
	private TableView<UserRegistrationComp> tvAlunosMatriculados;
	@FXML
	private TableView<User> tvAlunosProcurados;
	@FXML
	private TableColumn<User, String> procuradoMatricula;
	@FXML
	private TableColumn<User, String> procuradoNome;
	@FXML
	private TableColumn<User, String> procuradoRg;
	@FXML
	private TableColumn<User, String> procuradoUF;
	@FXML
	private TableColumn<UserRegistrationComp, String> matriculadoMatricula;
	@FXML
	private TableColumn<UserRegistrationComp, String> matriculaNome;
	@FXML
	private TableColumn<UserRegistrationComp, String> matriculaRg;
	@FXML
	private TableColumn<UserRegistrationComp, String> matriculaUF;
	@FXML
	private TableColumn<UserRegistrationComp, String> matriculaGratuidade;
	@FXML
	private TableColumn<UserRegistrationComp, String> matriculaGratuidadeAtiva;
	@FXML
	private Button bDesassociar;
	@FXML
	private Button bLegenda;

	private static List<Registration> allRegistration;

	@FXML
	private Button bVoltar;

	// private static User userToRegister;

	List<User> allUser;

	public static User selectedUser;

	public void setCourse(SchoolCourseComp selected) {
		matriculaCourse = selected;
	}

	private static RootLayoutController rlController;

	public void setRootLayoutController(RootLayoutController rlControoler) {
		this.rlController = rlControoler;
	}

	public void prepareRegistration() {
		// prepare interface
		SchoolController sController = new SchoolController();
		tfSchoolNumber.setText("" + sController.getAll().get(0).getSchoolId());
		tfCurse.setText(matriculaCourse.getCourseDescrption());
		tfSchoolCourseCode.setText(matriculaCourse.getSchoolCourseCode());
		tfYear.setText(matriculaCourse.getScOriginal().getYear());
		tfSemester.setText(matriculaCourse.scVigencia);
		tfDescription.setText(matriculaCourse.getScDescription());
		bAssociar.setDisable(true);
		bDesligarFiltro.setDisable(true);
		tfRGNumber.setEditable(true);
		tfRGDigit.setEditable(true);
		tfRGName.setEditable(true);
		bDesassociar.setDisable(true);
		// set gratuity types
		cbGratuidade.getItems().addAll("0 - Sem Gratuidade",
				" 1 - Municipal - Supletivo", "2 - Municipal - Fundamental",
				" 3 - Municipal - Ensino M�dio", "4 - Municipal - T�cnico",
				"5 - Municipal - Profissionalizante",
				"6 - Estadual - Supletivo", "7 - Estadual - Fundamental",
				"8 - Estadual - Ensino M�dio", "9 - Estadual - T�cnico",
				"10 - Estadual - Profissionalizante",
				"11 - Federal - Supletivo", "12 - Federal - Fundamental",
				"13 - Federal - Ensino M�dio", "14 - Federal - T�cnico",
				"15 - Federal - Profissionalizante",
				"16 - Superior Particular - FIES",
				"18 - Superior Particular - PROUni",
				"19 - Superior Particular - Escola Fam�lia",
				"20 - Superior Particular - Cota Social",
				"21 - Superior Estadual - Baixa Renda",
				"22 - Superior Federal - Baixa Renda"

		);
		cbGratuidade.getSelectionModel().select(0);
		// load users
		UserController uController = new UserController();
		allUser = uController.getAll();
		bLigarFiltro.setVisible(false);
		bDesligarFiltro.setVisible(false);
	}

	public void balanceTables() {
		procuradoMatricula.prefWidthProperty().bind(
				tvAlunosProcurados.widthProperty().divide(12).multiply(2));
		procuradoNome.prefWidthProperty().bind(
				tvAlunosProcurados.widthProperty().divide(12).multiply(6));
		procuradoRg.prefWidthProperty().bind(
				tvAlunosProcurados.widthProperty().divide(12).multiply(3));
		procuradoUF.prefWidthProperty().bind(
				tvAlunosProcurados.widthProperty().divide(12).multiply(1));

		matriculadoMatricula.prefWidthProperty().bind(
				tvAlunosMatriculados.widthProperty().divide(16).multiply(2));
		matriculaNome.prefWidthProperty().bind(
				tvAlunosMatriculados.widthProperty().divide(16).multiply(6));
		matriculaRg.prefWidthProperty().bind(
				tvAlunosMatriculados.widthProperty().divide(16).multiply(3));
		matriculaUF.prefWidthProperty().bind(
				tvAlunosMatriculados.widthProperty().divide(16));
		matriculaGratuidade.prefWidthProperty().bind(
				tvAlunosMatriculados.widthProperty().divide(16).multiply(2));
		matriculaGratuidadeAtiva.prefWidthProperty().bind(
				tvAlunosMatriculados.widthProperty().divide(16).multiply(2));

	}

	public void onClickProcurar() {

		String searchValue = "";
		int searchBy = -1;
		if (!tfRGName.getText().isEmpty()) {
			searchValue = tfRGName.getText();
			searchBy = SEARCH_BY_NAME;
		}
		if (!tfRGNumber.getText().isEmpty()) {
			searchValue = tfRGNumber.getText();
			searchBy = SEARCH_BY_RG;
		}

		List<User> matchedUsers = new ArrayList<User>();

		if (searchValue.isEmpty() || searchValue.equals("")) {
			procuradoMatricula
					.setCellValueFactory(new PropertyValueFactory<User, String>(
							"registerName"));
			procuradoNome
					.setCellValueFactory(new PropertyValueFactory<User, String>(
							"name"));
			procuradoRg
					.setCellValueFactory(new PropertyValueFactory<User, String>(
							"rgNumber"));
			procuradoUF
					.setCellValueFactory(new PropertyValueFactory<User, String>(
							"rgEmittedState"));

			ObservableList<User> olUser = FXCollections
					.observableArrayList(allUser);

			tvAlunosProcurados.setItems(olUser);

		} else if (searchBy == SEARCH_BY_NAME) {
			for (User user : allUser) {
				if (user.getName().toUpperCase()
						.contains(searchValue.toUpperCase())) {
					matchedUsers.add(user);
				}
				procuradoMatricula
						.setCellValueFactory(new PropertyValueFactory<User, String>(
								"registerName"));
				procuradoNome
						.setCellValueFactory(new PropertyValueFactory<User, String>(
								"name"));
				procuradoRg
						.setCellValueFactory(new PropertyValueFactory<User, String>(
								"rgNumber"));
				procuradoUF
						.setCellValueFactory(new PropertyValueFactory<User, String>(
								"rgEmittedState"));

				ObservableList<User> olUser = FXCollections
						.observableArrayList(matchedUsers);

				tvAlunosProcurados.setItems(olUser);
			}
		} else if (searchBy == SEARCH_BY_RG) {
			for (User user : allUser) {
				if (user.getRgNumber().contains(searchValue)) {
					matchedUsers.add(user);
				}
			}
			procuradoMatricula
					.setCellValueFactory(new PropertyValueFactory<User, String>(
							"registerName"));
			procuradoNome
					.setCellValueFactory(new PropertyValueFactory<User, String>(
							"name"));
			procuradoRg
					.setCellValueFactory(new PropertyValueFactory<User, String>(
							"rgNumber"));
			procuradoUF
					.setCellValueFactory(new PropertyValueFactory<User, String>(
							"rgEmittedState"));

			ObservableList<User> olUser = FXCollections
					.observableArrayList(matchedUsers);
			tvAlunosProcurados.setItems(olUser);
		}

		tvAlunosProcurados
				.getSelectionModel()
				.selectedItemProperty()
				.addListener(
						(observable, oldValue, newValue) -> enableAssociar(newValue));
	}

	private void enableAssociar(User selectedUser) {

		bAssociar.setDisable(false);
		this.selectedUser = selectedUser;
	}

	private static Stage sConfirmationStage;

	public void onClickAssociar() {

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(App.class
				.getResource("view/ConfirmRegistrationView.fxml"));
		AnchorPane apConfirmReg;
		try {
			apConfirmReg = (AnchorPane) loader.load();
			UserEditViewController uController = loader.getController();
			uController.displayUser(selectedUser);
			uController.setMatriculaController(this);
			uController.disableConfirmationButtons();
			Stage secondStage = new Stage();
			sConfirmationStage = secondStage;
			secondStage.initModality(Modality.APPLICATION_MODAL);
			secondStage.initStyle(StageStyle.UNIFIED);
			secondStage.setTitle("Configura��o do sistema");
			secondStage.setScene(new Scene(apConfirmReg));
			secondStage.setResizable(false);
			secondStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Registration rNewRegistration = new Registration(registrationId,
		// selectedUser.getRegisterName(), selectedUser.getUserId(),
		// matriculaCourse.getScOriginal().getSchoolCoursePK().getSchoolId(),
		// matriculaCourse.getCourseOriginal().getCourseCode(),
		// matriculaCourse.getScCode(), REGISTRATION_STATUS_ACTIVE,
		// 0, matriculaCourse.getScOriginal().getSchemaId(), registrationDate,
		// initialValidDate, finalValidDate)
		//
	}

	public void onCLickNovo() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Criar novo usu�rio");
		alert.setHeaderText("Gostaria de criar um novo usu�rio?");
		alert.setContentText("Voc� ser� direcionado a tela de cria��o de usu�rios!");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			rlController.loadUserEdit(true, null);

		} else {

		}

	}

	public void onClickLigarFiltro() {
		System.out.println("onCLickligarFiltro");
	}

	public void onCLickDesligarFiltro() {
		System.out.println("onCLickDesligarFiltro");
	}

	public void onCLickDessassociar() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Dessassociar usu�rio");
		alert.setHeaderText("Gostaria de desvincular este usu�rio a este curso?");
		alert.setContentText("Essa opera��o n�o poder� ser desfeita, por�m voc� poder� rematricula-lo mais tarde!");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {

			RegistrationController rController = new RegistrationController();
			Registration rCancel = rController.getRegistrationByUserId(
					urUser.getOriginalUser().getUserId()).get(0);
			if (rCancel != null) {
				rController.delete(rCancel);
				Alert desassociarAlert = new Alert(AlertType.INFORMATION);
				desassociarAlert.setTitle("Aluno desmatr\u00edculado");
				desassociarAlert
						.setHeaderText("O aluno foi desmatriculado com sucesso!");
				desassociarAlert
						.setContentText("Obs: este opera\u00e7\u00e3o n\u00e3o pode ser desfeita");

				desassociarAlert.showAndWait();

			}
		} else {

		}
		refresh();
	}

	public void onCLickLegenda() {
		System.out.println("onCLickLegenda");
	}

	public void onClickVoltar() {

		rlController.onClickMatricular();
	}

	public void fillRegisteredTable() {

		RegistrationController rController = new RegistrationController();
		List<Registration> allRegistrations = rController.getAll();
		this.allRegistration = allRegistrations;
		Collections.sort(this.allRegistration,
				(Registration r1, Registration r2) -> r1.getRegistrationId()
						- r2.getRegistrationId());
		List<Registration> scRegistrations = new ArrayList<Registration>();
		for (Registration registration : allRegistrations) {
			if (registration.getSchoolCourseId() == matriculaCourse
					.getScOriginal().getSchoolCoursePK().getSchoolCourseId()
					&& registration.getCourseCode().equals(
							matriculaCourse.getScOriginal().getSchoolCoursePK()
									.getCourseCode())) {
				System.out.println("adicionado, entrou aqui");
				scRegistrations.add(registration);
			}
		}
		UserController uController = new UserController();
		List<User> registrationUsers = new ArrayList<User>();
		List<UserRegistrationComp> userRegistrations = new ArrayList<UserRegistrationComp>();
		for (Registration registration : scRegistrations) {
			User uUser = uController.getById(registration.getUserId());
			registrationUsers.add(uUser);
			userRegistrations
					.add(new UserRegistrationComp(uUser, registration));
			System.out.println("userregistered = " + userRegistrations.size());
		}

		ObservableList<UserRegistrationComp> olRegistrations = FXCollections
				.observableArrayList(userRegistrations);
		System.out.println(olRegistrations.size());

		matriculadoMatricula
				.setCellValueFactory(new PropertyValueFactory<UserRegistrationComp, String>(
						"register"));
		matriculaNome
				.setCellValueFactory(new PropertyValueFactory<UserRegistrationComp, String>(
						"name"));
		matriculaRg
				.setCellValueFactory(new PropertyValueFactory<UserRegistrationComp, String>(
						"RG"));
		matriculaUF
				.setCellValueFactory(new PropertyValueFactory<UserRegistrationComp, String>(
						"uf"));
		matriculaGratuidade
				.setCellValueFactory(new PropertyValueFactory<UserRegistrationComp, String>(
						"gratuityType"));
		matriculaGratuidadeAtiva
				.setCellValueFactory(new PropertyValueFactory<UserRegistrationComp, String>(
						"gratuityActive"));

		tvAlunosMatriculados.setItems(olRegistrations);

		tvAlunosMatriculados
				.getSelectionModel()
				.selectedItemProperty()
				.addListener(
						(observable, oldValue, newValue) -> enableDesassociar(newValue));
		//
		// ObservableList<SchoolCourseComp> olCourse = FXCollections
		// .observableArrayList(coursesToShow);
		//
		// ID.setCellValueFactory(new PropertyValueFactory<SchoolCourseComp,
		// String>(
		// "schoolCourseCode"));
		// courseDesc
		// .setCellValueFactory(new PropertyValueFactory<SchoolCourseComp,
		// String>(
		// "courseDescrption"));
		// turmaDesc
		// .setCellValueFactory(new PropertyValueFactory<SchoolCourseComp,
		// String>(
		// "scDescription"));
		// semestre.setCellValueFactory(new
		// PropertyValueFactory<SchoolCourseComp, String>(
		// "semestre"));
		// serie.setCellValueFactory(new PropertyValueFactory<SchoolCourseComp,
		// String>(
		// "scSerie"));
		// tvCourses.setItems(olCourse);
		//
		// tvCourses
		// .getSelectionModel()
		// .selectedItemProperty()
		// .addListener(
		// (observable, oldValue, newValue) -> enableSelecionar(newValue));
	}

	private static UserRegistrationComp urUser;

	private void enableDesassociar(UserRegistrationComp newValue) {
		bDesassociar.setDisable(false);
		urUser = newValue;
	}

	/*
	 * 
	 * this method receives the edited user for the registration
	 */
	public void setUserRegistration(User editedUser) {
		// TODO Auto-generated method stub

	}

	public void registerUser(User editedUser) {
		RegistrationController rController = new RegistrationController();
		List<Registration> userRegistrations = rController
				.getRegistrationByUserId(editedUser.getUserId());

		if (userRegistrations != null && userRegistrations.size() != 0) {

			Registration rRegistration = userRegistrations.get(0);
			if (rRegistration.getCourseCode().equals(
					matriculaCourse.getCourseOriginal().getCourseCode())
					&& rRegistration.getSchoolCourseId() == matriculaCourse
							.getScOriginal().getSchoolCoursePK()
							.getSchoolCourseId()) {
				sConfirmationStage.close();
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Resultado da matr\u00edcula");
				alert.setHeaderText("Matr\u00edcula n\u00e3o realizada");
				alert.setContentText("O aluno j\u00e1 esta matriculado nesta turma!");

				alert.showAndWait();

			} else {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Confirmar matr\u00edcula");
				alert.setHeaderText("Cancelar antiga matr�cula?");
				alert.setContentText("O aluno j� possui uma matr�cula ativa, tem certeza que deseja matr�cula-lo neste turma? "
						+ "Obs: a antiga matr�cula ser� desfeita!!!");

				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() == ButtonType.OK) {
					rController.delete(rRegistration);

					int registrationId = allRegistration.get(
							allRegistration.size() - 1).getRegistrationId() + 1;
					Registration newRegistration = new Registration(
							registrationId, editedUser.getRegisterName(),
							editedUser.getUserId(), matriculaCourse
									.getScOriginal().getSchoolCoursePK()
									.getSchoolId(), matriculaCourse
									.getCourseOriginal().getCourseCode(),
							Integer.parseInt(matriculaCourse.getScCode()),
							REGISTRATION_STATUS_ACTIVE, 0, matriculaCourse
									.getScOriginal().getSchemaId(), new Date(),
							matriculaCourse.getScOriginal()
									.getValidInitialDate(), matriculaCourse
									.getScOriginal().getValidFinalDate());
					sConfirmationStage.close();
					rController.persist(newRegistration);
					Alert sucessAlert = new Alert(AlertType.INFORMATION);
					sucessAlert.setTitle("Resultado da matr�cula");
					sucessAlert.setHeaderText("Matr�cula realizada");
					sucessAlert
							.setContentText("O aluno foi desmatriculado do curso antigo, e est� matr�culado neste curso!");

					sucessAlert.showAndWait();

				} else {
					sConfirmationStage.close();
					Alert sucessAlert = new Alert(AlertType.INFORMATION);
					sucessAlert.setTitle("Resultado da matr�cula");
					sucessAlert.setHeaderText("Matr�cula n�o realizada");
					sucessAlert
							.setContentText("Esta matr�cula n�o foi efetuada!");
				}
			}
		} else {
			int registrationId = allRegistration
					.get(allRegistration.size() - 1).getRegistrationId() + 1;
			Registration newRegistration = new Registration(registrationId,
					editedUser.getRegisterName(), editedUser.getUserId(),
					matriculaCourse.getScOriginal().getSchoolCoursePK()
							.getSchoolId(), matriculaCourse.getCourseOriginal()
							.getCourseCode(), Integer.parseInt(matriculaCourse
							.getScCode()), REGISTRATION_STATUS_ACTIVE, 0,
					matriculaCourse.getScOriginal().getSchemaId(), new Date(),
					matriculaCourse.getScOriginal().getValidInitialDate(),
					matriculaCourse.getScOriginal().getValidFinalDate());
			rController.persist(newRegistration);
			sConfirmationStage.close();
			Alert sucessAlert = new Alert(AlertType.INFORMATION);
			sucessAlert.setTitle("Resultado da matr�cula");
			sucessAlert.setHeaderText("Matr�cula realizada");
			sucessAlert
					.setContentText("O aluno est� matr�culado com sucesso na turma!");

		}
		rController.commit();
		this.refresh();
	}

	/*
	 * 
	 * refresh the screen so the user that was just registered appear in the
	 * registered users table
	 */
	private void refresh() {
		MatricularViewController mvc = new MatricularViewController();
		mvc.onClickSelecionar();

	}
}
