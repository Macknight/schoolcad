package sv.school.view;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import sv.school.controller.UserController;
import sv.school.model.User;

public class AlunosViewController {

	@FXML
	private Button bProcurar;

	@FXML
	private Button bSelecionar;

	@FXML
	private Button bNovo;

	@FXML
	private TextField tfRG;

	@FXML
	private TextField tfRgDigit;

	@FXML
	private TextField tfNome;

	@FXML
	private TableView<User> usersTable;

	@FXML
	private TableColumn<User, String> tcMatricula;

	@FXML
	private TableColumn<User, String> tcName;

	private String searchValue;

	private static List<User> userList;
	private List<User> matchedUserList;

	private int searchBy;

	private static final int SEARCH_BY_NAME = 0;
	private static final int SEARCH_BY_RG = 1;
	private User selected;
	private static RootLayoutController rootController;

	@FXML
	private void onClickProcurar() {
		searchValue = "";
		if (!tfRG.getText().isEmpty()) {
			searchValue = tfRG.getText();
			searchBy = SEARCH_BY_RG;
		}
		if (!tfNome.getText().isEmpty()) {
			searchValue = tfNome.getText();
			searchBy = SEARCH_BY_NAME;
		}

		UserController userController = new UserController();
		// se a lista de alunos ainda n�o foi carregada, carrega
		if (userList == null || userList.size() <= 1) {

			/**
			 * pega 100 alunos, utilizando pagina��o para salvar memoria
			 *
			 */
			userList = userController.getLimitUsers(0, 100);
		}
		matchedUserList = new ArrayList<User>();
		if (searchValue.isEmpty() || searchValue == "") { //procura todo mundo, pois nao ha metodo de pesquisa
			//neste caso apenas mostra a lista de x alunos aleatorios vindos do banco
			ObservableList<User> olUser = FXCollections
					.observableArrayList(userList);
			tcMatricula
					.setCellValueFactory(new PropertyValueFactory<User, String>(
							"registerName"));
			tcName.setCellValueFactory(new PropertyValueFactory<User, String>(
					"name"));

			usersTable.setItems(olUser);
		} else if (searchBy == SEARCH_BY_NAME) { // NOME
			// add matched user to the list to be displayed
			// on the tableView

			matchedUserList = userController.getUsersByName(tfNome.getText());

			ObservableList<User> olUser = FXCollections
					.observableArrayList(matchedUserList);
			tcMatricula
					.setCellValueFactory(new PropertyValueFactory<User, String>(
							"registerName"));
			tcName.setCellValueFactory(new PropertyValueFactory<User, String>(
					"name"));

			usersTable.setItems(olUser);
		} else if (searchBy == SEARCH_BY_RG) { // RG
			// add matched user to the list to be displayed
			// on the tableView
		matchedUserList = userController.getUsersByRG(tfRG.getText());
			ObservableList<User> olUser = FXCollections
					.observableArrayList(matchedUserList);
			tcMatricula
					.setCellValueFactory(new PropertyValueFactory<User, String>(
							"registerName"));
			tcName.setCellValueFactory(new PropertyValueFactory<User, String>(
					"name"));

			usersTable.setItems(olUser);

		}

		usersTable
				.getSelectionModel()
				.selectedItemProperty()
				.addListener(
						(observable, oldValue, newValue) -> enableSelecionar(newValue));

		disableSelectButton();

	}

	public void enableSelecionar(User selectedUser) {

		if (selectedUser != null) {
			bSelecionar.setDisable(false);
			selected = selectedUser;
		}
	}

	public void setEnterKeySensibility() {
		tfNome.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					onClickProcurar();
				}
			}
		});
		tfRG.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					onClickProcurar();
				}
			}
		});
		tfRgDigit.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					onClickProcurar();
				}
			}
		});
	}

	public void disableSelectButton() {
		bSelecionar.setDisable(true);
	}

	public void balanceTableColumns() {
		tcMatricula.prefWidthProperty().bind(
				usersTable.widthProperty().divide(4));
		tcName.prefWidthProperty().bind(
				usersTable.widthProperty().divide(4).multiply(3));

	}

	public void onClickSelecionar() {
		rootController.loadUserEdit(false, selected);

	}

	public void onClickNovo() {

		rootController.loadUserEdit(true, null);
	}

	public void setRootController(RootLayoutController rootLayoutController) {
		rootController = rootLayoutController;

	}
}
