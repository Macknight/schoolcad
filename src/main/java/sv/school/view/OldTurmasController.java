package sv.school.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import sv.school.model.SchoolCourse;

public class OldTurmasController {
	@FXML
	TableView schoolCoursesTable;
	@FXML
	private TableColumn<SchoolCourse, String> courseCode;

	@FXML
	private TableColumn<SchoolCourse, String> description;
	@FXML
	private TableColumn<SchoolCourse, String> schoolLevel;
	@FXML
	private TableColumn<SchoolCourse, String> vigencia;
	@FXML
	private TableColumn<SchoolCourse, String> schema;
	@FXML
	private Button searchChourses;
	@FXML
	private Button showSchoolCourses;
}
