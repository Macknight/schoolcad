package sv.school.view;

import sv.school.model.Course;
import sv.school.model.OfferedCourses;

/**
 * Class that unites the couseClass with some other data from the database to
 * create a unique class to display in the turmas menu in the javafx perspective
 */

/*
 * SchoolCourseLevel 1 PrimarySchool Ensino Fundamental SchoolCourseLevel 2
 * HighSchool Ensino M�dio SchoolCourseLevel 3 University Superior
 * SchoolCourseLevel 4 SupplementarySchool Supletivo Ensino Fundamental ou M�dio
 * SchoolCourseLevel 5 TechnicalSchool Educa��o Profissionalizante ou N�vel
 * T�cnico SchoolCourseLevel 6 TechnicalHighSchool Ensino M�dio T�cnico
 */
public class ComposedCourse {

	private String courseCode;

	private String description;

	private int schoolLevelCodeId;

	private String schoolarLevel;

	private String vigencia;

	private String schema;

	private int xId;

	private Course originalCourse;
	private OfferedCourses originalOffCourse;

	public Course getOriginalCourse() {
		return this.originalCourse;
	}

	public OfferedCourses getOfferedCourse() {
		return this.originalOffCourse;
	}

	public ComposedCourse(String courseCode, String description,
			int schoolLevelCodeId, int periodTypeId, int schemaId,
			Course original, OfferedCourses offOriginal) {
		this.courseCode = courseCode;
		this.description = description;
		this.originalCourse = original;
		this.originalOffCourse = offOriginal;
		switch (schoolLevelCodeId) {
		case 1:
			this.schoolarLevel = "Ensino Fundamental";
			break;
		case 2:
			this.schoolarLevel = "Ensino Medio";
			break;
		case 3:
			this.schoolarLevel = "Superior";
			break;
		case 4:
			this.schoolarLevel = "Supletivo Ensino Fundamental ou Medio";
			break;
		case 5:
			this.schoolarLevel = "Educa��o Profissionalizante ou N�vel T�cnico";
			break;
		case 6:
			schoolarLevel = "Educa��o Profissionalizante ou N�vel T�cnico";
		}

		switch (periodTypeId) {
		case 1:
			this.vigencia = "Semestral";
			break;
		case 2:
			this.vigencia = "Anual";
			break;
		case 3:
			this.vigencia = "Anual com aula em julho";
			break;
		}

		switch (schemaId) {
		case 0:
			schema = "Cota 0 (2o semestre)";
			break;
		case 10:
			schema = "Cota 10 (anual)";
			break;
		case 11:
			schema = "Cota 10 (1o semestre)";
			break;
		case 12:
			schema = "Cota 10 (2o semestre)";
			break;
		case 20:
			schema = "Cota 20 (anual)	";
			break;
		case 21:
			schema = "Cota 20 (1o semestre)";
			break;
		case 22:
			schema = "Cota 20 (2o semestre)";
			break;
		case 23:
			schema = "Cota 10 (anual) (inclusive Julho)";
			break;
		case 30:
			schema = "Cota 30 (anual)";
			break;
		case 31:
			schema = "Cota 30 (1o semestre)";
			break;
		case 32:
			schema = "Cota 30 (2o semestre)";
			break;
		case 33:
			schema = "Cota 20 (anual) (inclusive Julho)";
			break;
		case 40:
			schema = "Cota 40 (anual)";
			break;
		case 41:
			schema = "Cota 40 (1o semestre)";
			break;
		case 42:
			schema = "Cota 40 (2o semestre)";
			break;
		case 43:
			schema = "Cota 30 (anual) (inclusive Julho)";
			break;
		case 50:
			schema = "Cota 50 (anual)";
			break;
		case 51:
			schema = "Cota 50 (1o semestre)";
			break;
		case 52:
			schema = "Cota 50 (2o semestre)";
			break;
		case 53:
			schema = "Cota 40 (anual) (inclusive Julho)";
			break;
		case 80:
			schema = "Cota 80 (anual)";
			break;
		case 81:
			schema = "Cota 80 (1o semestre)";
			break;
		case 82:
			schema = "Cota 80 (2o semestre)	";
			break;
		case 83:
			schema = "Cota 70 (anual) (inclusive Julho)";
			break;
		case 99:
			schema = "Cota 0 (2o semestre)";
			break;
		}

	}

	public String getSchoolarLevel() {
		return schoolarLevel;
	}

	public String getVigencia() {
		return vigencia;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchoolarLevel(String schoolarLevel) {
		this.schoolarLevel = schoolarLevel;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public String getDescription() {
		return description;
	}

	public int getSchoolLevelCodeId() {
		return schoolLevelCodeId;
	}

	public int getxId() {
		return xId;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setSchoolLevelCodeId(int schoolLevelCodeId) {
		this.schoolLevelCodeId = schoolLevelCodeId;
	}

	public void setxId(int xId) {
		this.xId = xId;
	}

}
