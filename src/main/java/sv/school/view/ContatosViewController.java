package sv.school.view;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import sv.school.model.SchoolContacts;

public class ContatosViewController {
	@FXML
	private TableView<SchoolContacts> tvContacts;
	@FXML
	private TableColumn<SchoolContacts, String> tcName;
	@FXML
	private TableColumn<SchoolContacts, String> tcRG;
	@FXML
	private TableColumn<SchoolContacts, String> tcRgDigit;
	@FXML
	private TableColumn<SchoolContacts, String> tcUF;
	@FXML
	private TableColumn<SchoolContacts, String> tcEmissionDate;

	@FXML
	private TextField tfName;
	@FXML
	private TextField tfRgNumber;
	@FXML
	private TextField tfRgDigit;
	@FXML
	private TextField tfUF;
	@FXML
	private TextField emissionDate;
	@FXML
	private TextField tfTelephone;
	@FXML
	private TextField tfTelephoneRamal;
	@FXML
	private TextField tfEmail;
	@FXML
	TextField tfEmissionDate;

	public void showContacts(List<SchoolContacts> contacts) {
		ObservableList<SchoolContacts> olContacts = FXCollections
				.observableArrayList(contacts);

		tcName.setCellValueFactory(new PropertyValueFactory<SchoolContacts, String>(
				"name"));
		tcRG.setCellValueFactory(new PropertyValueFactory<SchoolContacts, String>(
				"rgNumber"));
		tcRgDigit
				.setCellValueFactory(new PropertyValueFactory<SchoolContacts, String>(
						"rgDigit"));
		tcUF.setCellValueFactory(new PropertyValueFactory<SchoolContacts, String>(
				"rgEmittedState"));
		tcEmissionDate
				.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SchoolContacts, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(
							TableColumn.CellDataFeatures<SchoolContacts, String> schoolContacts) {
						SimpleStringProperty property = new SimpleStringProperty();
						DateFormat dateFormat = new SimpleDateFormat(
								"dd/MM/yyyy");
						property.setValue(dateFormat.format(schoolContacts
								.getValue().getRgEmittedDate()));
						return property;
					}
				});
		tvContacts.setItems(olContacts);
		tvContacts
				.getSelectionModel()
				.selectedItemProperty()
				.addListener(
						(observable, oldValue, newValue) -> showContactDetails(newValue));

	}

	public void showContactDetails(SchoolContacts selectedContact) {
		if (selectedContact != null) {
			tfName.setText(selectedContact.getName());
			tfRgNumber.setText(selectedContact.getRgNumber());
			tfUF.setText(selectedContact.getRgEmittedState());
			tfTelephone.setText(selectedContact.getTelephoneNumber());
			tfTelephoneRamal.setText(selectedContact.getTelephoneBranch());
			tfEmail.setText(selectedContact.getEmail());
			tfRgDigit
					.setText(selectedContact.getRgDigit() == null ? "Sem digito"
							: selectedContact.getRgDigit());
			tfEmissionDate
					.setText(selectedContact.getRgEmittedDate() == null ? " "
							.toString() : new SimpleDateFormat("yyyy-MM-dd")
							.format(selectedContact.getRgEmittedDate()));

		} else {
			tfName.setText(" ");
			tfRgNumber.setText(" ");
			tfUF.setText(" ");
			tfTelephone.setText(" ");
			tfTelephoneRamal.setText(" ");
			tfEmail.setText(" ");
			tfRgDigit.setText(" ");
			tfEmissionDate.setText(" ");
		}
	}

	public void balanceTable() {
		tcName.prefWidthProperty().bind(
				tvContacts.widthProperty().divide(6).multiply(2));
		tcRG.prefWidthProperty().bind(tvContacts.widthProperty().divide(6));
		tcRgDigit.prefWidthProperty()
				.bind(tvContacts.widthProperty().divide(6));
		tcUF.prefWidthProperty().bind(tvContacts.widthProperty().divide(6));
		tcEmissionDate.prefWidthProperty().bind(
				tvContacts.widthProperty().divide(6));

	}

}
