package sv.school.view;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import sv.school.controller.CotaController;
import sv.school.controller.CountryController;
import sv.school.controller.CourseController;
import sv.school.controller.RegistrationController;
import sv.school.controller.SchoolController;
import sv.school.controller.SchoolCourseController;
import sv.school.controller.UserController;
import sv.school.model.Cota;
import sv.school.model.Country;
import sv.school.model.Course;
import sv.school.model.Registration;
import sv.school.model.SchoolCourse;
import sv.school.model.SchoolCourse.SchoolCoursePK;
import sv.school.model.User;

public class DataIntegrationController {

	@FXML
	private TextField exportPath;

	@FXML
	private TextField importPath;

	@FXML
	private TextField currentTable;

	@FXML
	private TextField curretnRegister;

	@FXML
	private TextField tablesQuantity;

	@FXML
	private TextField totalRegister;

	@FXML
	private Button exportarDados;

	@FXML
	private Button importarDados;
	@FXML
	private ProgressBar exportationStatus;
	@FXML
	private ProgressBar importationStatus;
	@FXML
	private ProgressBar fileValidationStatus;
	@FXML
	private ProgressBar currentTableStatus;

	@FXML
	private TextArea infoArea;

	@FXML
	private Button setExportationPath;
	@FXML
	private Button setImportationPath;

	@FXML
	private TextField importCurrentRegister;
	@FXML
	private TextField importTotalRegister;

	@FXML
	private TextArea registerErros;
	@FXML
	private TextArea errorLines;

	@FXML
	private Button saveError;
	@FXML
	private Button saveEroorLines;

	@FXML
	private Button print;

	@FXML
	private Button printConfig;

	private static File importFile;

	private static List<Registration> errorRegistrations;

	public void onClickExportar() {
		if (exportPath.getText() == null
				|| exportPath.getText().trim().isEmpty()) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setContentText("Voc\u00ea precisa selecionar uma vig\u00eancia (Primeiro semestre, Segundo semestre, ou ambos)!");
			alert.showAndWait();
		} else {
			String cotasFilePath = "cotas.csv";
			String cursosFilePath = "cursos.csv";
			String paisesFilePath = "paises.csv";
			String tumarsFilePath = "turmas.csv";

			CotaController cController = new CotaController();
			CourseController courseController = new CourseController();
			CountryController countryController = new CountryController();
			SchoolCourseController scController = new SchoolCourseController();

			List<Cota> allCotas = cController.getAll();
			List<Course> allCourses = courseController.getAll();
			List<Country> allCountry = countryController.getAll();
			List<SchoolCourse> allSchoolCourse = scController.getAll();

			File cotasFile = new File(exportPath.getText() + "\\"
					+ cotasFilePath);
			File courseFile = new File(exportPath.getText() + "\\"
					+ cursosFilePath);
			File countryFile = new File(exportPath.getText() + "\\"
					+ paisesFilePath);
			File turmasFile = new File(exportPath.getText() + "\\"
					+ tumarsFilePath);

			String newLine = System.getProperty("line.separator");

			tablesQuantity.setText(4 + "");

			int totalRegister = 0;
			totalRegister = allCotas.size() + allCourses.size()
					+ allCountry.size() + allSchoolCourse.size();
			this.totalRegister.setText(totalRegister + "");
			IntegerProperty score = new SimpleIntegerProperty(0);
			curretnRegister.textProperty().bind(score.asString());
			try {

				if (cotasFile.createNewFile()) {
					currentTable.setText("Cotas");
					infoArea.setText("Exportando cotas...");
					infoArea.appendText("\n");
					FileWriter fWriter = new FileWriter(cotasFile);
					BufferedWriter bWriter = new BufferedWriter(fWriter);
					bWriter.write("C�digo_Cota;Descricao");
					bWriter.write(newLine);
					for (Cota cCota : allCotas) {
						bWriter.write(cCota.getCotaCode() + ";"
								+ cCota.getCotaDescription());
						bWriter.write(newLine);
						totalRegister++;
						if (totalRegister % 10 == 0) {
							score.set(totalRegister);

						}
					}
					bWriter.close();
					exportationStatus.setProgress(0.25);
				}

				if (courseFile.createNewFile()) {
					infoArea.appendText("Exportando Cursos...");
					infoArea.appendText("\n");
					currentTable.setText("Curso");
					FileWriter fWriter = new FileWriter(courseFile);
					BufferedWriter bWriter = new BufferedWriter(fWriter);
					bWriter.write("Codigo_Curso;Descricao;Nivel");
					bWriter.write(newLine);
					for (Course cCourse : allCourses) {
						bWriter.write(cCourse.getCourseCode() + ";"
								+ cCourse.getDescription() + ";"
								+ cCourse.getSchoolLevelCodeId());
						bWriter.write(newLine);
						totalRegister++;
						if (totalRegister % 10 == 0) {
							score.set(totalRegister);

						}
					}

					exportationStatus.setProgress(0.5);
					bWriter.close();
				}
				if (countryFile.createNewFile()) {
					infoArea.appendText("Exportando Pa\u00edses...");
					infoArea.appendText("\n");
					currentTable.setText("Pa\u00edses");
					FileWriter fWriter = new FileWriter(countryFile);
					BufferedWriter bWriter = new BufferedWriter(fWriter);
					bWriter.write("Codigo_Pais;Nacionalidade");
					bWriter.write(newLine);
					for (Country cCountry : allCountry) {
						bWriter.write(cCountry.getCountryCode() + ";"
								+ cCountry.getNationality());
						bWriter.write(newLine);
						totalRegister++;
						if (totalRegister % 10 == 0) {
							score.set(totalRegister);
						}
					}
					exportationStatus.setProgress(0.75);
					bWriter.close();
				}
				if (turmasFile.createNewFile()) {
					infoArea.appendText("Exportando Turmas...");
					infoArea.appendText("\n");
					currentTable.setText("Turmas");
					FileWriter fWriter = new FileWriter(turmasFile);
					BufferedWriter bWriter = new BufferedWriter(fWriter);
					bWriter.write("Codigo_Curso;Codigo_Turma;Descricao;Codigo_Cota;Ano");
					bWriter.write(newLine);
					for (SchoolCourse sc : allSchoolCourse) {
						bWriter.write(sc.getCourseCode() + ";"
								+ sc.getSchoolCoursePK().getSchoolCourseId()
								+ ";" + sc.getDescription() + ";"
								+ sc.getSchemaId() + ";" + sc.getYear());
						bWriter.write(newLine);
						totalRegister++;
						if (totalRegister % 10 == 0) {
							score.set(totalRegister);
						}
					}
					exportationStatus.setProgress(1);
					bWriter.close();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public Date getDateFromString(String stringToFormat) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

		LocalDate ldate = LocalDate.parse(stringToFormat, formatter);

		Instant instant = ldate.atStartOfDay().atZone(ZoneId.systemDefault())
				.toInstant();
		Date date;
		date = Date.from(instant);
		return date;
	}

	public void onCLickImportar() {
		int currentLineCounter = 0;
		RegistrationController rController = new RegistrationController();
		UserController uController = new UserController();
		errorRegistrations = new ArrayList<Registration>();
		SchoolCourseController scController = new SchoolCourseController();
		SchoolController schoolController = new SchoolController();
		if (importFile != null) {

			try {
				BufferedReader bReader = new BufferedReader(new FileReader(
						this.importFile));
				String currentLine;
				currentLine = bReader.readLine();
				if (!currentLine.contains("START")) {
					Alert desassociarAlert = new Alert(AlertType.INFORMATION);
					desassociarAlert.setTitle("Arquivo incorreto");
					desassociarAlert
							.setHeaderText("Algum erro ocorreu com o arquivo!");
					desassociarAlert
							.setContentText("O arquivo selecionado n\u00e3o est\u00e1 padronizado corretamente segundo as"
									+ " normas da SPTRANS");

					desassociarAlert.showAndWait();

				} else {
					System.out.println("o arquivo come\u00e7a com START");

					while (!(currentLine.equals("END") )) {
						currentLine = bReader.readLine();
						if (currentLine.equals("END")) {
							continue;
						}
						// checks if is the first line of the file

						// 0 Nome do Aluno (sem abrevia��o),
						// 1 Matricula,
						// 2 Numero do RG,
						// 3 Digito do RG,
						// 4 Estado Emissor do RG,
						// 5 Data de Emiss�o do RG (no formato dd/mm/yyyy),
						// 6 Sexo (M ou F),
						// 7 Data de Nascimento do aluno (no formato dd/mm/yyyy)
						// 8 N�mero do Endere�o,
						// 9 N�mero do Apartamento,
						// 10 Bloco,
						// 11 Complemento do Endere�o,
						// 12 Cep (oito d�gitos num�ricos sem h�fen),
						// 13 N�mero do Telefone,
						// 14 N�mero do Telefone Celular,
						// 15 E-mail,
						// 16 Nome do Respons�vel (sem abrevia��o),
						// 17 C�digo do Pa�s (ver tabelas exportadas no passo
						// anterior),
						// 18 C�digo do Curso onde o aluno est� matriculado (ver
						// tabelas exportadas),
						// 19 C�digo da Turma onde o aluno est� matriculado (ver
						// tabelas exportadas)
						// 20 CPF (somente algarismos, sem pontua��o),
						// 21 Tipo do CPF (Titular ou Dependente)
						// 22 Tipo de Gratuidade
						// 23 Gratuidade ativa (1 = sim, 0= n�o)
						currentLineCounter++;
						// Deste arquivo ser�o retirados dois objetos, o user, e
						// sua registration!

						String[] regFields = currentLine.split(";");
						User uUser = new User();

						Registration rRegistration = new Registration();

						SimpleDateFormat formatter = new SimpleDateFormat(
								"dd/mm/yyyy");
						try {
							System.out.println("Importando registro #"
									+ currentLineCounter);
							int userId = uController.getLastUserId() + 1;
							uUser.setUserId(userId);
							uUser.setName(regFields[0]);
							uUser.setRegisterName(regFields[1]);
							uUser.setRgNumber(regFields[2]);
							uUser.setRgDigit(regFields[3]);
							uUser.setRgEmittedState(regFields[4]);
							uUser.setRgEmittedDate(formatter
									.parse(regFields[5]));
							uUser.setGender(regFields[6]);
							uUser.setBirthDate(formatter.parse(regFields[7]));
							uUser.setAdressNumber(regFields[8]);
							uUser.setAdressApNumber(regFields[9]);
							uUser.setAdressBlockNumber(regFields[10]);
							uUser.setAdressComplement(regFields[11]);
							uUser.setAdressZipCode(regFields[12]);
							uUser.setTelephone(regFields[13]);
							uUser.setTelephoneMobile(regFields[14]);
							uUser.setEmail(regFields[15]);
							uUser.setResponsible(regFields[16]);
							uUser.setCountryCode(regFields[17]);
							uUser.setCpf(regFields[20]);
							uUser.setCpfType(regFields[21]);

							rRegistration.setCourseCode(regFields[18]);
							rRegistration.setSchoolCourseId(Integer
									.parseInt(regFields[19]));
							rRegistration.setRegisterNumber(regFields[1]);
							rRegistration.setUserId(userId);

							rRegistration.setSchoolId(schoolController.getAll()
									.get(0).getSchoolId());

							SchoolCourse scCurse = scController
									.getById(new SchoolCoursePK(rRegistration
											.getSchoolId(), rRegistration
											.getCourseCode(), rRegistration
											.getSchoolCourseId()));
							if (scCurse != null) {
								rRegistration.setInitialValidDate(scCurse
										.getValidInitialDate());

								rRegistration.setFinalValidDate(scCurse
										.getValidFinalDate());
								rRegistration.setMovementTypeId(0);
								rRegistration.setRegistrationDate(new Date());
								rRegistration.setRegistrationId(rController
										.getLastRegistrationId() + 1);
								rRegistration
										.setSchemaId(scCurse.getSchemaId());
								rRegistration.setRegistrationStatusId(2);// ativo
								rController.persist(rRegistration);
								uController.persist(uUser);
							} else {
								errorRegistrations.add(rRegistration);
								registerErros.appendText("Registro #"
										+ currentLineCounter
										+ "nao foi importado corretamente");

							}

						} catch (ParseException e) {
							// erro de formato da data!
							// salva esse registro de matricula + estudante em
							// um arquivo separado mais tarde
							// para o usuario analisalo
							errorRegistrations.add(rRegistration);
							System.out.println("registro #"
									+ currentLineCounter
									+ "n\u00e3o foi importado corretamente");
							continue;
						}
						System.out.println("registro #"
								+ currentLineCounter
								+ " foi importado corretamente: " + uUser.getName());
					}//	while
					uController.commit();
					System.out.println("COMMITED");
				}



			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void onCLickSetImportPath() {

		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Escolha o arquivo de integra\u00e7\u00e3o");
		fileChooser.getExtensionFilters().add(
				new FileChooser.ExtensionFilter("csv", "*.csv"));
		File importFile = fileChooser.showOpenDialog(null);
		this.importFile = importFile;
		importPath.setText(importFile.getPath());
		// DirectoryChooser chooser = new DirectoryChooser();
		// chooser.setTitle("Escola o caminho para exportar");
		// String path = chooser.showDialog(null).getAbsolutePath();
		// if (path != null) {
		// importPath.setText(path);
		// }
	}

	public void onCLickSetExportPath() {
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setTitle("Escola o caminho para exportar");
		String path = chooser.showDialog(null).getAbsolutePath();
		if (path != null) {
			exportPath.setText(path);
		}
	}

	public void onClickSaveErrorsToFile() {
		System.out.println("onClickSaveErrorsToFile");
	}

	public void onClickSaveProblemsTOFile() {
		System.out.println("onClickSaveProblemsTOfile");
	}

}
