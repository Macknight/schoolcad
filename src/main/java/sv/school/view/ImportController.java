package sv.school.view;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import sv.school.App;
import sv.school.model.School;
import sv.school.util.FileParser;

public class ImportController {

	private static final ExecutorService executor = Executors
			.newCachedThreadPool();
	@FXML
	private Button bNext;
	@FXML
	private Button bCancel;
	@FXML
	private Button bBefore;
	@FXML
	private Pane apMiddlePane;
	@FXML
	private Button bImportFile;

	private static Label status;

	private static Button staticNext;
	private static Button staticBefore;

	@FXML
	private TextField tfSchoolId;
	@FXML
	private TextField tfSchoolName;
	@FXML
	private TextField tfSchoolMecNumber;
	@FXML
	private TextField tfSchoolFantasyName;
	@FXML
	private TextField tfSchoolAdress;
	@FXML
	private TextField tfSchoolNumber;
	@FXML
	private TextField tfSchoolComplement;
	@FXML
	private TextField tfSchoolAdressQuarter;
	@FXML
	private TextField tfSchoolPhone;
	@FXML
	private TextField tfSchoolEmail;

	private static Stage stage;

	private static AnchorPane apMain;

	private static FileParser fileParser;

	private static ProgressBar importProgress;

	private School school;

	private static ImportController itSelf;

	public void setItself(ImportController controller) {
		itSelf = controller;
	}

	private static SchoolReviewController schoolReviewController;
	private static int currentPage = 0;

	public static final int CURRENT_PAGE_INITIAL = 0;

	private static final int CURRENT_PAGE_IMPORT = 1;

	private static final int CURRENT_PAGE_SHOW_SCHOOL = 2;

	private static final int CURRENT_PAGE_IMPORTATION_STATUS = 3;

	private void updateSchoolFromInterface() {
		this.school.setSchoolId(Integer.parseInt(tfSchoolId.getText()));
		this.school.setName(tfSchoolName.getText());
		this.school.setMecNumber(tfSchoolMecNumber.getText());
		this.school.setFantasyName(tfSchoolFantasyName.getText());
		this.school.setAddressStreet(tfSchoolAdress.getText());
		this.school.setAddressNumber(tfSchoolNumber.getText());
		this.school.setAddressComplement(tfSchoolComplement.getText());
		this.school.setAddressQuarter(tfSchoolAdressQuarter.getText());
		this.school.setTelephoneNumber(tfSchoolPhone.getText());
		this.school.setEmail(tfSchoolEmail.getText());
	}

	// get reference to this the windows controlled by this controller
	public void setStage(Stage sStage) {
		this.stage = sStage;
	}

	// gets the root pane thats set into this controllers windows
	public void setMainPane(AnchorPane apPane) {
		this.apMain = apPane;
	}

	public void disableBackButton() {
		bBefore.setDisable(true);
	}

	public void enableBackButton() {
		bBefore.setDisable(false);
	}

	// closes this windows
	public void onClickCancel() {
		stage.close();

	}

	public void onClickNext() {
		if (currentPage == CURRENT_PAGE_IMPORTATION_STATUS) {

			stage.close();
		}
		// if its the first importation page
		if (currentPage == CURRENT_PAGE_INITIAL) {
			loadImportPage();
			currentPage = CURRENT_PAGE_IMPORT;
		}
		if (currentPage == CURRENT_PAGE_SHOW_SCHOOL) {
			loadProgressPage();
			currentPage = CURRENT_PAGE_IMPORTATION_STATUS;
		}
		/*
		 * enable or disable back button
		 */

		if (currentPage == CURRENT_PAGE_IMPORTATION_STATUS) {
			disableNextButton();
			disableBackButton();
		}

		if (currentPage == CURRENT_PAGE_INITIAL) {
			disableBackButton();

		}
		if (currentPage == CURRENT_PAGE_IMPORT) {
			disableNextButton();
			enableBackButton();
		}
		if (currentPage == CURRENT_PAGE_SHOW_SCHOOL) {
			enableNextButton();

		}

	}

	public void setStatus(String s) {
		status.setText(s);

	}

	private void loadProgressPage() {
		AnchorPane oldPane = (AnchorPane) apMain.getChildren().get(0);
		// creates the progressBar to display the file importation
		// progress
		importProgress = new ProgressBar();

		/*
		 * removes the middle pane to set the progres info pane
		 */
		apMain.getChildren().remove(0);
		AnchorPane newPane = new AnchorPane();
		// gets the coordnates from the old pane
		newPane.setLayoutX(oldPane.getLayoutX());
		newPane.setLayoutY(oldPane.getLayoutY());
		// copies the border
		newPane.setBorder(oldPane.getBorder());
		// and the size specs
		newPane.setPrefSize(oldPane.getPrefWidth(), oldPane.getPrefHeight());
		apMain.getChildren().add(0, newPane);
		importProgress.setLayoutX(25);
		importProgress.setLayoutY(20);
		importProgress.setPrefWidth(620);
		status = new Label();
		status.setLayoutX(25);
		status.setLayoutY(40);
		newPane.getChildren().add(importProgress);
		newPane.getChildren().add(status);

		if (fileParser != null) {
			executor.execute(new Runnable() {

				@Override
				public void run() {

					fileParser.setProgressBar(importProgress);
					importProgress.progressProperty().bind(fileParser.dpLines);
					fileParser.instantiateStatus();
					status.textProperty().bind(fileParser.statusString);

					fileParser.getDataFromFile();
				}
			});
		}
		bNext.setText("Concluir");
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				bNext.setDisable(false);
			}
		});
	}

	public void onClickImportFile() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Escolha o arquivo de configuração");
		fileChooser.getExtensionFilters().add(
				new FileChooser.ExtensionFilter("DAT", "*.DAT"));
		File importFile = fileChooser.showOpenDialog(stage);

		fileParser = new FileParser();
		if (importFile != null) {
			fileParser.setFile(importFile);
			school = fileParser.getSchoolFromFile(false);
			displaySchoolReview(this.school);
			schoolReviewController.setAppSChool(this.school);

			itSelf.enableNextButton();
		}
		itSelf.enableBackButton();

	}

	public void onClickBack() {

		if (currentPage == CURRENT_PAGE_IMPORT) {
			loadInitialPage();

			currentPage = CURRENT_PAGE_INITIAL;
		}
		if (currentPage == CURRENT_PAGE_SHOW_SCHOOL) {
			loadImportPage();
			currentPage = CURRENT_PAGE_IMPORT;
		}
		// tratamento dos botoes
		if (currentPage == CURRENT_PAGE_INITIAL) {
			disableBackButton();
		} else {
			enableBackButton();
		}

		if (currentPage == CURRENT_PAGE_IMPORT) {
			disableNextButton();
		} else {
			enableNextButton();
		}

	}

	public void loadImportPage() {
		FXMLLoader loader = new FXMLLoader(
				App.class.getResource("view/ImportPane.fxml"));

		try {

			apMiddlePane = (AnchorPane) loader.load();
			double xpos = apMain.getChildren().get(0).getLayoutX();
			double ypos = apMain.getChildren().get(0).getLayoutY();
			apMain.getChildren().remove(0);
			apMain.getChildren().add(0, apMiddlePane);
			apMain.getChildren().get(0).setLayoutX(xpos);
			apMain.getChildren().get(0).setLayoutY(ypos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	private void initialize() {

	}

	private void disableNextButton() {
		bNext.setDisable(true);
	}

	public void enableNextButton() {
		bNext.setDisable(false);
	}

	private void loadInitialPage() {

		FXMLLoader importarLoader = new FXMLLoader(
				App.class.getResource("view/dataImportView.fxml"));
		try {
			AnchorPane apImport = (AnchorPane) importarLoader.load();
			apMain.getChildren().remove(0);
			apMain.getChildren().add(0, apImport.getChildren().get(0));

			// controller.disableBackButton();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void displaySchoolReview(School school) {
		FXMLLoader loader = new FXMLLoader(
				App.class.getResource("view/ImportSchoolReview.fxml"));
		staticNext = bNext;
		try {
			// loads schoolReview screen
			AnchorPane schoolReview = loader.load();
			// gets x and y pos of the main anchorPane
			double xpos = apMain.getChildren().get(0).getLayoutX();
			double ypos = apMain.getChildren().get(0).getLayoutY();
			// removes inner screen to replace with the schoolReview
			apMain.getChildren().remove(0);
			apMain.getChildren().add(0, schoolReview);
			// set the schoolReview screen to have the same x and y positioning
			// from the previous screen
			apMain.getChildren().get(0).setLayoutX(xpos);
			apMain.getChildren().get(0).setLayoutY(ypos);
			schoolReviewController = loader.getController();
			schoolReviewController.setImportController(this);
			if (schoolReviewController == null) {
				System.out.println("controller eh null");
				// tfSchoolId.setText(Integer.toString(school.getSchoolId()));
			} else {
				schoolReviewController.setSchoolFields(school);
			}

			currentPage = CURRENT_PAGE_SHOW_SCHOOL;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void initializeCurrentPage(int currentPageI) {
		currentPage = currentPageI;
	}

	public void setSchoolFields(School sSchool) {
		this.tfSchoolId.setText(Integer.toString(sSchool.getSchoolId()));
		this.tfSchoolName.setText(sSchool.getName());
		this.tfSchoolMecNumber.setText(sSchool.getMecNumber());
		this.tfSchoolFantasyName.setText(sSchool.getFantasyName());
		this.tfSchoolAdress.setText(sSchool.getAddressStreet());
		this.tfSchoolNumber.setText(sSchool.getAddressNumber());
		this.tfSchoolComplement.setText(sSchool.getAddressComplement());
		this.tfSchoolPhone.setText(sSchool.getTelephoneNumber());
		this.tfSchoolEmail.setText(sSchool.getEmail());
		this.tfSchoolAdressQuarter.setText(sSchool.getAddressQuarter());

	}

}
