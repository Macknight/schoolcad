package sv.school.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import sv.school.controller.CotaController;
import sv.school.controller.CountryController;
import sv.school.controller.CourseController;
import sv.school.controller.OfferedCoursesController;
import sv.school.controller.RegistrationController;
import sv.school.controller.SchoolContactsController;
import sv.school.controller.SchoolController;
import sv.school.controller.SchoolCourseController;
import sv.school.controller.UserController;
import sv.school.model.Cota;
import sv.school.model.Country;
import sv.school.model.Course;
import sv.school.model.OfferedCourses;
import sv.school.model.Registration;
import sv.school.model.School;
import sv.school.model.SchoolContacts;
import sv.school.model.SchoolCourse;
import sv.school.model.User;

/**
 * 
 * @author rodrigo.poloni 20/05/2015 This is a class that receives the
 *         configuration file and parses the data to the h2 database. The file
 *         to be imported has the following structure:
 */
// #0 --> tells where the SCHOOL definition is
// #1 --> Users
// #2 --> tells where the schoolContacts are defined
// #3 --> registration
// #4 --> asOfferedCourses
// #5 --> schoolCourses

// SCHOOL FORMAT #0
// schoolDivision[0] SCHOOL_ID
// schoolDivision[1] NAME
// schoolDivision[2] FANTASY_NAME
// schoolDivision[3] ADDRESS_STREET
// schoolDivision[4] ADDRESS_NUMBER
// schoolDivision[5] ADDRESS_COMPLEMENT
// schoolDivision[6] ADDRESS_QUARTER
// schoolDivision[7] ADDRESS_ZIP_CODE
// schoolDivision[8] TELEPHONE_NUMBER
// schoolDivision[9] TELEPHONE_BRANCH
// schoolDivision[10] EMAIL
// schoolDivision[11] MEC_NUMBER
// schoolDivision[12] INSERT_DT
// schoolDivision[13] ESTABLISHMENT_TYPE_ID
// schoolDivision[14] SCHOOL_LEVEL_FLAGS ==> this field aint used in this app
// schoolDivision[15] SCHOOL_LEVEL_TYPE_ID
// schoolDivision[16] SCHOOL_STATUS_ID
// schoolDivision[17] XID
// schoolDivision[18] COMMENTS

// USERS #1
// user[0] ========USER_ID
// user[1] ========NAME
// user[2] ========REG_NUMBER
// user[3] ========RG
// user[4] ========RG DIGIT
// user[5] ========RG STATE
// user[6] ========RG DATE -
// user[7] ========GENDER
// user[8] ========BIRTH
// user[9] ========COUNTRY
// user[10] ======DRESS_STREET
// user[11] ======ADDRESS_NUMBER
// user[12] ======AP_NUM
// user[13] ======BLOCK_NUM
// user[14] ======COMPLEMENT
// user[15] ======QUARTER
// user[16] ======ZIP
// user[17] ======TEL
// user[18] ======TEL MOBILE
// user[19] ======EMAIL
// user[20] ======EMAIL OPT
// user[21] ======RESPONSI
// user[22] =======RG DATE
// user[23] =======USER_STATUS_ID
// user[24] =======USER_TYPE_ID
// user[25] =======XID
// user[26] =======COMMENTS
// user[27]
// user[28] == CPF (if user.size == 28)
// user[29] == TIPO CPF (if user.size == 29)

// School contact format #2
// schoolContatc[0] SCHOOL_CONTACT_ID
// schoolContatc[1] SCHOOL_ID
// schoolContatc[2] NAME
// schoolContatc[3] RG_NUMBER
// schoolContatc[4] RG_DIGIT
// schoolContatc[5] RG_EMITTED_STATE
// schoolContatc[6] RG_EMITTED_DT
// schoolContatc[7] EMAIL
// schoolContatc[8] TELEPHONE_NUMBER
// schoolContatc[9] TELEPHONE_BRANCH
// schoolContatc[10] SCHOOL_CONTACT_TYPE_ID
// schoolContatc[11] SCHOOL_CONTACT_STATUS_ID
// schoolContatc[12] XID
// schoolContatc[13] COMMENTS

// registration format #3
// registration[0] == REGISTRATION_ID
// registration[1] == REGISTER_NUMBER
// registration[2] == USER_ID
// registration[3] == SCHOOL_ID
// registration[4] == COURSE_CODE
// registration[5] == SCHOOL_COURSE_ID
// registration[6] == REGISTRATION_STATUS_ID
// registration[7] == MOVEMENT_TYPE_ID
// registration[8] == SCHEMA_ID
// registration[9] == REGISTRATION_DT
// registration[10] == INITIAL_VALID_DT
// registration[11] == FINAL_VALID_DT - Sem XID

// offeredCoursesFormat #4
// offeredCourses[0] == OFFERED_COURSE_ID
// offeredCourses[1] == SCHOOL_ID
// offeredCourses[2] == COURSE_CODE
// offeredCourses[3] == SCHEMA_ID
// offeredCourses[4] == SYS_YEAR
// offeredCourses[5] == PERIOD_TYPE_ID

// SchoolCourses #5
// schoolCourse[0] == SCHOOL_ID
// schoolCourse[1] == COURSE_CODE
// schoolCourse[2] == SCHOOL_COURSE_ID
// schoolCourse[3] == DESCRIPTION
// schoolCourse[4] == GRADE
// schoolCourse[5] == SCHEMA_ID
// schoolCourse[6] == PERIOD_TYPE_ID
// schoolCourse[7] == SYS_YEAR
// schoolCourse[8] == VALID_INITIAL_DT
// schoolCourse[9] == VALID_FINAL_DT
// schoolCourse[10] = STUDENT_QUANTITY=
// schoolCourse[11] = SCHOOL_COURSE_STATUS_ID=
// schoolCourse[12] = VIGENCIA_TYPE_ID=
// schoolCourse[13] = PERIOD_ID=
// schoolCourse[14] == XID

public class FileParser {
	private File file;

	private School school;

	public StringProperty statusString;
	private List<SchoolContacts> schoolContatcs;

	private String currentLine;
	private String schoolLine;

	private DateTimeFormatter formatter;
	private BufferedReader fileReader;

	private boolean controller = true;

	public DoubleProperty dpLines;
	public DoubleProperty exportationPorgress;
	private int fileLines;
	private int currentLineCounter = 0;

	int studentsCounter;
	int registrationCounter;
	int schoolCourseCounter;
	int oferredCoursesCounter;
	int contactCounter;
	private static final String FILE_PARSER_VERSION = "1";
	String newline = System.getProperty("line.separator");

	String errorAdvisor = "";

	public void setProgressBar(ProgressBar importProgress) {
		dpLines = new SimpleDoubleProperty(0);

	}

	public School getSchoolFromFile(boolean persist) {
		try {

			fileReader = new BufferedReader(new FileReader(file));
			while ((currentLine = readLine()) != null) {

				if (currentLine.contains("#0")) {
					schoolLine = readLine();
					String[] schoolDivision = schoolLine.split(";");

					if (controller) {
						Date date = getDateFromString(schoolDivision[12]);
						school = new School(
								Integer.parseInt(schoolDivision[0]),
								schoolDivision[1], schoolDivision[2],
								schoolDivision[3], schoolDivision[4],
								schoolDivision[5], schoolDivision[6],
								schoolDivision[7], schoolDivision[8],
								schoolDivision[9], schoolDivision[10],
								schoolDivision[11], date, schoolDivision[13],
								Integer.parseInt(schoolDivision[15]),
								Integer.parseInt(schoolDivision[16]),
								Integer.parseInt(schoolDivision[17]),
								schoolDivision[18]);

					}

					if (persist) {
						SchoolController controller = new SchoolController();
						controller.persist(school);
						// increases progressbar counter
						updateProgress();
					}

					break;
				}

			}
			// fileReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return school;

	}

	public void updateProgress() {
		float prog = (float) currentLineCounter / fileLines;

		dpLines.set(prog); // variable being watched by the progressbar
		// importProgress.setProgress(prog);
	}

	/**
	 * Simple method to format the dates that comes from the file into an actual
	 * Date object with the dd/mm/yyyy format
	 * 
	 * @param String
	 *            schoolDivision
	 * @return Date the date that was in the string
	 */
	// checks if the fileReader is already poiting towards
	// the SchoolContatcs area o fthe File.

	public Date getDateFromString(String stringToFormat) {
		formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

		LocalDate ldate = LocalDate.parse(stringToFormat, formatter);

		Instant instant = ldate.atStartOfDay().atZone(ZoneId.systemDefault())
				.toInstant();
		Date date;
		date = Date.from(instant);
		return date;
	}

	/**
	 * 
	 * Method that parses the data from the .DAT file and build the objects to
	 * save to the database
	 */
	public void getDataFromFile() {

		statusString.set("Deletando dados antigos");

		dpLines.set(0);

		SchoolController schoolController = new SchoolController();

		if (schoolController.getAll() != null) {
			schoolController.deleteAll();
		}
		UserController userController = new UserController();
		if (userController.getAll() != null) {
			userController.deleteAll();
		}

		SchoolContactsController contactsController = new SchoolContactsController();
		if (contactsController.getAll() != null) {
			contactsController.deleteAll();
		}

		RegistrationController regController = new RegistrationController();
		if (regController.getAll() != null) {
			regController.deleteAll();
		}
		OfferedCoursesController offeredController = new OfferedCoursesController();
		if (offeredController.getAll() != null) {
			offeredController.deleteAll();
		}

		SchoolCourseController courseController = new SchoolCourseController();
		if (courseController.getAll() != null) {
			courseController.deleteAll();
		}

		// the true parameter indicates that the method should also save the
		// school
		// to the database
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				statusString.set("Arquivo sendo processado");

			}
		});
		getSchoolFromFile(true);

		// students

		importStudents();

		// schoolcontacts

		importSchoolContacts();
		// registrations

		importRegistrations();
		// offeredcourses

		importOfferedCourses();
		// schoolCourses

		importSchoolCourses();
		schoolController.commit();
		importCotas();
		importCountry();
		importCourses();
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				statusString.set(statusString.get() + newline
						+ "Arquivo importado");

			}
		});

	}

	private void importSchoolCourses() {
		List<SchoolCourse> schoolCourses = new ArrayList<SchoolCourse>();
		int counter = 0;
		String courseName = "";
		try {
			if (!currentLine.contains("#5")) {
				while (!(currentLine = readLine()).contains("#5")) {
					// blank while just to set the reader to the registrations
					// field
				}
			}
			SchoolCourseController controller = new SchoolCourseController();
			SchoolCourse currentSchoolCourse;

			while ((currentLine = readLine()) != null) {
				counter++;
				try {
					String[] coursesField = currentLine.split(";");

					Date initDate = getDateFromString(coursesField[8]);
					Date FinalDate = getDateFromString(coursesField[9]);
					currentSchoolCourse = new SchoolCourse(
							Integer.parseInt(coursesField[0]), coursesField[1],
							Integer.parseInt(coursesField[2]), coursesField[3],
							coursesField[4], Integer.parseInt(coursesField[5]),
							Integer.parseInt(coursesField[6]), coursesField[7],
							initDate, FinalDate,
							Integer.parseInt(coursesField[10]),
							Integer.parseInt(coursesField[11]),
							Integer.parseInt(coursesField[12]),
							Integer.parseInt(coursesField[13]),
							Integer.parseInt(coursesField[14]));

					controller.persist(currentSchoolCourse);
					errorAdvisor = currentSchoolCourse.getDescription();
					currentSchoolCourse = null;
					if (currentLineCounter % 5 == 0) {
						updateProgress();
					}

				} catch (Exception e) {
					Alert alert = new Alert(
							AlertType.WARNING,
							"O curso oferecido #"
									+ courseName
									+ " n�o foi importado corretamente,"
									+ "por favor, verificar integridade do arquivo ");
					alert.show();

					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							Alert alert = new Alert(
									AlertType.WARNING,
									"O curso oferecido #"
											+ errorAdvisor
											+ " n�o foi importado corretamente,"
											+ "por favor, verificar integridade do arquivo ");
							alert.show();
						}
					});

					continue;
				}
				schoolCourses.add(currentSchoolCourse);

			}
			schoolCourseCounter = counter;
		} catch (IOException e) {

			e.printStackTrace();

		} finally {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					statusString.set(statusString.get() + newline
							+ schoolCourseCounter + " Turmas importadas");

				}
			});
		}
		Set<SchoolCourse> set = new HashSet<SchoolCourse>(schoolCourses);
		if (set.size() < schoolCourses.size()) {
			System.out.println("set = " + set.size());
			System.out.println("list = " + schoolCourses.size());
		}

	}

	private void importOfferedCourses() {
		int counter = 0;
		String courseID = "";

		OfferedCoursesController offeredController = new OfferedCoursesController();
		if (offeredController.getAll() != null) {
			offeredController.deleteAll();
		}

		try {
			if (!currentLine.contains("#4")) {
				while (!(currentLine = readLine()).contains("#4")) {
					// blank while just to set the reader to the registrations
					// field
				}
			}
			OfferedCourses currentCourse;
			while (!(currentLine = readLine()).contains("#5")) {
				try {
					counter++;
					String[] offeredFields = currentLine.split(";");
					currentCourse = new OfferedCourses(
							Long.parseLong(offeredFields[0]),
							Long.parseLong(offeredFields[1]), offeredFields[2],
							Integer.parseInt(offeredFields[3]),
							offeredFields[4],
							Integer.parseInt(offeredFields[5]));
					errorAdvisor = offeredFields[0];
					offeredController.persist(currentCourse);

					if (currentLineCounter % 5 == 0) {
						updateProgress();
					}
					currentCourse = null;
				} catch (Exception e) {

					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							Alert alert = new Alert(
									AlertType.WARNING,
									"O curso oferecido #"
											+ errorAdvisor
											+ " n�o foi importado corretamente,"
											+ "por favor, verificar integridade do arquivo ");
							alert.show();

						}
					});

					continue;
				}
			}
			oferredCoursesCounter = counter;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					statusString.set(statusString.get() + newline
							+ oferredCoursesCounter
							+ " cursos oferecidos importados");

				}
			});
		}

	}

	private String readLine() throws IOException {
		String s = fileReader.readLine();
		if (s != null) {
			currentLineCounter++;
		}

		return s;
	}

	private void importRegistrations() {
		int counter = 0;

		try {

			RegistrationController regController = new RegistrationController();

			if (!currentLine.contains("#3")) {
				while (!(currentLine = readLine()).contains("#3")) {
					// blank while just to set the reader to the registrations
					// field
				}
			}
			Registration currentRegistration;
			while (!(currentLine = readLine()).contains("#4")) {
				try {
					String[] regFields = currentLine.split(";");

					Date regDate = getDateFromString(regFields[9]);
					Date initDate = getDateFromString(regFields[10]);
					Date finalDate = getDateFromString(regFields[11]);
					currentRegistration = new Registration(
							Integer.parseInt(regFields[0]), regFields[1],
							Integer.parseInt(regFields[2]),
							Integer.parseInt(regFields[3]), regFields[4],
							Integer.parseInt(regFields[5]),
							Integer.parseInt(regFields[6]),
							Integer.parseInt(regFields[7]),
							Integer.parseInt(regFields[8]), regDate, initDate,
							finalDate);
					errorAdvisor = regFields[0];
					if (currentRegistration != null) {
						regController.persist(currentRegistration);
					}
					// updating progressBar for the user

					if (currentLineCounter % 5 == 0) {

						updateProgress();
					}
					currentRegistration = null;
					counter++;
				} catch (Exception e) {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							Alert alert = new Alert(
									AlertType.WARNING,
									"A matricula "
											+ errorAdvisor
											+ "n�o foi importado corretamente,"
											+ "por favor, verificar integridade do arquivo ");
							alert.show();

						}
					});
					continue;

				}
				registrationCounter = counter;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					statusString.set(statusString.get() + newline
							+ registrationCounter
							+ " registros de matr�cula importados");

				}
			});
		}

	}

	public void importSchoolContacts() {
		SchoolContactsController contactsController = new SchoolContactsController();
		int counter = 0;
		String contactName = "";
		try {
			// checks if the fileReader is already poiting towards
			// the SchoolContatcs area o fthe File.
			if (!currentLine.contains("#2")) {
				while (!(currentLine = readLine()).contains("#2")) {
					// blank while just to setup the filereader
				}
			}
			SchoolContacts currentContact;
			while (!(currentLine = readLine()).contains("#3")) {
				try {
					counter++;
					String[] contactsField = currentLine.split(";");
					if (contactsField.length == 14) {

						Date rgEmitedDate = getDateFromString(contactsField[6]);
						currentContact = new SchoolContacts(
								Integer.parseInt(contactsField[0]),
								Integer.parseInt(contactsField[1]),
								contactsField[2], contactsField[3],
								contactsField[4], contactsField[5],
								rgEmitedDate, contactsField[7],
								contactsField[8], contactsField[9],
								Integer.parseInt(contactsField[10]),
								Integer.parseInt(contactsField[11]),
								contactsField[12], contactsField[13]);
						contactsController.persist(currentContact);
					}
					if (contactsField.length == 13) {

						Date rgEmitedDate = getDateFromString(contactsField[6]);
						currentContact = new SchoolContacts(
								Integer.parseInt(contactsField[0]),
								Integer.parseInt(contactsField[1]),
								contactsField[2], contactsField[3],
								contactsField[4], contactsField[5],
								rgEmitedDate, contactsField[7],
								contactsField[8], contactsField[9],
								Integer.parseInt(contactsField[10]),
								Integer.parseInt(contactsField[11]),
								contactsField[12]);
						contactsController.persist(currentContact);

						//
						//
						if (currentLineCounter % 5 == 0) {
							updateProgress();
						}
						currentContact = null;
					}
					errorAdvisor = contactsField[2];
				} catch (Exception e) {

					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							e.printStackTrace();
							Alert alert = new Alert(
									AlertType.WARNING,
									"O contato "
											+ errorAdvisor
											+ "n�o foi importado corretamente,"
											+ "por favor, verificar integridade do arquivo ");
							alert.show();

						}
					});
					continue;
				}

			}
			contactCounter = counter;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					statusString.set(statusString.get() + newline
							+ contactCounter + " contatos  importados");

				}
			});
		}

	}

	public void importStudents() {
		int counter = 0;

		try {
			UserController userController = new UserController();
			String userName = "";
			// In the importation of the .DAT file
			// clear all previous data from this program's instance
			if (userController.getAll() != null) {
				userController.deleteAll();
			}
			User currentUser = null;
			while ((currentLine = readLine()) != null) {

				if (currentLine.contains("#1")) {
					// start the users importation
					// reads until it finds the next separator
					while (!(currentLine = readLine()).contains("#2")) {
						try {
							counter++;

							// breaks the line into an array of strings
							String[] userLine = currentLine.split(";");

							Date rgDate = getDateFromString(userLine[6]);
							Date birthDate = getDateFromString(userLine[8]);
							Date registrationDate = getDateFromString(userLine[22]);

							if (userLine.length == 30) {
								// contem cpf, e cpf type

								System.out.println(userLine[28]);
								System.out.println(userLine[29]);
								currentUser = new User(
										Integer.parseInt(userLine[0]),
										userLine[1], userLine[2], userLine[3],
										userLine[4], userLine[5], rgDate,
										userLine[7], birthDate, userLine[9],
										userLine[10], userLine[11],
										userLine[12], userLine[13],
										userLine[14], userLine[15],
										userLine[16], userLine[17],
										userLine[18], userLine[19],
										userLine[20], userLine[21],
										registrationDate,
										Integer.parseInt(userLine[23]),
										Integer.parseInt(userLine[24]),
										Integer.parseInt(userLine[25]),
										userLine[26], userLine[28],
										userLine[29]);
								// the userLine[27] is ignored by this app
								// TODO: set the user Cpf
								// TODO: set the user Cpf_type

							} else if (userLine.length == 29) {
								System.out.println(userLine[28]);

								currentUser = new User(
										Integer.parseInt(userLine[0]),
										userLine[1], userLine[2], userLine[3],
										userLine[4], userLine[5], rgDate,
										userLine[7], birthDate, userLine[9],
										userLine[10], userLine[11],
										userLine[12], userLine[13],
										userLine[14], userLine[15],
										userLine[16], userLine[17],
										userLine[18], userLine[19],
										userLine[20], userLine[21],
										registrationDate,
										Integer.parseInt(userLine[23]),
										Integer.parseInt(userLine[24]),
										Integer.parseInt(userLine[25]),
										userLine[26], userLine[28]);

							} else if (userLine.length == 28) {

								currentUser = new User(
										Integer.parseInt(userLine[0]),
										userLine[1], userLine[2], userLine[3],
										userLine[4], userLine[5], rgDate,
										userLine[7], birthDate, userLine[9],
										userLine[10], userLine[11],
										userLine[12], userLine[13],
										userLine[14], userLine[15],
										userLine[16], userLine[17],
										userLine[18], userLine[19],
										userLine[20], userLine[21],
										registrationDate,
										Integer.parseInt(userLine[23]),
										Integer.parseInt(userLine[24]),
										Integer.parseInt(userLine[25]),
										userLine[26]);
							}
							if (currentUser != null) {
								userController.persist(currentUser);
								errorAdvisor = currentUser.getName();
							}

							if (currentLineCounter % 5 == 0) {

								updateProgress();
							}
						} catch (Exception e) {

							Platform.runLater(new Runnable() {
								@Override
								public void run() {
									e.printStackTrace();
									Alert alert = new Alert(
											AlertType.WARNING,
											"O usu�rio "
													+ errorAdvisor
													+ "n�o foi importado corretamente,"
													+ "por favor, verificar integridade do arquivo ");
									alert.show();

								}
							});
							continue;
						}
					}
					currentUser = null;
					System.out.println("alunos importados: #" + counter);
					studentsCounter = counter;
					break;
				}

			}
		} catch (IOException e) {

			e.printStackTrace();

		} finally {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					statusString.set(statusString.get() + newline
							+ studentsCounter + " Alunos importados");

				}
			});
		}
	}

	public File getFile() {
		return file;
	}

	public School getSchool() {
		return school;
	}

	public List<SchoolContacts> getSchoolContatcs() {
		return schoolContatcs;
	}

	public void setFile(File file) {
		this.file = file;
		LineNumberReader lnr;
		try {
			lnr = new LineNumberReader(new FileReader(file));
			lnr.skip(Long.MAX_VALUE);
			fileLines = lnr.getLineNumber() + 1; // Add 1 because line
													// index
													// starts at 0
			// Finally, the LineNumberReader object should be closed to prevent
			// resource leak

			lnr.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setSchool(School school) {
		this.school = school;
	}

	public void setSchoolContatcs(List<SchoolContacts> schoolContatcs) {
		this.schoolContatcs = schoolContatcs;
	}

	public void instantiateStatus() {
		statusString = new SimpleStringProperty();

	}

	/**
	 * 
	 * @param pathToExport
	 *            Is the path of the directory in which the file is going to be
	 *            stored
	 * @param prompt
	 *            is the placeholder that show the exportation status to the
	 *            user
	 * @param progress
	 *            of the the exportation
	 */
	public void exportDataToSptrans(String pathToExport, TextArea prompt,
			ProgressBar progress, boolean exportaAllData) {
		// calling all the controllers that will gather the data to export
		SchoolController schoolController = new SchoolController();
		RegistrationController regController = new RegistrationController();
		SchoolCourseController scController = new SchoolCourseController();
		UserController userController = new UserController();
		SchoolContactsController contactsController = new SchoolContactsController();
		exportationPorgress = new SimpleDoubleProperty();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
		Date date = new Date();

		School school = schoolController.getAll().get(0);

		String fileName = "SCA_" + school.getSchoolId() + "_"
				+ dateFormat.format(date) + ".EXP";
		File exportFile = new File(pathToExport + "\\" + fileName);
		System.out.println(pathToExport + fileName);
		String newLine = System.getProperty("line.separator");
		try {
			if (exportFile.createNewFile()) {
				prompt.setText("Exportando os dados \n");
				exportationPorgress.set(0.1);
				FileWriter fWriter = new FileWriter(exportFile);
				BufferedWriter bWriter = new BufferedWriter(fWriter);
				bWriter.write("START-" + FILE_PARSER_VERSION + "-" + fileName);
				bWriter.write(System.getProperty("line.separator"));
				bWriter.write("#1");
				bWriter.write(System.getProperty("line.separator"));
				if (exportaAllData) {
					prompt.appendText("Exportando escola \n");

					exportationPorgress.set(0.2);
					writeSchool(schoolController, bWriter);

				}
				bWriter.write("#2");
				bWriter.write(System.getProperty("line.separator"));
				writeRegistrations(regController, bWriter);
				prompt.appendText("Exportando matriculas \n");
				exportationPorgress.set(0.4);
				bWriter.write("#3");
				bWriter.write(System.getProperty("line.separator"));
				if (exportaAllData) {
					prompt.appendText("Exportando Turmas \n");
					exportationPorgress.set(0.6);
					writeSchoolCourses(scController, bWriter);
				}
				bWriter.write("#4");
				bWriter.write(System.getProperty("line.separator"));
				if (exportaAllData) {
					prompt.appendText("Exportando Turmas \n");
					exportationPorgress.set(0.8);
					writeUsers(userController, bWriter);
				}
				bWriter.write("#5");
				prompt.appendText("Exportando Contatos \n");
				exportationPorgress.set(1);
				bWriter.write(System.getProperty("line.separator"));
				bWriter.write("END");
				bWriter.close();

				System.out.println("done!");
				prompt.appendText("Exporta��o finalizada");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void writeUsers(UserController userController, BufferedWriter bWriter)
			throws IOException {
		List<User> allUsers = userController.getAll();
		for (User users : allUsers) {
			bWriter.write(users.toCVS());
			bWriter.write(System.getProperty("line.separator"));

		}
	}

	public void writeSchoolCourses(SchoolCourseController scController,
			BufferedWriter bWriter) throws IOException {
		List<SchoolCourse> allSchoolCourses = scController.getAll();
		for (SchoolCourse schoolCourse : allSchoolCourses) {
			bWriter.write(schoolCourse.toCSV());
			bWriter.write(System.getProperty("line.separator"));

		}
	}

	public void writeRegistrations(RegistrationController regController,
			BufferedWriter bWriter) throws IOException {
		List<Registration> allRegistrations = regController.getAll();
		for (Registration reg : allRegistrations) {
			bWriter.write(reg.toCSV());
			bWriter.write(System.getProperty("line.separator"));

		}
	}

	public void writeSchool(SchoolController schoolController,
			BufferedWriter bWriter) throws IOException {
		bWriter.write(schoolController.getAll().get(0).toCSV());

	}

	public void importCourses() {
		List<Course> lCourses = new ArrayList<Course>();
		String filePath = new File("").getAbsolutePath();

		String fullpath = filePath.concat("\\resources\\SPT_COURSE.csv");

		File coursesFile = new File(fullpath);
		String line;
		Course sCourse;
		CourseController cController = new CourseController();
		if (coursesFile.exists()) {
			try {
				BufferedReader coursesReader = new BufferedReader(
						new FileReader(coursesFile));
				while ((line = coursesReader.readLine()) != null) {
					String[] courseField = line.split(";");
					sCourse = new Course(courseField[0], courseField[1],
							Integer.parseInt(courseField[2]),
							Integer.parseInt(courseField[3]), "");
					if (sCourse != null) {

						cController.persist(sCourse);

					}
					sCourse = null;
				}
				System.out.println("importa��o finalizada");

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		cController.commit();
	}

	public void importCountry() {
		List<Country> lCountry = new ArrayList<Country>();
		String filePath = new File("").getAbsolutePath();
		String fullpath = filePath.concat("\\resources\\paises.csv");
		File countryFile = new File(fullpath);
		String line;
		Country cCountry;
		CountryController cController = new CountryController();
		if (countryFile.exists()) {
			try {
				BufferedReader countryReader = new BufferedReader(
						new FileReader(countryFile));
				while ((line = countryReader.readLine()) != null) {
					String[] countryField = line.split(";");
					cCountry = new Country();
					cCountry.setCountryCode(countryField[0]);
					cCountry.setNationality(countryField[1]);
					cController.persist(cCountry);
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		cController.commit();

	}

	public void importCotas() {
		List<Cota> lCountry = new ArrayList<Cota>();
		String filePath = new File("").getAbsolutePath();
		String fullpath = filePath.concat("\\resources\\cotas.csv");
		File cotaFile = new File(fullpath);
		String line;
		Cota cCota;
		CotaController cController = new CotaController();
		if (cotaFile.exists()) {
			try {
				BufferedReader cotaReader = new BufferedReader(new FileReader(
						cotaFile));
				while ((line = cotaReader.readLine()) != null) {
					String[] countryField = line.split(";");
					cCota = new Cota();
					cCota.setCotaCode(Integer.parseInt(countryField[0]));
					cCota.setCotaDescription(countryField[1]);
					cController.persist(cCota);
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		cController.commit();
	}
}
