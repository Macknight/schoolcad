package sv.school.view;

import java.io.IOException;
import java.util.Optional;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sv.school.App;
import sv.school.controller.CourseController;
import sv.school.controller.OfferedCoursesController;
import sv.school.controller.SchoolContactsController;
import sv.school.controller.SchoolController;
import sv.school.model.User;

public class RootLayoutController {
	@FXML
	private Label contentDescriptor;
	@FXML
	private Button bUnidadeEnsino;
	@FXML
	private Button bTurmas;
	@FXML
	private Button bAlunos;
	@FXML
	private Button bMatricular;
	@FXML
	private Button bRematricular;
	@FXML
	private Button bExportarDados;
	@FXML
	private Button bImportarDados;
	@FXML
	private Button bIntegracaoDados;
	@FXML
	private Button bRelatorios;
	@FXML
	private Button bBackup;
	@FXML
	private Button bAjuda;
	@FXML
	private Button bSair;
	@FXML
	private Pane centerPane;
	@FXML
	private Button bEscola;
	@FXML
	private Button bContatos;
	@FXML
	private Button bVoltar;
	@FXML
	private Button bProcurar;
	@FXML
	private Button bEditarTurma;
	@FXML
	private Button bSalvar;
	@FXML
	private Button bCancelar;
	@FXML
	private Button bApagar;
	static App mainApp;
	private static TurmasController turmasStaticController;
	private static BorderPane rootLayout;

	public static boolean bNewCourse = false;

	private SchoolCOurseDetailsController schoolCOurseDetailsController;
	private SpecificSchoolCourseViewController specificCourseController;

	public void setContentDescription(String desc) {
		this.contentDescriptor.setText(desc);
	}

	public void setMainApp(App mainApplication) {
		mainApp = mainApplication;
	}

	public void setRootLayout(BorderPane rootLayout) {
		this.rootLayout = rootLayout;
	}

	public RootLayoutController() {

	}

	@FXML
	public void onHoverOut() {
		setContentDescription("Selecione uma categoria.");
	}

	@FXML
	public void onHoverUnidadeEnsino() {
		setContentDescription("Dados Cadastrais da Unidade de Ensino.");
	}

	@FXML
	public void onHoverTurmas() {
		setContentDescription("Cadastro de Turmas da Unidade de Ensino.");
	}

	@FXML
	public void onHoverAlunos() {
		setContentDescription("Cadastro de alunos na Unidade de Ensino.");
	}

	@FXML
	public void onHoverMatricular() {
		setContentDescription("Matricular aluno em turma.");
	}

	@FXML
	public void onHoverRematricular() {
		setContentDescription("Desmatricula aluno e rematricula em turma diferente.");
	}

	@FXML
	public void onHoverExportarDados() {
		setContentDescription("Exporta dados em formato .EXP para ser enviado a SPTRANS.");
	}

	@FXML
	public void onHoverImportarDados() {
		setContentDescription("Atualiza o banco de dados.");
	}

	@FXML
	public void onHoverIntegracaoDados() {
		setContentDescription("Permite a integra��o de uma grande quantidade de alunos.");
	}

	@FXML
	public void onHoverRelatorios() {
		setContentDescription("Relatorios sobre a escola");
	}

	@FXML
	public void onHoverBackup() {
		setContentDescription("Backup de todos os dados.");
	}

	@FXML
	public void onHoverAjuda() {
		setContentDescription("Ajuda");
	}

	@FXML
	public void onHoverSaida() {
		setContentDescription("Sair do aplicativo.");
	}

	@FXML
	public void onClickSaida() {

		Platform.exit();
		System.exit(0);
	}

	/**
	 * button on the main interface "unidade de ensino" do not confuse it with
	 * the 'escola' button inside the "unidade de ensino" view
	 */
	@FXML
	public void onClickUnidadeEnsino() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(App.class
				.getResource("view/CadastroUnidadeEnsinoView.fxml"));
		FXMLLoader menuLoader = new FXMLLoader();
		menuLoader.setLocation(App.class
				.getResource("view/UnidadeEnsinoMenu.fxml"));
		try {
			AnchorPane unidadeEnsinoPane = loader.load();
			Pane unidadeEnsinoMenu = menuLoader.load();

			rootLayout.setCenter(unidadeEnsinoPane);
			rootLayout.setLeft(unidadeEnsinoMenu);

			CadastroUnidadeEnsinoController controller = loader.getController();

			SchoolController sController = new SchoolController();
			controller.mostrarEscola(sController.getAll().get(0));
		} catch (IOException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		} catch (IndexOutOfBoundsException e) {
			Alert alert = new Alert(AlertType.WARNING,
					"Voc� ainda n�o importou o arquivo de configura��o!");
			alert.show();
		}

	}

	public void onClickVoltar() {
		// FXMLLoader loader = new FXMLLoader();
		// loader.setLocation(App.class.getResource("view/initialContent.fxml"));
		// AnchorPane center;
		// try {
		// center = loader.load();
		// rootLayout.setCenter(center);
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		mainApp.setInitialContent();
		mainApp.reloadMainMenu();

	}

	public void reloadMainMenu() {
		mainApp.reloadMainMenu();
	}

	/**
	 * this method is called when the user clicks "procurar" on the TurmasMenu
	 * Menu
	 */
	public void voltarTurmas() {
		onClickTurmas();

	}

	public void onClickContatos() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(App.class.getResource("view/ContatosView.fxml"));
		AnchorPane contactsPane;

		try {
			contactsPane = (AnchorPane) loader.load();
			ContatosViewController contatosViewController = loader
					.getController();
			SchoolContactsController contatosController = new SchoolContactsController();
			contatosViewController.showContactDetails(contatosController
					.getById(2));
			contatosViewController.balanceTable();
			contatosViewController.showContacts(contatosController.getAll());

			rootLayout.setCenter(contactsPane);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onClickEscola() {
		onClickUnidadeEnsino();
	}

	public void onClickMostrarAlunos() {
		FXMLLoader alunosLoader = new FXMLLoader(
				App.class.getResource("view/UsersView.fxml"));
		try {
			rootLayout.setCenter((AnchorPane) alunosLoader.load());
			AlunosViewController alunosController = alunosLoader
					.getController();
			alunosController.balanceTableColumns();
			alunosController.setEnterKeySensibility();
			alunosController.setRootController(this);
			alunosController.disableSelectButton();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * loadUserEdit is called from the userViewController to update the scene
	 * with the users edition interface
	 */
	public void loadUserEdit(boolean isNew, User selected) {
		FXMLLoader centerLoader = new FXMLLoader();
		centerLoader.setLocation(App.class
				.getResource("view/AlunoEditView.fxml"));
		FXMLLoader menuLoader = new FXMLLoader();
		menuLoader.setLocation(App.class.getResource("view/UsersMenu.fxml"));

		try {
			rootLayout.setCenter((AnchorPane) centerLoader.load());
			rootLayout.setLeft((AnchorPane) menuLoader.load());
			UserEditViewController editUserController = centerLoader
					.getController();

			UserEditViewController menuController = menuLoader.getController();
			menuController.setRootController(this);
			menuController.setCenterReference(editUserController);
			menuController.disableButtonsForNewStudent();

			if (isNew == false) {
				// this display the selected user as the user is not new
				editUserController.displayUser(selected);
			} else if (isNew == true) {
				// in this case the user wants to register a new user
				editUserController.onClickNovo();

			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void onClickTurmas() {
		FXMLLoader centerLoader = new FXMLLoader();
		centerLoader.setLocation(App.class
				.getResource("view/TurmasOverview.fxml"));
		FXMLLoader menuLoader = new FXMLLoader();
		menuLoader.setLocation(App.class.getResource("view/TurmasMenu.fxml"));

		try {
			rootLayout.setCenter((AnchorPane) centerLoader.load());
			rootLayout.setLeft((AnchorPane) menuLoader.load());
			CourseController courseController = new CourseController();
			TurmasController turmasController = centerLoader.getController();
			OfferedCoursesController ofCourseController = new OfferedCoursesController();
			turmasController.setEnterKeySensibility();
			turmasController.setOfferedCourses(ofCourseController.getAll());
			turmasController.setCourseList(courseController.getAll());
			turmasController.disableTurmasButton();
			// generates the controller for the left menu and send it
			// to the center layout controller
			RootLayoutController menuController = menuLoader.getController();

			menuController.disableTurmasMenu();
			turmasController.setRootLayout(rootLayout);
			turmasController.setRootLayoutController(menuController);
			turmasController.balanceTableColumns();
			turmasStaticController = turmasController;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void onClickIntegracaoDados() {
		FXMLLoader centerLoader = new FXMLLoader();
		centerLoader.setLocation(App.class
				.getResource("view/DataIntegration.fxml"));
		DataIntegrationController diController = centerLoader.getController();
		try {
			rootLayout.setCenter(centerLoader.load());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@FXML
	public void onClickAjuda() {
		// try {
		// Desktop.getDesktop().browse(
		// new URI("http://www.youtube.com/watch?v=Sagg08DrO5U"));
		// } catch (IOException e1) {
		// e1.printStackTrace();
		// } catch (URISyntaxException e1) {
		// e1.printStackTrace();
		// }
	}

	public void disableTurmasMenu() {
		this.bProcurar.setDisable(true);
		this.bEditarTurma.setDisable(true);
		this.bSalvar.setDisable(true);
		this.bCancelar.setDisable(true);
		this.bApagar.setDisable(true);

	}

	public void disableTurmasMenuSpecific() {

		this.bSalvar.setDisable(true);
		this.bCancelar.setDisable(true);
		this.bApagar.setDisable(true);
		this.bEditarTurma.setDisable(true);
	}

	public void onClickEditarTurma() {
		this.bSalvar.setDisable(false);
		this.bCancelar.setDisable(false);
		this.bApagar.setDisable(false);
		this.bProcurar.setDisable(true);
		this.bEditarTurma.setDisable(true);
		schoolCOurseDetailsController.enableEdit();
	}

	public void enableSearchButton() {
		bProcurar.setDisable(false);
	}

	public void onClickImportarDados() {
		FXMLLoader importarLoader = new FXMLLoader(
				App.class.getResource("view/dataImportView.fxml"));
		try {
			AnchorPane apImport = (AnchorPane) importarLoader.load();
			Stage secondStage = new Stage();
			secondStage.initModality(Modality.APPLICATION_MODAL);
			secondStage.initStyle(StageStyle.UNIFIED);
			secondStage.setTitle("Configura��o do sistema");
			secondStage.setScene(new Scene(apImport));
			secondStage.setResizable(false);
			secondStage.show();
			ImportController controller = importarLoader.getController();
			controller.initializeCurrentPage(0);
			// sets the page-control variable to 0
			// to indicate is has just opened

			controller.setStage(secondStage);
			controller.setMainPane(apImport);
			controller.disableBackButton();
			controller.setItself(controller);

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void onCLickBackup() {
		FXMLLoader centerLoader = new FXMLLoader();
		centerLoader.setLocation(App.class.getResource("view/BackupView.fxml"));
		try {
			rootLayout.setCenter(centerLoader.load());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void onClickMatricular() {
		FXMLLoader centerLoader = new FXMLLoader();
		centerLoader.setLocation(App.class
				.getResource("view/MatriculaView.fxml"));

		try {
			rootLayout.setCenter(centerLoader.load());
			MatricularViewController mvController = centerLoader
					.getController();

			mvController.prepareView();
			mvController.prepareSchoolCourses();
			mvController.balanceTable();
			mvController.showSchoolCourses();
			mvController.setRootController(this);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void onClickExportardados() {
		FXMLLoader centerLoader = new FXMLLoader();
		centerLoader.setLocation(App.class.getResource("view/DataExport.fxml"));
		ExportDataController controller = centerLoader.getController();
		try {
			rootLayout.setCenter(centerLoader.load());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void setSchoolCourseDetailsController(
			SchoolCOurseDetailsController schoolCOurseDetailsController) {
		this.schoolCOurseDetailsController = schoolCOurseDetailsController;

	}

	public void enableEditSchoolCourse() {

		bEditarTurma.setDisable(false);
	}

	/**
	 * Menu das turmas: metodo chamado quando se clica no 'salvar' na view das
	 * turmas
	 */
	public void onClickSalvarTurma() {
		AnchorPane modPane = (AnchorPane) rootLayout.getCenter();
		Pane dadosTurma = (Pane) modPane.getChildren().get(1);
		RadioButton fSemester = (RadioButton) dadosTurma.getChildren().get(10);
		RadioButton sSemester = (RadioButton) dadosTurma.getChildren().get(11);
		RadioButton bSemester = (RadioButton) dadosTurma.getChildren().get(12);

		if (!fSemester.isSelected() && !bSemester.isSelected()
				&& !sSemester.isSelected()) {
			// if none of the 'VIGENCIA' types are selected, tcCad warns the
			// user and dont save the schoolCourse

			Alert alert = new Alert(AlertType.WARNING);
			alert.setContentText("Voc� precisa selecionar uma vig�ncia (Primeiro semestre, Segundo semestre, ou ambos)!");
			alert.showAndWait();
		} else {
			bEditarTurma.setDisable(true);
			bCancelar.setDisable(true);
			bApagar.setDisable(true);
			bSalvar.setDisable(true);

			if (bNewCourse) {
				// the true argument here means the SchoolCourse is new so it
				// shold be persisted
				schoolCOurseDetailsController.saveSchoolCourse(true);
			} else {
				// the false argument to saveSchoolCourse means the course is
				// not
				// new
				// so it will only UPDATE the course being edited, not create a
				// new
				// one
				schoolCOurseDetailsController.saveSchoolCourse(false);
			}
			// then the screen returns to the courses view
			turmasStaticController.onClickMostrarTurmas();

		}
	}

	public void onClickCancel() {
		System.out.println("onclickcancel");
		bCancelar.setDisable(true);
		bApagar.setDisable(true);
		bSalvar.setDisable(true);
		bEditarTurma.setDisable(false);
		bProcurar.setDisable(false);
		schoolCOurseDetailsController.cancel();
	}

	/**
	 * M�todo chamado quando se apaga uma turma na interface de turmas. este
	 * metodo deleta a turma e em seguida volta pra tela de offeredCourses
	 * (cursos oferecidos pela escola)
	 * 
	 */
	public void onClickApagar() {
		System.out.println("onclickapagar");
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmar exclus�o");
		alert.setHeaderText("Look, a Confirmation Dialog");
		alert.setContentText("Tem certeza que deseja apagar esta turma?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			schoolCOurseDetailsController.delete();
			if (turmasStaticController != null) {
				turmasStaticController.onClickMostrarTurmas();
				disableTurmasMenuSpecific();
			} else {
				System.out.println("turmascontroller null");
			}
		} else {
			// do nothing at all, wait for another click on this button
		}

	}

	public void showSchoolCourses() {
		turmasStaticController.onClickMostrarTurmas();
		disableTurmasMenu();
	}

	public void setSpecificCourseController(
			SpecificSchoolCourseViewController controller) {
		this.specificCourseController = controller;

	}

	/**
	 * Prepara o menu de turmas para um novo turma: O usu�rio s� pode cancelar
	 * ou salvar a turma
	 */
	public void prepareForNewCourse() {
		bSalvar.setDisable(false);
		bCancelar.setDisable(false);

	}

	public void showAssociarMatricula(SchoolCourseComp selected) {
		FXMLLoader centerLoader = new FXMLLoader();
		centerLoader.setLocation(App.class
				.getResource("view/AssociarMatriculaView.fxml"));
		try {
			rootLayout.setCenter(centerLoader.load());
			AssociarMatriculaController matriculaController = centerLoader
					.getController();
			matriculaController.setRootLayoutController(this);
			matriculaController.setCourse(selected);
			matriculaController.balanceTables();
			matriculaController.prepareRegistration();
			matriculaController.fillRegisteredTable();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
